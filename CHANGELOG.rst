ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

1.6.0 (2023-12-20)
------------------
* remove obsolete and unused code after migration to messenger

1.4.0 (2023-02-02)
------------------
* fred-notify-contact-data-reminder now does not
  send reminder to validated contacts. Validation
  age limit can be configured.

1.3.0 (2022-09-01)
------------------
* switch to new storage of additional contacts

1.2.2 (2022-09-02)
------------------
* fixes disclose flag values in emails

1.2.1 (2022-08-04)
------------------
* fred-notify-object-events now skips events
  to notify for which notification fails

1.1.0 (2022-03-22)
------------------
* module fred-notify-object-events

1.0.0 (2022-03-01)
------------------

* Initial ``Notifier`` implementation
  * module fred-notify-contact-data-reminder
