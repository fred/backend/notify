/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg/notification_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

boost::program_options::options_description make_notification_options(NotificationOptions& _notification_options)
{
    boost::program_options::options_description notification_options{"Notification options", Util::terminal_width};
    const auto set_config_file = [&](const std::string& config_file) { _notification_options.config_file = config_file; };
    const auto set_state_age_days_max = [&](unsigned int state_age_days_max) { _notification_options.state_age_days_max = state_age_days_max; };
    const auto set_notifications_max = [&](unsigned int notifications_max) { _notification_options.notifications_max = notifications_max; };
    const auto set_generic_email_prefixes =
            [&](const std::vector<std::string>& generic_email_prefixes) {
                copy(generic_email_prefixes.begin(),
                     generic_email_prefixes.end(),
                     std::inserter(_notification_options.generic_email_prefixes, _notification_options.generic_email_prefixes.end()));
            };
    notification_options.add_options()
        ("notification.config_file", boost::program_options::value<std::string>()->notifier(set_config_file), "configuration of notifications in JSON format")
        ("notification.state_age_days_max", boost::program_options::value<unsigned int>()->notifier(set_state_age_days_max), "do not notify state changes older then state_age_days_max days")
        ("notification.notifications_max", boost::program_options::value<unsigned int>()->notifier(set_notifications_max), "process no more than notifications_max notifications")
        ("notification.generic_email_prefix", boost::program_options::value<std::vector<std::string>>()->multitoken()->notifier(set_generic_email_prefixes), "the local part of the generic email address for domain (local_part@domainname.tld)");

    return notification_options;
}

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred
