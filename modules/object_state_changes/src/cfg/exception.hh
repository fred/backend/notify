/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTION_HH_E430C0A5A61E488C9B32AF1484CE2C5E
#define EXCEPTION_HH_E430C0A5A61E488C9B32AF1484CE2C5E

#include <stdexcept>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

struct Exception : std::runtime_error
{
    explicit Exception(const char* _msg = "");
    explicit Exception(const std::string& _msg);
};

struct UnknownOption : Exception
{
    explicit UnknownOption(const char* _msg);
    explicit UnknownOption(const std::string& _msg);
};

struct MissingOption : Exception
{
    explicit MissingOption(const char* _msg);
    explicit MissingOption(const std::string& _msg);
};

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
