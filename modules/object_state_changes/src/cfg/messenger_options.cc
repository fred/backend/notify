/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg/messenger_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

boost::program_options::options_description make_messenger_options(MessengerOptions& _messenger_options)
{
    boost::program_options::options_description messenger_options{"Common messenger access options", Util::terminal_width};
    const auto set_endpoint = [&](const std::string& endpoint) { _messenger_options.endpoint = endpoint; };
    const auto set_archive = [&](const bool archive) { _messenger_options.archive = archive; };
    messenger_options.add_options()
        ("messenger.endpoint", boost::program_options::value<std::string>()->notifier(set_endpoint), "name of endpoint to connect to")
        ("messenger.archive", boost::program_options::value<bool>()->notifier(set_archive), "archive the message");
    return messenger_options;
}

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred
