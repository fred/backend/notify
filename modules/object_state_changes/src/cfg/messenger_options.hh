/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef MESSENGER_OPTIONS_HH_9016558A8BD54196AF4EC197F10C158F
#define MESSENGER_OPTIONS_HH_9016558A8BD54196AF4EC197F10C158F

#include <boost/asio/ip/address.hpp>
#include <boost/program_options.hpp>

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

struct MessengerOptions
{
    std::string endpoint;
    bool archive;
};

boost::program_options::options_description make_messenger_options(MessengerOptions& _messenger_options);

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
