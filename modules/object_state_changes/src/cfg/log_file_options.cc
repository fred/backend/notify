/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/log_file_options.hh"

#include "src/cfg/exception.hh"
#include "src/cfg/util/make_severity.hh"
#include "src/cfg/util/terminal_width.hh"

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

void LogFileOptions::set_file_name(const std::string& name)
{
    if (file_name != boost::none)
    {
        throw Exception{"file name can be defined at most once"};
    }
    file_name = name;
}

void LogFileOptions::set_min_severity(const std::string& severity)
{
    if (min_severity != boost::none)
    {
        throw Exception{"severity can be defined at most once"};
    }
    min_severity = Util::make_severity(severity);
}

boost::program_options::options_description make_log_file_options(LogFileOptions& _log_file_options)
{
    boost::program_options::options_description options_description{"Logging into file options", Util::terminal_width};
    const auto set_file_name = [&](const std::string& file_name) { _log_file_options.set_file_name(file_name); };
    const auto set_min_severity = [&](const std::string& severity) { _log_file_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.file.file_name", boost::program_options::value<std::string>()->notifier(set_file_name), "what file to log into")
        ("log.file.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred
