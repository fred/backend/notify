/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TERMINAL_WIDTH_HH_5E5DFE7ADAF94835BF058118D70BAA91
#define TERMINAL_WIDTH_HH_5E5DFE7ADAF94835BF058118D70BAA91

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {
namespace Util {

static constexpr unsigned terminal_width = 120;

} // namespace Fred::Notify::ObjectStateChanges::Cfg::Util
} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
