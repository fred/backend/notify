/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NOTIFICATION_OPTIONS_HH_B13D5B2D87B04D8BAA1539024C963E28
#define NOTIFICATION_OPTIONS_HH_B13D5B2D87B04D8BAA1539024C963E28

#include <boost/program_options.hpp>

#include <set>
#include <string>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

struct NotificationOptions
{
    std::string config_file;
    unsigned int state_age_days_max;
    unsigned int notifications_max;
    std::set<std::string> generic_email_prefixes;
};

boost::program_options::options_description make_notification_options(NotificationOptions& _notification_options);

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
