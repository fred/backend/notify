/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CFG_HH_DA6E20C2C23443E791A60D1669D8059E
#define CFG_HH_DA6E20C2C23443E791A60D1669D8059E

#include "src/cfg/database_options.hh"
#include "src/cfg/exception.hh"
#include "src/cfg/log_options.hh"
#include "src/cfg/messenger_options.hh"
#include "src/cfg/notification_options.hh"

#include <boost/optional.hpp>

#include <chrono>
#include <set>
#include <stdexcept>
#include <string>
#include <vector>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Cfg {

class AllDone : public std::exception
{
public:
    explicit AllDone(std::string _msg) noexcept;
    const char* what() const noexcept override;
private:
    const std::string msg_;
};

class Options
{
public:
    Options() = delete;
    Options(const Options&) = delete;
    Options(Options&&) = delete;
    Options& operator=(const Options&) = delete;
    Options& operator=(Options&&) = delete;

    static const Options& get();
    static const Options& init(int _argc, const char* const* _argv);

    boost::optional<std::string> config_file_name;

    DatabaseOptions database;
    MessengerOptions messenger;
    Log log;
    NotificationOptions notification;
private:
    Options(int _argc, const char* const* _argv);
};

void handle_cli_args(int _argc, char* _argv[]);

} // namespace Fred::Notify::ObjectStateChanges::Cfg
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
