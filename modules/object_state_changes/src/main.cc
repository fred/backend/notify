/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg.hh"

#include "src/impl/notify_object_state_changes.hh"
#include "src/util/set_logging.hh"

#include "liblog/liblog.hh"

#include <nlohmann/json.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>

namespace Cfg = Fred::Notify::ObjectStateChanges::Cfg;
namespace Util = Fred::Notify::ObjectStateChanges::Util;
namespace Impl = Fred::Notify::ObjectStateChanges::Impl;

Impl::Notifications make_notification_configuration(const std::string& _config_file_name)
{
    Impl::Notifications notifications;

    using json = nlohmann::json;
    std::ifstream config_file(_config_file_name);
    json json_data;
    config_file >> json_data;

    if (json_data.empty())
    {
        throw std::runtime_error("configuration of notifications in json config file \"" + _config_file_name + "\" empty");
    }
    if (!json_data.contains("notifications"))
    {
        throw std::runtime_error("configuration of notifications in json config file \"" + _config_file_name + "\" invalid: notifications missing");
    }

    for (const auto& j_element : json_data["notifications"].items())
    {
        const auto& j_object_types = j_element.value()["objects"];
        const auto& j_flags = j_element.value()["flags"];
        const auto& j_message_type_value = j_element.value()["message_type"]["value"];
        const auto& j_message_type_properties = j_element.value()["message_type"]["properties"];
        const auto& j_recipients = j_element.value()["recipients"];
        for (const auto& j_flag : j_flags)
        {
            for (const auto& j_object_type : j_object_types)
            {
                const Impl::State state{
                        Impl::from_string<Impl::ObjectType>(j_object_type),
                        Impl::Flag{j_flag}};

                auto message_type =
                    Impl::MessageType{j_message_type_value,
                                      {{"mail_type", ""},
                                       {"mail_template_subject", ""},
                                       {"mail_template_body", ""},
                                       {"sms_type", ""},
                                       {"sms_template_body", ""}}};

                if (j_message_type_value == "email")
                {
                    message_type.properties["mail_type"] = j_message_type_properties["mail_type"];
                    message_type.properties["mail_template_subject"] = j_message_type_properties["mail_template_subject"];
                    message_type.properties["mail_template_body"] = j_message_type_properties["mail_template_body"];
                }
                else if (j_message_type_value == "sms")
                {
                    message_type.properties["sms_type"] = j_message_type_properties["sms_type"];
                    message_type.properties["sms_template_body"] = j_message_type_properties["sms_template_body"];
                }
                else {
                    throw std::runtime_error{"unexpected contact type " + std::string{j_message_type_value}};
                }

                std::transform(
                        j_recipients.cbegin(),
                        j_recipients.cend(),
                        std::back_inserter(notifications[state][message_type]),
                        [](auto&& j_recipient) -> Impl::RecipientGroup {
                            return Impl::from_string<Impl::RecipientGroup>(j_recipient);
                        });
            }
        }
    }
    return notifications;
}

int main(int argc, char** argv)
{
    Cfg::handle_cli_args(argc, argv);

    try
    {
        Util::set_logging(Cfg::Options::get().log);
        LIBLOG_SET_CONTEXT(Ctx, log_ctx, __func__);
        LIBLOG_INFO("fred-notify-object-state-changes configured");

        Impl::notify_object_state_changes(
                make_notification_configuration(Cfg::Options::get().notification.config_file),
                Impl::StateAgeDaysMax{Cfg::Options::get().notification.state_age_days_max},
                Impl::NotificationsMax{Cfg::Options::get().notification.notifications_max},
                Impl::MessengerEndpoint{Cfg::Options::get().messenger.endpoint},
                Impl::MessengerArchive{Cfg::Options::get().messenger.archive});
    }
    catch (const std::exception& e)
    {
        LIBLOG_WARNING("std::exception caught: {}", e.what());
    }
    catch (...)
    {
        LIBLOG_WARNING("exception caught");
    }
}
