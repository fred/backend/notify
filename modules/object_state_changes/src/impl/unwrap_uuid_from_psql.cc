/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/unwrap_uuid_from_psql.hh"

#include "src/impl/exception.hh"

#include <boost/uuid/string_generator.hpp>

#include <stdexcept>

namespace boost {
namespace uuids {

uuid unwrap_from_psql_representation(const uuid&, const char* _str)
{
    try
    {
        return string_generator{}(_str);
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : Fred::Notify::ObjectStateChanges::Impl::InvalidUuid
        {
            explicit ConversionFailure(const std::string& _msg)
                : InvalidUuid(_msg)
            {
            }
        };
        throw ConversionFailure{"unable to convert string \"" + std::string{_str} + "\" to uuid"};
    }
}

} // namespace boost::uuids
} // namespace boost
