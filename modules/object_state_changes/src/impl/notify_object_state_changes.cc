/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/notify_object_state_changes.hh"

#include "src/cfg.hh"
#include "src/impl/exception.hh"
#include "src/impl/strong_type.hh"
#include "src/impl/unwrap_uuid_from_psql.hh"

#include "src/util/util.hh"
#include "src/util/get_optional.hh"

#include "libhermes/libhermes.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"

#include "libstrong/type.hh"

#include <boost/algorithm/string/trim.hpp>
#include <boost/date_time/date_clock_device.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/functional/hash.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <regex>
#include <stdexcept>
#include <tuple>
#include <utility>
#include <vector>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Impl {

namespace {

enum struct NotificationContactType
{
    email_address,
    phone_number
};

enum struct NotificationMessageType
{
    email,
    letter,
    sms,
};

/**
 * Helps to implement from_db_handle function using to_db_handle conversion function.
 * @tparam S type of source value
 * @tparam D type of destination value
 * @tparam items number of all possible destination values
 * @param _src source value
 * @param _set_of_results C array of all possible destination values
 * @param _forward_transformation function which transforms D value into S value
 * @return matching counterpart value
 * @throw std::invalid_argument in case that conversion fails
 */
template <class S, class D, unsigned items>
D inverse_transformation(const S& _src, const D (&_set_of_results)[items], S (*_forward_transformation)(D))
{
    const D* const end_of_results = _set_of_results + items;
    for (const D* result_candidate_ptr = _set_of_results; result_candidate_ptr < end_of_results; ++result_candidate_ptr)
    {
        if (_forward_transformation(*result_candidate_ptr) == _src)
        {
            return *result_candidate_ptr;
        }
    }
    throw std::invalid_argument("the value \"" + _src + "\" is invalid: conversion does not exist");
}

std::string to_string(ObjectType _value)
{
    switch (_value)
    {
        case ObjectType::contact: return "contact";
        case ObjectType::nsset: return "nsset";
        case ObjectType::domain: return "domain";
        case ObjectType::keyset: return "keyset";
    }
    throw std::logic_error("value doesn't exist in ObjectState");
}

std::string to_string(RecipientGroup _value)
{
    switch (_value)
    {
        case RecipientGroup::admin: return "admin";
        case RecipientGroup::tech: return "tech";
        case RecipientGroup::generic: return "generic";
        case RecipientGroup::additional: return "additional";
    }
    throw std::logic_error("value doesn't exist in RecipientGroup");
}

std::string to_string(NotificationContactType _value)
{
    switch (_value)
    {
        case NotificationContactType::email_address: return "email_address";
        case NotificationContactType::phone_number: return "phone_number";
    }
    throw std::logic_error("value doesn't exist in NotificationContactType");
}

std::string to_string(NotificationMessageType _value)
{
    switch (_value)
    {
        case NotificationMessageType::email: return "email";
        case NotificationMessageType::letter: return "letter";
        case NotificationMessageType::sms: return "sms";
    }
    throw std::logic_error("value doesn't exist in NotificationMessageType");
}

} // namespace Fred::Notify::ObjectStateChanges::Impl::{anonymous}

template <>
ObjectType from_string<ObjectType>(const std::string& _str)
{
    static constexpr ObjectType possible_results[] =
            {
                ObjectType::contact,
                ObjectType::nsset,
                ObjectType::domain,
                ObjectType::keyset
            };
    return inverse_transformation(_str, possible_results, to_string);
}

template <>
RecipientGroup from_string<RecipientGroup>(const std::string& _str)
{
    static constexpr RecipientGroup possible_results[] =
            {
                RecipientGroup::admin,
                RecipientGroup::tech,
                RecipientGroup::generic,
                RecipientGroup::additional
            };
    return inverse_transformation(_str, possible_results, to_string);
}

template <>
NotificationContactType from_string<NotificationContactType>(const std::string& _str)
{
    static constexpr NotificationContactType possible_results[] =
            {
                NotificationContactType::email_address,
                NotificationContactType::phone_number
            };
    return inverse_transformation(_str, possible_results, to_string);
}

template <>
NotificationMessageType from_string<NotificationMessageType>(const std::string& _str)
{
    static constexpr NotificationMessageType possible_results[] =
            {
                NotificationMessageType::email,
                NotificationMessageType::letter,
                NotificationMessageType::sms
            };
    return inverse_transformation(_str, possible_results, to_string);
}

decltype(auto) wrap_into_psql_representation(RecipientGroup _value)
{
    switch (_value)
    {
        case RecipientGroup::admin: return std::string{"admin"};
        case RecipientGroup::tech: return std::string{"tech"};
        case RecipientGroup::generic: return std::string{"generic"};
        case RecipientGroup::additional: return std::string{"additional"};
    }
    struct ConversionFailure : InvalidRecipientGroup
    {
        explicit ConversionFailure(std::string _msg)
            : InvalidRecipientGroup(std::move(_msg))
        {
        }
    };
    throw ConversionFailure{"unexpected value of RecipientGroup"};
}

namespace {

decltype(auto) wrap_into_psql_representation(NotificationMessageType _value)
{
    try
    {
        return to_string(_value);
    }
    catch (...)
    {
        struct ConversionFailure : InvalidObjectType
        {
            explicit ConversionFailure() : InvalidObjectType{std::string{}} { }
            const char* what() const noexcept override { return "unexpected value of NotificationMessageType"; }
        };
        throw ConversionFailure{};
    }
}

} // namespace Fred::Notify::ObjectStateChanges::Impl::{anonymous}

Flag unwrap_from_psql_representation(const Flag&, const char* _str)
{
    try
    {
        return Flag{std::string{_str}};
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : InvalidFlag
        {
            explicit ConversionFailure(std::string _msg)
                : InvalidFlag(std::move(_msg))
            {
            }
        };
        throw ConversionFailure{"unable to convert string \"" + std::string{_str} + "\" to Flag"};
    }
}

decltype(auto) wrap_into_psql_representation(ObjectType _value)
{
    switch (_value)
    {
        case ObjectType::contact: return std::string{"contact"};
        case ObjectType::nsset: return std::string{"nsset"};
        case ObjectType::domain: return std::string{"domain"};
        case ObjectType::keyset: return std::string{"keyset"};
    }
    struct ConversionFailure : InvalidObjectType
    {
        explicit ConversionFailure(std::string _msg)
            : InvalidObjectType(std::move(_msg))
        {
        }
    };
    throw ConversionFailure{"unexpected value of ObjectType"};
}

ObjectType unwrap_from_psql_representation(const ObjectType&, const char* _str)
{
    try
    {
        return from_string<ObjectType>(_str);
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : InvalidObjectType
        {
            explicit ConversionFailure(std::string _msg)
                : InvalidObjectType(std::move(_msg))
            {
            }
        };
        throw ConversionFailure{"unable to convert string \"" + std::string{_str} + "\" to ObjectType"};
    }
}

RecipientGroup unwrap_from_psql_representation(const RecipientGroup&, const char* _str)
{
    try
    {
        return from_string<RecipientGroup>(_str);
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : InvalidRecipientGroup
        {
            explicit ConversionFailure(std::string _msg)
                : InvalidRecipientGroup(std::move(_msg))
            {
            }
        };
        throw ConversionFailure{"unable to convert string \"" + std::string{_str} + "\" to RecipientGroup"};
    }
}

namespace {

NotificationContactType unwrap_from_psql_representation(const NotificationContactType&, const char* _str)
{
    try
    {
        return from_string<NotificationContactType>(_str);
    }
    catch (const std::runtime_error&)
    {
        struct ConversionFailure : InvalidNotificationContactType
        {
            explicit ConversionFailure(std::string _msg)
                : InvalidNotificationContactType(std::move(_msg))
            {
            }
        };
        throw ConversionFailure{"unable to convert string \"" + std::string{_str} + "\" to NotificationContactType"};
    }
}

} // namespace Fred::Notify::ObjectStateChanges::Impl::{anonymous}

bool MessageType::operator<(const MessageType& _other) const
{
    return std::make_tuple(value, properties) <
           std::make_tuple(_other.value, _other.properties);
}

namespace {

using StateId = LibStrong::Type<int, struct StateIdTag_>;
using ValidFrom = LibStrong::Type<boost::posix_time::ptime, struct ValidFromTag_>;
using ObjectId = LibStrong::Type<int, struct ObjectIdTag_>;
using ObjectUuid = LibStrong::Type<boost::uuids::uuid, struct ObjectUuidTag_>;

using ContactId = LibStrong::Type<int, struct ContactIdTag_>;
using DomainId = LibStrong::Type<int, struct DomainIdTag_>;
using NssetId = LibStrong::Type<int, struct NssetIdTag_>;
using KeysetId = LibStrong::Type<int, struct KeysetIdTag_>;

using Handle = StrongString<struct HandleTag_>;

using NotificationContact = StrongString<struct NotificationContactTag_>;
using ContactUuid = StrongUuid<struct ContactUuidTag_>;

struct Recipient
{
    std::set<NotificationContact> notification_contacts;
    NotificationContactType notification_contact_type;
    Optional<ContactUuid> uuid;

    Recipient(
            std::set<NotificationContact> _notification_contacts,
            NotificationContactType _notification_contact_type,
            Optional<ContactUuid> _uuid)
        : notification_contacts(std::move(_notification_contacts)),
          notification_contact_type(std::move(_notification_contact_type)),
          uuid(std::move(_uuid))
    {
    }

    bool operator<(const Recipient& _other) const
    {
        return std::make_tuple(notification_contacts, notification_contact_type, uuid) <
               std::make_tuple(_other.notification_contacts, _other.notification_contact_type, _other.uuid);
    }
};

std::set<Recipient> get_domain_admin_emails_history(
        const LibPg::PgTransaction& _tx,
        const DomainId& _domain_id)
{
    std::set<Recipient> recipients;

    const auto query_registrant = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM object_registry dor "
              "JOIN domain_history dh ON dh.historyid = dor.historyid "
              "JOIN object_registry cor ON cor.id = dh.registrant "
              "JOIN contact_history ch ON ch.historyid = cor.historyid "
             "WHERE dor.id = " << LibPg::parameter<DomainId>().as_integer();

    const auto dbres_registrant = exec(_tx, query_registrant, {_domain_id});

    for (const auto& row : dbres_registrant)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }

    const auto query_related = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM contact_history ch "
              "JOIN object_registry cor ON cor.historyid = ch.historyid "
              "JOIN domain_contact_map_history dcm ON dcm.contactid = cor.id "
              "JOIN object_registry dor ON dor.historyid = dcm.historyid "
             "WHERE dor.id = " << LibPg::parameter<DomainId>().as_integer();
            // clang-format on

    const auto dbres_related = exec(_tx, query_related, {_domain_id});

    for (const auto& row : dbres_related)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }
    return recipients;
}

std::set<Recipient> get_domain_tech_emails_history(
        const LibPg::PgTransaction& _tx,
        const DomainId& _domain_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM contact_history ch "
              "JOIN object_registry cor ON cor.historyid = ch.historyid "
              "JOIN nsset_contact_map_history ncm ON ncm.contactid = cor.id "
              "JOIN object_registry nor ON nor.historyid = ncm.historyid "
              "JOIN domain_history dh ON dh.nsset = nor.id "
              "JOIN object_registry dor ON dor.historyid = dh.historyid "
             "WHERE dor.id = " << LibPg::parameter<DomainId>().as_integer();
            // clang-format on

    const auto dbres = exec(_tx, query, {_domain_id});

    std::set<Recipient> recipients;

    for (const auto& row : dbres)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }
    return recipients;
}

// ticket #14873
template <typename T>
std::set<Recipient> get_recipients_additional(const LibPg::PgTransaction&, const T&, const StateId& _state_id);

template <>
std::set<Recipient> get_recipients_additional<DomainId>(
        const LibPg::PgTransaction& _tx,
        const DomainId& _domain_id,
        const StateId& _state_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "WITH dlp AS "
            "("
                "SELECT o.id, "
                       "o.valid_for_exdate_after, "
                       "COALESCE((SELECT i.valid_for_exdate_after "
                                   "FROM domain_lifecycle_parameters i "
                                  "WHERE o.id < i.id "
                               "ORDER BY i.id "
                                  "LIMIT 1), "
                                "'infinity'::TIMESTAMP "
                               ") AS valid_for_exdate_before, "
                       "o.outzone_unguarded_email_warning_period, "
                       "o.expiration_dns_protection_period, "
                       "o.expiration_letter_warning_period, "
                       "o.expiration_registration_protection_period "
                  "FROM domain_lifecycle_parameters o "
            "), "
            "d AS "
            "("
                "SELECT domain.id, "
                       "domain.exdate, "
                       "(domain.exdate + dlp.outzone_unguarded_email_warning_period)::DATE AS outzone_unguarded_warning, "
                       "(domain.exdate + dlp.expiration_dns_protection_period)::DATE AS outzone, "
                       "(domain.exdate + dlp.expiration_letter_warning_period)::DATE AS delete_warning, "
                       "(domain.exdate + dlp.expiration_registration_protection_period)::DATE AS delete_candidate "
                  "FROM domain "
                  "JOIN dlp ON dlp.valid_for_exdate_after <= domain.exdate AND domain.exdate < dlp.valid_for_exdate_before "
                 "WHERE domain.id = " << LibPg::parameter<DomainId>().as_big_int() <<
            "), "
            "time_zone AS "
            "("
                "SELECT val FROM enum_parameters WHERE name = 'regular_day_procedure_zone' "
            "), "
            "s AS "
            "("
                "SELECT os.state_id, "
                       "(os.valid_from AT TIME ZONE 'UTC' AT TIME ZONE time_zone.val)::DATE AS valid_from_local_date "
                  "FROM object_state os "
                  "JOIN d ON d.id = os.object_id, "
                       "time_zone "
                 "WHERE os.id = " << LibPg::parameter<StateId>().as_big_int() << " AND "
                       "os.valid_to IS NULL "
            ") "
            "SELECT" << LibPg::item<NotificationContactType>() << "osac.type" <<
                        LibPg::item<NotificationContact>() << "UNNEST(osac.contacts) "
              "FROM notification.object_state_additional_contact osac "
              "JOIN d ON d.id = osac.object_id AND "
                        "d.exdate = osac.valid_from::DATE "
              "JOIN s ON s.state_id = osac.state_flag_id "
              "JOIN enum_object_states eos ON eos.id = s.state_id "
             "WHERE (eos.name = 'outzoneUnguardedWarning' AND "
                    "d.outzone_unguarded_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.outzone) "
                   "OR "
                   "(eos.name = 'deleteWarning' AND "
                    "d.delete_warning <= s.valid_from_local_date AND s.valid_from_local_date < d.delete_candidate)";
            // clang-format on

    const auto dbres = exec(_tx, query, {_domain_id, _state_id});

    std::set<Recipient> recipients;
    const auto contact_uuid_not_available = ContactUuid::nullopt;
    for (const auto& row : dbres)
    {
        recipients.emplace(std::set<NotificationContact>{row.get<NotificationContact>()}, row.get<NotificationContactType>(), contact_uuid_not_available);
    }
    return recipients;
}

std::set<Recipient> get_nsset_tech_emails_history(
        const LibPg::PgTransaction& _tx,
        const NssetId& _nsset_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM contact_history ch "
              "JOIN object_registry cor ON cor.historyid = ch.historyid "
              "JOIN nsset_contact_map_history ncmap ON ncmap.contactid = ch.id "
              "JOIN nsset_history nh ON ncmap.historyid = nh.historyid "
              "JOIN object_registry nobr ON nobr.historyid = nh.historyid "
             "WHERE nobr.id = " << LibPg::parameter<NssetId>().as_integer();
            // clang-format on

    const auto dbres = exec(_tx, query, {_nsset_id});

    std::set<Recipient> recipients;

    for (const auto& row : dbres)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }
    return recipients;
}

std::set<Recipient> get_keyset_tech_emails_history(
        const LibPg::PgTransaction& _tx,
        const KeysetId& _keyset_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM contact_history ch "
              "JOIN object_registry cor ON cor.historyid = ch.historyid "
              "JOIN keyset_contact_map_history kcmap ON kcmap.contactid = ch.id "
              "JOIN keyset_history kh ON kcmap.historyid = kh.historyid "
              "JOIN object_registry kobr ON kobr.historyid = kh.historyid "
             "WHERE kobr.id = " << LibPg::parameter<KeysetId>().as_integer();
            // clang-format on
 
    const auto dbres = exec(_tx, query, {_keyset_id});

    std::set<Recipient> recipients;

    for (const auto& row : dbres)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }
    return recipients;
}

std::set<Recipient> get_contact_emails_history(
        const LibPg::PgTransaction& _tx,
        const ContactId& _contact_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off`
            "SELECT" << LibPg::nullable_item<NotificationContact>() << "ch.email" <<
                        LibPg::item<ContactUuid>() << "cor.uuid "
              "FROM contact_history ch "
              "JOIN object_registry cor ON ch.historyid = cor.historyid "
             "WHERE cor.id = " << LibPg::parameter<ContactId>().as_integer();
            // clang-format on

    const auto dbres = exec(_tx, query, {_contact_id});

    std::set<Recipient> recipients;

    for (const auto& row : dbres)
    {
        if (!row.is_null<NotificationContact>())
        {
            recipients.emplace(std::set<NotificationContact>{row.get_nullable<NotificationContact>()}, NotificationContactType::email_address, row.get<ContactUuid>());
        }
    }
    return recipients;
}

// ticket #3797
std::set<Recipient> get_recipients_domain_generic(
        const Handle& _fqdn)
{
    if (_fqdn->empty())
    {
        throw std::runtime_error{"fqdn empty"};
    }
    std::set<Recipient> recipients;
    for (const auto& generic_email_prefix : Cfg::Options::get().notification.generic_email_prefixes)
    {
        recipients.emplace(std::set<NotificationContact>{NotificationContact(generic_email_prefix + "@" + *_fqdn)}, NotificationContactType::email_address, ContactUuid::nullopt);
    }
    recipients.emplace(std::set<NotificationContact>{NotificationContact{_fqdn->substr(0, _fqdn->find(".")) + "@" + *_fqdn}}, NotificationContactType::email_address, ContactUuid::nullopt);
    return recipients;
}

template <typename T>
std::set<Recipient> get_recipients(const LibPg::PgTransaction&, const T&, RecipientGroup);

template <>
std::set<Recipient> get_recipients<ContactId>(const LibPg::PgTransaction& _tx, const ContactId& _contact_id, RecipientGroup _recipient)
{
    switch (_recipient)
    {
        case RecipientGroup::admin:
            return get_contact_emails_history(_tx, _contact_id);
        case RecipientGroup::tech:
        case RecipientGroup::generic:
        case RecipientGroup::additional:
            break;
    }
    throw std::runtime_error("invalid configuration: contact does not expect recipient group " + to_string(_recipient));
}

template <>
std::set<Recipient> get_recipients<NssetId>(const LibPg::PgTransaction& _tx, const NssetId& _nsset_id, RecipientGroup _recipient)
{
    switch (_recipient)
    {
        case RecipientGroup::admin: 
            return get_nsset_tech_emails_history(_tx, _nsset_id);
        case RecipientGroup::tech:
        case RecipientGroup::generic:
        case RecipientGroup::additional:
            break;
    }
    throw std::runtime_error("invalid configuration: nsset does not expect recipient group " + to_string(_recipient));
}

template <>
std::set<Recipient> get_recipients<DomainId>(const LibPg::PgTransaction& _tx, const DomainId& _domain_id, RecipientGroup _recipient)
{
    switch (_recipient)
    {
        case RecipientGroup::admin:
            return get_domain_admin_emails_history(_tx, _domain_id);
        case RecipientGroup::tech:
            return get_domain_tech_emails_history(_tx, _domain_id);
        case RecipientGroup::generic: // should be unreachable - specific function get_recipients_domain_generic exists
        case RecipientGroup::additional: // should be unreachable - specific function get_recipients_domain_additional exists
            break;
    }
    throw std::runtime_error("invalid configuration: domain does not expect recipient group " + to_string(_recipient));
}

template <>
std::set<Recipient> get_recipients<KeysetId>(const LibPg::PgTransaction& _tx, const KeysetId& _keyset_id, RecipientGroup _recipient)
{
    switch (_recipient)
    {
        case RecipientGroup::admin:
            return get_keyset_tech_emails_history(_tx, _keyset_id);
        case RecipientGroup::tech:
        case RecipientGroup::generic:
        case RecipientGroup::additional:
            break;
    }
    throw std::runtime_error("invalid configuration: keyset does not expect recipient group " + to_string(_recipient));
}


void merge_into(LibHermes::Struct& _dst, const LibHermes::Struct& _src)
{
    if (!_src.empty())
    {
        for (LibHermes::Struct::const_iterator it = _src.begin(); it != _src.end(); ++it)
        {
            if (_dst.find(it->first) != _dst.end())
            {
                throw std::runtime_error{"data loss"};
            }
        }
        _dst.insert(_src.begin(), _src.end());
    }
}

LibHermes::Struct collect_domain_template_parameters_history_admin(
        const LibPg::PgTransaction& _tx,
        const ObjectId& _object_id)
{
    const auto query = LibPg::make_query() <<
            // clang-format off`
            "SELECT" << LibPg::item<Handle>() << "cor.name " <<
              "FROM object_registry dor "
              "JOIN domain_contact_map_history dcmh ON dcmh.historyid = dor.historyid AND dcmh.role = 1 "
              "JOIN object_registry cor ON cor.id = dcmh.contactid "
             "WHERE dor.id = " << LibPg::parameter<ObjectId>().as_integer();
    // clang-format on

    const auto dbres = exec(_tx, query, {_object_id});

    LibHermes::Struct result;
    {
        std::vector<LibHermes::StructValue> administrators;

        for (const auto& row : dbres)
        {
            const auto handle = row.get<Handle>();
            administrators.push_back(LibHermes::StructValue{*handle});
        }

        if (!administrators.empty())
        {
            result.emplace(LibHermes::StructKey{"administrators"}, LibHermes::StructValue{std::move(administrators)});
        }
    }
    return result;
}

LibHermes::Struct collect_domain_template_parameters_history(
        const LibPg::PgTransaction& _tx,
        const ObjectId& _object_id,
        const ValidFrom& _valid_from)
{
    using Fqdn = StrongString<struct FqdnTag_>;
    using Owner = StrongString<struct OwnerTag_>;
    using Nsset = StrongString<struct NssetTag_>;
    using RegistrarHandle = StrongString<struct RegistrarHandleTag_>;
    using RegistrarName = StrongString<struct RegistrarNameTag_>;
    using RegistrarUrl = StrongString<struct RegistrarUrlTag_>;
    using ValDate = LibStrong::Type<boost::gregorian::date, struct ValDateTag_>;
    using ExDate = LibStrong::Type<boost::gregorian::date, struct ExDateTag_>;
    using DnsDate = LibStrong::Type<boost::gregorian::date, struct DnsDateTag_>;
    using ExRegDate = LibStrong::Type<boost::gregorian::date, struct ExRegDateTag_>;
    using Zone = StrongString<struct ZoneTag_>;

    const auto query = LibPg::make_query() <<
            // clang-format off`
            "SELECT" << LibPg::item<Fqdn>() << "dobr.name" <<
                        LibPg::item<Owner>() << "cobr.name" <<
                        LibPg::nullable_item<Nsset>() << "(SELECT name FROM object_registry WHERE id = dh.nsset)" <<
                        LibPg::item<RegistrarHandle>() << "r.handle" <<
                        LibPg::nullable_item<RegistrarName>() << "r.name" <<
                        LibPg::nullable_item<RegistrarUrl>() << "r.url" <<
                        LibPg::nullable_item<ValDate>() << "(SELECT exdate FROM enumval_history WHERE historyid = dobr.historyid)" <<
                        LibPg::nullable_item<ExDate>() << "dh.exdate" <<
                        LibPg::nullable_item<DnsDate>() << "dh.exdate + dlp.expiration_dns_protection_period" <<
                        LibPg::nullable_item<ExRegDate>() << "dh.exdate + dlp.expiration_registration_protection_period" <<
                        LibPg::nullable_item<Zone>() << "(SELECT fqdn FROM zone WHERE id = dh.zone) "
              "FROM object_registry dobr "
              "JOIN object_history doh ON doh.historyid = dobr.historyid "
              "JOIN registrar r ON r.id = doh.clid "
              "JOIN domain_history dh ON dh.historyid = dobr.historyid "
              "JOIN object_registry cobr ON cobr.id = dh.registrant "
              "JOIN domain_lifecycle_parameters dlp ON dlp.valid_for_exdate_after = (SELECT MAX(valid_for_exdate_after) FROM domain_lifecycle_parameters WHERE valid_for_exdate_after <= dh.exdate) "
              "WHERE dobr.id = " << LibPg::parameter<ObjectId>().as_integer();
    // clang-format on

    const auto dbres = exec(_tx, query, {_object_id});

    if (dbres.size() != LibPg::RowIndex{1})
    {
        throw std::runtime_error{"unexpected number of rows"};
    }

    const auto& row = dbres.front();

    const auto domain = row.get<Fqdn>();
    const auto owner = row.get<Owner>();
    const auto nsset = Util::get_optional<Nsset>(row);
    const auto valdate = Util::get_optional<ValDate>(row);
    const auto exdate = Util::get_optional<ExDate>(row);
    const auto dnsdate = Util::get_optional<DnsDate>(row);
    const auto exregdate = Util::get_optional<ExRegDate>(row);
    const auto checkdate = to_iso_extended_string(boost::gregorian::date(boost::gregorian::day_clock::local_day()));
    const auto zone = Util::get_optional<Zone>(row);
    const auto registrar =
        [&](){
            const auto registrar_handle = boost::algorithm::trim_copy(*row.get<RegistrarHandle>());
            const auto registrar_name = row.is_null<RegistrarName>() ? "" : boost::algorithm::trim_copy(*row.get_nullable<RegistrarName>());
            const auto registrar_url = row.is_null<RegistrarUrl>() ? "" : boost::algorithm::trim_copy(*row.get_nullable<RegistrarUrl>());
            return registrar_name.empty()
                    ? registrar_handle
                    : registrar_name + (!registrar_url.empty() ? " (" + registrar_url + ")" : "");
        }();


    LibHermes::Struct result;
    result.emplace(LibHermes::StructKey{"checkdate"}, LibHermes::StructValue{checkdate});
    result.emplace(LibHermes::StructKey{"domain"}, LibHermes::StructValue{*domain});
    result.emplace(LibHermes::StructKey{"owner"},  LibHermes::StructValue{*owner});
    if (!is_nullopt(nsset))
    {
        result.emplace(LibHermes::StructKey{"nsset"},  LibHermes::StructValue{**nsset});
    }
    if (!is_nullopt(valdate) && !(**valdate).is_special())
    {
        result.emplace(LibHermes::StructKey{"valdate"}, LibHermes::StructValue{to_iso_extended_string(**valdate)});
    }
    if (!is_nullopt(exdate) && !(**exdate).is_special())
    {
        result.emplace(LibHermes::StructKey{"exdate"}, LibHermes::StructValue{to_iso_extended_string(**exdate)});
    }
    if (!is_nullopt(dnsdate) && !(**dnsdate).is_special())
    {
        result.emplace(LibHermes::StructKey{"dnsdate"}, LibHermes::StructValue{to_iso_extended_string(**dnsdate)});
    }
    if (!is_nullopt(exregdate) && !(**exregdate).is_special())
    {
        result.emplace(LibHermes::StructKey{"exregdate"}, LibHermes::StructValue{to_iso_extended_string(**exregdate)});
        const auto day_before_exregdate = **exregdate - boost::gregorian::date_duration(1);
        result.emplace(LibHermes::StructKey{"day_before_exregdate"}, LibHermes::StructValue{to_iso_extended_string(day_before_exregdate)});
    }

    if (!is_nullopt(zone))
    {
        result.emplace(LibHermes::StructKey{"zone"}, LibHermes::StructValue{**zone});
    }
    else
    {
        LIBLOG_INFO("unexpected: missing record in table zone");
    }

    result.emplace(LibHermes::StructKey{"statechangedate"}, LibHermes::StructValue{to_iso_extended_string((*_valid_from).date())});

    result.emplace(LibHermes::StructKey{"registrar"}, LibHermes::StructValue{registrar});

    const auto administrators = collect_domain_template_parameters_history_admin(_tx, _object_id);
    merge_into(result, administrators);

    return result;
}

int to_email_template_id(ObjectType _object_type)
{
    switch (_object_type)
    {
        case ObjectType::contact: return 1;
        case ObjectType::domain: return 3;
        case ObjectType::keyset: return 4;
        case ObjectType::nsset: return 2;
    }
    throw std::logic_error{"unexpected value of ObjectType"};
}

LibHermes::Reference::Type to_libhermes_reference_type(ObjectType _object_type)
{
    switch (_object_type)
    {
        case ObjectType::contact:
            return LibHermes::Reference::Type{"contact"};
        case ObjectType::domain:
            return LibHermes::Reference::Type{"domain"};
        case ObjectType::nsset:
            return LibHermes::Reference::Type{"nsset"};
        case ObjectType::keyset:
            return LibHermes::Reference::Type{"keyset"};
    }
    throw std::logic_error("unexpected value of ObjectType");
}

std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> to_libhermes_email_recipients_filter(const std::set<Recipient>& _recipients)
{
    std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> result;
    for (const auto& recipient : _recipients)
    {
        if (recipient.notification_contact_type == NotificationContactType::email_address)
        {
            if (recipient.uuid != ContactUuid::nullopt)
            {
                for (const auto& notification_contact : recipient.notification_contacts)
                {
                    result[LibHermes::Email::RecipientEmail{*notification_contact}].insert(LibHermes::Email::RecipientUuid{**recipient.uuid});
                }
            }
            else
            {
                for (const auto& notification_contact : recipient.notification_contacts)
                {
                    result[LibHermes::Email::RecipientEmail{*notification_contact}]; // create the record if does not exist yet
                }
            }
        }
    }
    return result;
}

bool is_sms_recipient(const Recipient& _recipient)
{
    return _recipient.notification_contact_type == NotificationContactType::phone_number;
}

struct EmailMessage
{
    LibHermes::Email::Email data;
    Flag flag;
    StateId state_id;
    ObjectUuid object_uuid;

    ObjectId object_id;
    ObjectType object_type;
    RecipientGroup recipient_group;
};

struct SmsMessage
{
    LibHermes::Sms::MessageData data;
    Flag flag;
    StateId state_id;
    ObjectUuid object_uuid;

    ObjectId object_id;
    ObjectType object_type;
    RecipientGroup recipient_group;
};

void create_configuration_table(
        const LibPg::PgRwTransaction& _tx,
        const Notifications& _notifications)
{
    exec(_tx, LibPg::make_query() <<
            "CREATE TEMPORARY TABLE tmp_configuration ("
            "object_state_id INTEGER NOT NULL, "
            "message_type notification.MESSAGE_TYPE NOT NULL, "
            // email_address
            "mail_type TEXT NOT NULL, "
            "mail_template_subject TEXT NOT NULL, "
            "mail_template_body TEXT NOT NULL, "
            // phone_number
            "sms_type TEXT NOT NULL, "
            "sms_template_body TEXT NOT NULL, "
            //
            "recipient_group notification.RECIPIENT_GROUP NOT NULL, "
            "object_type INTEGER NOT NULL)");

    for (const auto& notification : _notifications)
    {
        const auto object_type = std::get<ObjectType>(notification.first);
        const auto flag = std::get<Flag>(notification.first);

        for (const auto& contact_type_with_recipients : notification.second)
        {
            const auto message_type = contact_type_with_recipients.first;
            const auto recipients_groups = contact_type_with_recipients.second;

            for (const auto& recipient_group : recipients_groups)
            {
                LIBLOG_INFO("{} | {} | {}", to_string(object_type), *flag, to_string(recipient_group));

                exec(_tx, LibPg::make_query() <<
                        "INSERT INTO tmp_configuration ("
                            "object_state_id, "
                            "message_type, "
                            // email_address
                            "mail_type, "
                            "mail_template_subject, "
                            "mail_template_body, "
                            // phone_number
                            "sms_type, "
                            "sms_template_body, "
                            //
                            "recipient_group, "
                            "object_type) "
                        "VALUES ("
                            "(SELECT id FROM enum_object_states WHERE name = " << LibPg::parameter<Flag>().as_text() << "), " <<
                            LibPg::parameter<NotificationMessageType>().as_type("notification.MESSAGE_TYPE") << ", " <<
                            LibPg::parameter<MailType>().as_text() << ", " <<
                            LibPg::parameter<MailTemplateSubject>().as_text() << ", " <<
                            LibPg::parameter<MailTemplateBody>().as_text() << ", " <<
                            LibPg::parameter<SmsType>().as_text() << ", " <<
                            LibPg::parameter<SmsTemplateBody>().as_text() << ", " <<
                            LibPg::parameter<RecipientGroup>().as_type("notification.RECIPIENT_GROUP") << ", " <<
                            "(SELECT id FROM enum_object_type WHERE name = " << LibPg::parameter<ObjectType>().as_text() << "))",
                        {flag,
                         from_string<NotificationMessageType>(message_type.value),
                         MailType{message_type.properties.at("mail_type")},
                         MailTemplateSubject{message_type.properties.at("mail_template_subject")},
                         MailTemplateBody{message_type.properties.at("mail_template_body")},
                         SmsType{message_type.properties.at("sms_type")},
                         SmsTemplateBody{message_type.properties.at("sms_template_body")},
                         recipient_group,
                         object_type});
            }
        }
    }
}

void get_messages(
        const Notifications& _notifications,
        const StateAgeDaysMax& _state_age_days_max,
        const NotificationsMax& _notifications_max,
        std::vector<EmailMessage>& _email_messages,
        std::vector<SmsMessage>& _sms_messages)
{
    auto tx = Util::make_rw_transaction();

    create_configuration_table(tx, _notifications);

    const auto query = LibPg::make_query() <<
            // clang-format off
            "SELECT" << LibPg::item<StateId>() << "os.id" <<
                        LibPg::item<ValidFrom>() << "os.valid_from" <<
                        LibPg::item<ObjectId>() << "oreg.id" <<
                        LibPg::item<ObjectUuid>() << "oreg.uuid" <<
                        LibPg::item<ObjectType>() << "eot.name" <<
                        LibPg::item<Flag>() << "eos.name" <<
                        LibPg::item<Handle>() << "oreg.name" <<
                        LibPg::item<MailType>() << "tc.mail_type " <<
                        LibPg::item<MailTemplateSubject>() << "tc.mail_template_subject " <<
                        LibPg::item<MailTemplateBody>() << "tc.mail_template_body " <<
                        LibPg::item<SmsType>() << "tc.sms_type " <<
                        LibPg::item<SmsTemplateBody>() << "tc.sms_template_body " <<
                        LibPg::item<RecipientGroup>() << "tc.recipient_group "
              "FROM object_state os "
              "JOIN enum_object_states eos ON eos.id = os.state_id "
              "JOIN object_registry oreg ON oreg.id = os.object_id "
              "JOIN enum_object_type eot ON eot.id = oreg.type "
              "JOIN tmp_configuration tc ON (tc.object_state_id = os.state_id AND tc.object_type = oreg.type) "
             "WHERE os.valid_to IS NULL "
               "AND (NOW()::DATE - os.valid_from::DATE) <= " << LibPg::parameter<StateAgeDaysMax>().as_integer() << " "
               "AND NOT EXISTS ("
                      "SELECT 0 FROM notification.object_state_changes nosc "
                       "WHERE nosc.object_state_id = os.id "
                         "AND nosc.recipient_group = tc.recipient_group "
                         "AND nosc.message_type = tc.message_type) "
             "ORDER BY os.id " <<
             (*_notifications_max != 0 ?  "LIMIT " :  "OFFSET ") <<
              LibPg::parameter<NotificationsMax>().as_integer();
            // clang-format on

    const auto dbres = exec(tx, query, {_state_age_days_max, _notifications_max});

    for (const auto& row : dbres)
    {
        const auto state_id = row.get<StateId>();
        const auto valid_from = row.get<ValidFrom>();
        const auto object_id = row.get<ObjectId>();
        const auto object_uuid = row.get<ObjectUuid>();
        const auto object_type = row.get<ObjectType>();
        const auto flag = row.get<Flag>();
        const auto handle = row.get<Handle>();
        const auto mail_type = row.get<MailType>();
        const auto mail_template_subject = row.get<MailTemplateSubject>();
        const auto mail_template_body = row.get<MailTemplateBody>();
        const auto sms_type = row.get<SmsType>();
        const auto sms_template_body = row.get<SmsTemplateBody>();
        const auto recipient_group = row.get<RecipientGroup>();

        LIBLOG_INFO("processing event: valid_from {}, state_id {}, object type {}. flag {}, uuid {}, recipient_group {}", to_iso_extended_string(*valid_from), *state_id, to_string(object_type), *flag, boost::uuids::to_string(*object_uuid), to_string(recipient_group));

        const auto recipients =
                [&](ObjectType object_type, const RecipientGroup& recipient_group) -> std::set<Recipient>{
                    switch (object_type)
                    {
                        case ObjectType::contact:
                                switch (recipient_group)
                                {
                                    case RecipientGroup::additional:
                                        return {};
                                    default:
                                        return get_recipients(tx, ContactId{*object_id}, recipient_group);
                                }
                        case ObjectType::nsset:
                                switch (recipient_group)
                                {
                                    case RecipientGroup::additional:
                                        return {};
                                    default:
                                        return get_recipients(tx, NssetId{*object_id}, recipient_group);
                                }
                        case ObjectType::domain:
                                switch (recipient_group)
                                {
                                    case RecipientGroup::additional:
                                        return get_recipients_additional(tx, DomainId{*object_id}, state_id);
                                    case RecipientGroup::generic:
                                        return get_recipients_domain_generic(handle);
                                    default:
                                        return get_recipients(tx, DomainId{*object_id}, recipient_group);
                                }
                        case ObjectType::keyset:
                                switch (recipient_group)
                                {
                                    case RecipientGroup::additional:
                                        return {};
                                    default:
                                        return get_recipients(tx, KeysetId{*object_id}, recipient_group);
                                }
                    }
                    throw std::logic_error("unexpected ObjectType");
                }(object_type, recipient_group);

        if (recipients.empty())
        {
            LIBLOG_WARNING("object of type {} has no recipients", to_string(object_type));
            continue;
        }

        {
            LibHermes::Struct template_parameters{
                {LibHermes::StructKey{"handle"},
                 LibHermes::StructValue{*handle}},
                {LibHermes::StructKey{"type"},
                 LibHermes::StructValue{to_email_template_id(object_type)}},
                {LibHermes::StructKey{"deldate"},
                 LibHermes::StructValue{to_iso_extended_string(boost::gregorian::date(boost::gregorian::day_clock::local_day()))}}};

            if (object_type == ObjectType::domain)
            {
                const auto domain_template_parameters =
                        collect_domain_template_parameters_history(tx, object_id, valid_from);
                merge_into(template_parameters, domain_template_parameters);
            }

            const auto libhermes_email_recipients = to_libhermes_email_recipients_filter(recipients);
            if (!libhermes_email_recipients.empty())
            {
                auto message_data =
                        LibHermes::Email::make_minimal_email(
                                libhermes_email_recipients,
                                LibHermes::Email::SubjectTemplate{*mail_template_subject},
                                LibHermes::Email::BodyTemplate{*mail_template_body});
                message_data.type = LibHermes::Email::Type{*mail_type};
                message_data.context = template_parameters;

                _email_messages.push_back(EmailMessage{std::move(message_data), flag, state_id, object_uuid, object_id, object_type, recipient_group});
            }
        }

        {
            LibHermes::Struct template_parameters{
                {LibHermes::StructKey{"handle"},
                 LibHermes::StructValue{*handle}},
                {LibHermes::StructKey{"type"},
                 LibHermes::StructValue{to_email_template_id(object_type)}},
                {LibHermes::StructKey{"deldate"},
                 LibHermes::StructValue{to_iso_extended_string(boost::gregorian::date(boost::gregorian::day_clock::local_day()))}}};

            if (object_type == ObjectType::domain)
            {
                const auto domain_template_parameters =
                        collect_domain_template_parameters_history(tx, object_id, valid_from);
                merge_into(template_parameters, domain_template_parameters);
            }

            for (const auto recipient : recipients)
            {
                if (is_sms_recipient(recipient))
                {
                    for (const auto& phone_number : recipient.notification_contacts)
                    {
                        auto message_data =
                                LibHermes::Sms::make_minimal_message(
                                        LibHermes::Sms::RecipientPhoneNumber{*phone_number},
                                        LibHermes::Sms::BodyTemplate{*sms_template_body});
                        message_data.type = LibHermes::Sms::Type{*sms_type};
                        message_data.context = template_parameters;

                        _sms_messages.push_back(SmsMessage{std::move(message_data), flag, state_id, object_uuid, object_id, object_type, recipient_group});
                    }
                }
            }
        }
    }

    commit(std::move(tx));
}

void save_confirmation(
        const LibPg::PgRwTransaction& _tx_rw,
        const StateId& _state_id,
        const RecipientGroup& _recipient_group,
        const NotificationMessageType& _message_type,
        bool _on_conflict_do_nothing = false)
{
    const auto query = LibPg::make_query() <<
           "INSERT INTO notification.object_state_changes "
           "(object_state_id, recipient_group, message_type) "
           "VALUES (" << LibPg::parameter<StateId>().as_integer() << ", " <<
                         LibPg::parameter<RecipientGroup>().as_type("notification.RECIPIENT_GROUP") << ", " <<
                         LibPg::parameter<NotificationMessageType>().as_type("notification.MESSAGE_TYPE") << ")" <<
            (_on_conflict_do_nothing ? " ON CONFLICT DO NOTHING" : "");
    exec(_tx_rw, query, {_state_id, _recipient_group, _message_type});
}

} // namespace Fred::Notify::ObjectStateChanges::Impl::{anonymous}

void notify_object_state_changes(
        const Notifications& _notifications,
        const StateAgeDaysMax& _state_age_days_max,
        const NotificationsMax& _notifications_max,
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive)
{
    try
    {
        std::vector<EmailMessage> email_messages;
        std::vector<SmsMessage> sms_messages;
        get_messages(_notifications, _state_age_days_max, _notifications_max, email_messages, sms_messages);

        auto conn_ptr = std::make_unique<LibPg::PgConnection>(Util::make_rw_connection());

        {
        LibHermes::Connection<LibHermes::Service::EmailMessenger> connection{
            LibHermes::Connection<LibHermes::Service::EmailMessenger>::ConnectionString{
                *_messenger_endpoint}};

        for (const auto& message : email_messages)
        {
            try
            {
                LIBLOG_INFO("Sending email about object with uuid {}", boost::uuids::to_string(*message.object_uuid));
                LibHermes::Email::batch_send(
                        connection,
                        message.data,
                        LibHermes::Email::Archive{*_messenger_archive},
                        {LibHermes::Reference{
                                to_libhermes_reference_type(message.object_type),
                                LibHermes::Reference::Value{boost::uuids::to_string(*message.object_uuid)}}});
            }
            catch (const LibHermes::Email::SendFailed& e)
            {
                LIBLOG_WARNING("gRPC exception caught while sending email about object with uuid {}: gRPC error code: {}, error message: {}, grpc_message_json: {}",
                        boost::uuids::to_string(*message.object_uuid), e.error_code(), e.error_message(), e.grpc_message_json());
                continue;
            }
            catch (const std::exception& e)
            {
                LIBLOG_WARNING("std::exception caught while sending email about object with uuid {}: {}", boost::uuids::to_string(*message.object_uuid), e.what());
                continue;
            }
            catch (...)
            {
                LIBLOG_WARNING("exception caught while sending email about object with uuid {}", boost::uuids::to_string(*message.object_uuid));
                continue;
            }

            auto tx = LibPg::PgRwTransaction{std::move(*(conn_ptr.release()))};
            save_confirmation(tx, message.state_id, message.recipient_group, NotificationMessageType::email);
            conn_ptr = std::make_unique<LibPg::PgConnection>(commit(std::move(tx)));
        }
        }

        {
        LibHermes::Connection<LibHermes::Service::SmsMessenger> connection{
            LibHermes::Connection<LibHermes::Service::SmsMessenger>::ConnectionString{
                *_messenger_endpoint}};

        for (const auto& message : sms_messages)
        {
            try
            {
                LIBLOG_INFO("Sending sms about object with uuid {}", boost::uuids::to_string(*message.object_uuid));
                LibHermes::Sms::send(
                        connection,
                        message.data,
                        LibHermes::Sms::Archive{*_messenger_archive},
                        {LibHermes::Reference{
                                to_libhermes_reference_type(message.object_type),
                                LibHermes::Reference::Value{boost::uuids::to_string(*message.object_uuid)}}});
            }
            catch (const LibHermes::Sms::SendFailed& e)
            {
                LIBLOG_WARNING("gRPC exception caught while sending sms about object with uuid {}: gRPC error code: {}, error message: {}, grpc_message_json: {}",
                        boost::uuids::to_string(*message.object_uuid), e.error_code(), e.error_message(), e.grpc_message_json());
                continue;
            }
            catch (const std::exception& e)
            {
                LIBLOG_WARNING("std::exception caught while sending sms about object with uuid {}: {}", boost::uuids::to_string(*message.object_uuid), e.what());
                continue;
            }
            catch (...)
            {
                LIBLOG_WARNING("exception caught while sending sms about object with uuid {}", boost::uuids::to_string(*message.object_uuid));
                continue;
            }

            auto tx = LibPg::PgRwTransaction{std::move(*(conn_ptr.release()))};
            auto on_conflict_do_nothing = true;
            save_confirmation(tx, message.state_id, message.recipient_group, NotificationMessageType::sms, on_conflict_do_nothing);
            conn_ptr = std::make_unique<LibPg::PgConnection>(commit(std::move(tx)));
        }
        }
    }
    catch (const LibPg::ExecFailure& e)
    {
        LIBLOG_WARNING("SQL exec failure: {}", e.what());
        throw;
    }
    catch (const LibPg::ConnectFailure& e)
    {
        LIBLOG_WARNING("DB connect failure: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_WARNING("std::exception caught: {}", e.what());
        throw;
    }
}

} // namespace Fred::Notify::ObjectStateChanges::Impl
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred
