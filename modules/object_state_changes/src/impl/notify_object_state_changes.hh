/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFY_OBJECT_STATE_CHANGES_HH_4AB20E7EF7304E72A51268EC2281A4EA
#define NOTIFY_OBJECT_STATE_CHANGES_HH_4AB20E7EF7304E72A51268EC2281A4EA

#include "src/impl/strong_type.hh"

#include <map>
#include <tuple>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Impl {

enum struct ObjectType
{
    contact,
    nsset,
    domain,
    keyset
};

using Flag = StrongString<struct FlagTag_>;
using MailType = StrongString<struct MailTypeTag_>;
using MailTemplateSubject = StrongString<struct MailTemplateSubjectTag_>;
using MailTemplateBody = StrongString<struct MailTemplateBodyTag_>;
using SmsType = StrongString<struct SmsTypeTag_>;
using SmsTemplateBody = StrongString<struct TemplateBodyTag_>;

enum struct RecipientGroup
{
    admin,
    tech,
    generic,
    additional
};

using State = std::tuple<ObjectType, Flag>;

template <typename T>
T from_string(const std::string& _str);

template<>
ObjectType from_string<ObjectType>(const std::string& _str);

template<>
RecipientGroup from_string<RecipientGroup>(const std::string& _str);

struct MessageType
{
    std::string value;
    std::map<std::string, std::string> properties;

    bool operator<(const MessageType& _other) const;
};

using Notifications = std::map<
        State,
        std::map<MessageType, std::vector<RecipientGroup>>>;

using MessengerEndpoint = StrongString<struct MessengerEndpointTag_>;
using MessengerArchive = LibStrong::Type<bool, struct MessengerEndpointTag_>;
using StateAgeDaysMax = LibStrong::Type<unsigned int, struct StateAgeDaysMaxTag_>;
using NotificationsMax = LibStrong::Type<unsigned int, struct NotificationsMaxTag_>;

void notify_object_state_changes(
        const Notifications& _notifications,
        const StateAgeDaysMax& _state_age_days_max,
        const NotificationsMax& _notifications_max,
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive);

} // namespace Fred::Notify::ObjectStateChanges::Impl
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
