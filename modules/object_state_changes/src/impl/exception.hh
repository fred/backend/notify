/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef EXCEPTION_HH_84D709BFD23B46AC9036FC92D5BF4900
#define EXCEPTION_HH_84D709BFD23B46AC9036FC92D5BF4900

#include <exception>
#include <string>

namespace Fred {
namespace Notify {
namespace ObjectStateChanges {
namespace Impl {

struct Exception : std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    const std::string msg_;
};

struct InvalidUuid : Exception
{
    explicit InvalidUuid(std::string _msg);
};

struct InvalidFlag : Exception
{
    explicit InvalidFlag(std::string _msg);
};

struct InvalidObjectType : Exception
{
    explicit InvalidObjectType(std::string _msg);
};

struct InvalidRecipientGroup : Exception
{
    explicit InvalidRecipientGroup(std::string _msg);
};

struct InvalidNotificationContactType : Exception
{
    explicit InvalidNotificationContactType(std::string _msg);
};

} // namespace Fred::Notify::ObjectStateChanges::Impl
} // namespace Fred::Notify::ObjectStateChanges
} // namespace Fred::Notify
} // namespace Fred

#endif
