CREATE TEMPORARY TABLE tmp_mail_template_messenger_migration
(
    mail_type TEXT NOT NULL,
    version INTEGER NOT NULL,
    subject TEXT NOT NULL CHECK (REPLACE(subject, ' ', '') = subject AND LENGTH(subject) > 0),
    subject_uuid UUID NOT NULL,
    body TEXT NOT NULL CHECK (REPLACE(body, ' ', '') = body AND LENGTH(body) > 0),
    body_uuid UUID NOT NULL
);

\COPY tmp_mail_template_messenger_migration (mail_type, version, subject, subject_uuid, body, body_uuid) FROM 'mail_template_messenger_migration_data.csv' WITH CSV DELIMITER ',' HEADER

SELECT json_build_object(
    'notifications',
    (SELECT array_to_json(ARRAY(
        SELECT json_build_object(
           'objects', json_agg(DISTINCT(eot.name)),
           'flags', json_agg(DISTINCT(eos.name)),
           'contact_type', (SELECT json_build_object(
                'value', 'email_address'::notification.CONTACT_TYPE,
                'properties', (SELECT json_build_object(
                   'mail_template_subject', json_agg(DISTINCT(
                    SELECT subject
                      FROM tmp_mail_template_messenger_migration mtmm
                     WHERE mtmm.mail_type = mt.name
                     ORDER BY mtmm.version DESC LIMIT 1))->>0,
                   'mail_template_body', json_agg(DISTINCT(
                    SELECT body
                      FROM tmp_mail_template_messenger_migration mtmm
                     WHERE mtmm.mail_type = mt.name
                     ORDER BY mtmm.version DESC LIMIT 1))->>0,
                   'mail_type', mt.name)))),
           'recipients', json_agg(
           CASE
               WHEN nsm.emails = 1 THEN 'admin'
               WHEN nsm.emails = 2 THEN 'tech'
               WHEN nsm.emails = 3 THEN 'generic'
               WHEN nsm.emails = 4 THEN 'additional'
           END)) as configuration
      FROM notify_statechange_map nsm
      JOIN enum_object_states eos ON eos.id = nsm.state_id
      JOIN enum_object_type eot ON eot.id = nsm.obj_type
      JOIN mail_type mt ON mt.id = nsm.mail_type_id
     GROUP BY eot.name, eos.name, mt.name), TRUE)));
