/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/notify_object_state_changes.hh"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TestObjectStateChanges)

BOOST_AUTO_TEST_CASE(enums)
{
    using namespace Fred::Notify::ObjectStateChanges::Impl;

    BOOST_CHECK_EQUAL(ObjectType::contact, from_string<ObjectType>("contact"));
    BOOST_CHECK_EQUAL(ObjectType::nsset, from_string<ObjectType>("nsset"));
    BOOST_CHECK_EQUAL(ObjectType::domain, from_string<ObjectType>("domain"));
    BOOST_CHECK_EQUAL(ObjectType::keyset, from_string<ObjectType>("keyset"));

    BOOST_CHECK_EQUAL(RecipientGroup::admin, from_string<RecipientGroup>("admin"));
    BOOST_CHECK_EQUAL(RecipientGroup::tech, from_string<RecipientGroup>("tech"));
    BOOST_CHECK_EQUAL(RecipientGroup::generic, from_string<RecipientGroup>("generic"));
    BOOST_CHECK_EQUAL(RecipientGroup::additional, from_string<RecipientGroup>("additional"));
};

BOOST_AUTO_TEST_SUITE_END() // TestObjectStateChanges
