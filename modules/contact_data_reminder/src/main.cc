/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg.hh"

#include "src/impl/notify_contact_data_reminder.hh"
#include "src/util/set_logging.hh"

#include "liblog/liblog.hh"

namespace Cfg = Fred::Notify::ContactDataReminder::Cfg;
namespace Util = Fred::Notify::ContactDataReminder::Util;
namespace Impl = Fred::Notify::ContactDataReminder::Impl;

int main(int argc, char** argv)
{
    Cfg::handle_cli_args(argc, argv);

    try
    {
        Util::set_logging(Cfg::Options::get().log);
        LIBLOG_SET_CONTEXT(Ctx, log_ctx, __func__);
        LIBLOG_INFO("fred-notify-contact-data-reminder configured");

        Impl::notify_contact_data_reminder(
                Impl::NotifyConfiguration{
                        Impl::ReminderDate{*Cfg::Options::get().reminder_date},
                        Impl::NotifyValidationAgeYearsLimit{Cfg::Options::get().notify.validation_age_years_limit}},
                Impl::MessengerEndpoint{Cfg::Options::get().messenger.endpoint},
                Impl::MessengerArchive{Cfg::Options::get().messenger.archive},
                Impl::TemplateNameEmailSubject{Cfg::Options::get().messenger.template_name_email_subject},
                Impl::TemplateNameEmailBody{Cfg::Options::get().messenger.template_name_email_body});
    }
    catch (...)
    {
    }
}
