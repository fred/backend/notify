/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "config.h"

#include "src/cfg.hh"
#include "src/cfg/util/terminal_width.hh"
#include "src/impl/exception.hh"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/program_options.hpp>

#include <syslog.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <unordered_map>
#include <utility>
#include <vector>

#ifndef DEFAULT_CONFIG_FILE
#define DEFAULT_CONFIG_FILE "fred-notify-contact-data-reminder.conf"
#endif

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

namespace {

decltype(auto) make_command_line_only_options(
        boost::optional<std::string>& _configl_file_name,
        boost::optional<boost::gregorian::date>& _reminder_date)
{
    boost::program_options::options_description command_line_only_options{"Options only available on command line", Util::terminal_width};
    const auto set_config_file_name = [&](const std::string& file_name) { _configl_file_name = file_name; };
    const auto set_reminder_date =
            [&](const std::string& reminder_date_as_string)
            {
                try
                {
                    _reminder_date = boost::gregorian::from_string(reminder_date_as_string);
                }
                catch (...)
                {
                    auto exception = boost::program_options::validation_error(boost::program_options::validation_error::invalid_option_value);
                    exception.m_error_template += "; invalid date format (expected [YYYY-MM-DD])";
                    throw exception;
                }
                if (_reminder_date > boost::gregorian::day_clock::universal_day()) {
                    auto exception = boost::program_options::validation_error(boost::program_options::validation_error::invalid_option_value);
                    exception.m_error_template += "; date in future is not allowed";
                    throw exception;
                }
            };
    command_line_only_options.add_options()
        ("help,h", "produce help message")
        ("version,V", "display version information")
        ("config,c",
#ifdef DEFAULT_CONFIG_FILE
         boost::program_options::value<std::string>()->default_value(std::string{DEFAULT_CONFIG_FILE})->notifier(set_config_file_name),
#else
         boost::program_options::value<std::string>()->notifier(set_config_file_name),

#endif
         "configuration file")
        ("date,d",
         boost::program_options::value<std::string>()->default_value(to_iso_extended_string(boost::gregorian::day_clock::universal_day()))->notifier(set_reminder_date),
         "specific date [YYYY-MM-DD]"); // TODO improve description
    return command_line_only_options;
}

void collect_log_options(
    const LogOptions& _log_options,
    const LogConsoleOptions& _log_console_options,
    const LogFileOptions& _log_file_options,
    const LogSyslogOptions& _log_syslog_options,
    Log& log)
{
    log.log_grpc_library_events =
            (_log_options.log_grpc_library_events != boost::none) &&
            *_log_options.log_grpc_library_events;

    static constexpr auto default_log_min_severity = LibLog::Level::critical;
    const auto common_log_min_severity =
            _log_options.min_severity != boost::none
                    ? *_log_options.min_severity
                    : default_log_min_severity;
    std::for_each(
        _log_options.devices.begin(),
        _log_options.devices.end(),
        [&](LogOptions::Device device)
        {
            switch (device)
            {
                case LogOptions::Device::console:
                {
                    LogConsole log_console;
                    log_console.min_severity = _log_console_options.min_severity != boost::none ? *_log_console_options.min_severity
                                                                                           : common_log_min_severity;
                    static constexpr auto default_output_stream = LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr;
                    log_console.output_stream = _log_console_options.output_stream != boost::none ? *_log_console_options.output_stream
                                                                                             : default_output_stream;
                    static constexpr auto default_color_mode = LibLog::ColorMode::never;
                    log_console.color_mode = _log_console_options.color_mode != boost::none ? *_log_console_options.color_mode
                                                                                       : default_color_mode;
                    log.log_console = log_console;
                    return;
                }
                case LogOptions::Device::file:
                {
                    if (_log_file_options.file_name == boost::none)
                    {
                        throw MissingOption{"missing option: 'log.file.file_name'"};
                    }
                    LogFile log_file;
                    log_file.file_name = *_log_file_options.file_name;
                    log_file.min_severity = _log_file_options.min_severity != boost::none ? *_log_file_options.min_severity
                                                                                         : common_log_min_severity;
                    log.log_file = log_file;
                    return;
                }
                case LogOptions::Device::syslog:
                {
                    LogSyslog log_syslog;
                    static const std::string default_ident = "";
                    log_syslog.ident =
                            _log_syslog_options.ident != boost::none
                                    ? *_log_syslog_options.ident
                                    : default_ident;
                    static constexpr int default_facility = LOG_USER;
                    static const auto make_facility = [](int offset)
                    {
                        switch (offset)
                        {
                            case 0: return LOG_LOCAL0;
                            case 1: return LOG_LOCAL1;
                            case 2: return LOG_LOCAL2;
                            case 3: return LOG_LOCAL3;
                            case 4: return LOG_LOCAL4;
                            case 5: return LOG_LOCAL5;
                            case 6: return LOG_LOCAL6;
                            case 7: return LOG_LOCAL7;
                        }
                        return default_facility;
                    };
                    log_syslog.facility =
                            _log_syslog_options.facility_local_offset != boost::none
                                    ? make_facility(*_log_syslog_options.facility_local_offset)
                                    : default_facility;
                    log_syslog.min_severity =
                            _log_syslog_options.min_severity != boost::none
                                    ? *_log_syslog_options.min_severity
                                    : common_log_min_severity;
                    static constexpr int default_options = LOG_CONS | LOG_ODELAY;
                    log_syslog.options = default_options;
                    log.log_syslog = log_syslog;
                    return;
                }
            }
            throw Exception("Invalid device");
        });
}

constexpr int fake_argc = -1;
constexpr const char* const fake_argv[] = { nullptr };

} // namespace Fred::Notify::ContactDataReminder::Cfg::{anonymous}

const Options& Options::get()
{
    return init(fake_argc, fake_argv);
}

const Options& Options::init(int _argc, const char* const* _argv)
{
    static const Options* singleton_ptr = nullptr;
    const bool init_requested = (_argc != fake_argc) || (_argv != fake_argv);
    const bool first_run = singleton_ptr == nullptr;
    if (first_run)
    {
        if (!init_requested)
        {
            throw std::runtime_error("First call of Cfg::Options::init must contain valid arguments");
        }
        static const Options singleton(_argc, _argv);
        singleton_ptr = &singleton;
    }
    else if (init_requested)
    {
        throw std::runtime_error("Only first call of Cfg::Options::init can contain valid arguments");
    }
    return *singleton_ptr;
}

Options::Options(int _argc, const char* const* _argv)
{
    LogOptions _log_options;
    LogConsoleOptions _log_console_options;
    LogFileOptions _log_file_options;
    LogSyslogOptions _log_syslog_options;

    const boost::program_options::options_description command_line_only_options = make_command_line_only_options(config_file_name, reminder_date);
    const boost::program_options::options_description database_options = make_database_options(database);
    const boost::program_options::options_description messenger_options = make_messenger_options(messenger);
    const boost::program_options::options_description notify_options = make_notify_options(notify);
    const boost::program_options::options_description log_options = make_log_options(_log_options);
    const boost::program_options::options_description log_console_options = make_log_console_options(_log_console_options);
    const boost::program_options::options_description log_file_options = make_log_file_options(_log_file_options);
    const boost::program_options::options_description log_syslog_options = make_log_syslog_options(_log_syslog_options);

    boost::program_options::options_description command_line_options{"fred-notify-contact-data-reminder options", Util::terminal_width};
    command_line_options
        .add(command_line_only_options)
        .add(database_options)
        .add(messenger_options)
        .add(notify_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options);

    boost::program_options::options_description config_file_options;
    config_file_options
        .add(database_options)
        .add(messenger_options)
        .add(notify_options)
        .add(log_options)
        .add(log_console_options)
        .add(log_file_options)
        .add(log_syslog_options);

    boost::program_options::variables_map variables_map;
    const auto has_option = [&](const char* option) { return 0 < variables_map.count(option); };
    try
    {
        boost::program_options::store(
                boost::program_options::command_line_parser(_argc, _argv)
                    .options(command_line_options)
                    .run(),
                variables_map);
    }
    catch (const boost::program_options::unknown_option& unknown_option)
    {
        std::ostringstream out;
        out << unknown_option.what() << "\n\n" << command_line_options;
        throw UnknownOption(out.str());
    }
    boost::program_options::notify(variables_map);

    if (has_option("help"))
    {
        std::ostringstream out;
        out << command_line_options;
        throw AllDone(out.str());
    }
    if (has_option("version"))
    {
        std::ostringstream out;
        out << "Application Version: " << PACKAGE_VERSION << std::endl;
        out << "LibLog Version: " << LIBLOG_VERSION << std::endl;
        out << "LibPg Version: " << LIBPG_VERSION << std::endl;
        out << "LibStrong Version: " << LIBSTRONG_VERSION << std::endl;
        throw AllDone(out.str());
    }
    const bool config_file_name_presents = config_file_name != boost::none;
    if (config_file_name_presents)
    {
        std::ifstream config_file{*config_file_name};
        if (!config_file)
        {
            throw Exception("can not open config file \"" + *config_file_name + "\"");
        }
        try
        {
            boost::program_options::store(
                    boost::program_options::parse_config_file(
                            config_file,
                            config_file_options),
                    variables_map);
        }
        catch (const boost::program_options::unknown_option& unknown_option)
        {
            std::ostringstream out;
            out << unknown_option.what() << "\n\n" << command_line_options;
            throw UnknownOption(out.str());
        }
        boost::program_options::notify(variables_map);
    }

    collect_log_options(
        _log_options,
        _log_console_options,
        _log_file_options,
        _log_syslog_options,
        log);

    const auto required = [&](const char* variable)
    {
        if (variables_map.count(variable) == 0)
        {
            throw MissingOption{"missing option: '" + std::string{variable} + "'"};
        }
    };

    required("messenger.endpoint");
    required("messenger.template_name_email_subject");
    required("messenger.template_name_email_body");
}

AllDone::AllDone(std::string msg) noexcept
    : msg_{std::move(msg)}
{
}

const char* AllDone::what() const noexcept
{
    return msg_.c_str();
}

void handle_cli_args(int _argc, char* _argv[])
{
    try
    {
        Options::init(_argc, _argv);
    }
    catch (const AllDone& e)
    {
        std::cout << e.what() << std::endl;
        exit(EXIT_SUCCESS);
    }
    catch (const UnknownOption& e)
    {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (const MissingOption& e)
    {
        std::cerr << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (const Exception& e)
    {
        std::cerr << "Exception caught: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (const std::exception& e)
    {
        std::cerr << "std::exception caught: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
    catch (...)
    {
        std::cerr << "Unexpected exception caught" << std::endl;
        exit(EXIT_FAILURE);
    }
}

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
