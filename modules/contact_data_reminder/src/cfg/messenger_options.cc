/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg/messenger_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

boost::program_options::options_description make_messenger_options(MessengerOptions& _messenger_options)
{
    boost::program_options::options_description messenger_options{"Common messenger access options", Util::terminal_width};
    const auto set_endpoint = [&](const std::string& endpoint) { _messenger_options.endpoint = endpoint; };
    const auto set_archive = [&](const bool archive) { _messenger_options.archive = archive; };
    const auto set_template_name_email_subject = [&](const std::string& template_name_email_subject) { _messenger_options.template_name_email_subject = template_name_email_subject; };
    const auto set_template_name_email_body = [&](const std::string& template_name_email_body) { _messenger_options.template_name_email_body = template_name_email_body; };
    messenger_options.add_options()
        ("messenger.endpoint", boost::program_options::value<std::string>()->notifier(set_endpoint), "URI of Fred.Api.Messenger.Email service to connect to")
        ("messenger.archive", boost::program_options::value<bool>()->notifier(set_archive), "archive the message")
        ("messenger.template_name_email_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_subject), "name of the template used for the message subject")
        ("messenger.template_name_email_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_body), "name of the template used for the message body");
    return messenger_options;
}

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
