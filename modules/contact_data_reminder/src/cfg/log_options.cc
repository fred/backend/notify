/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/log_options.hh"

#include "src/cfg/exception.hh"
#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

decltype(auto) LogOptions::make_device(const std::string& _device)
{
    static const std::unordered_map<std::string, Device> str2device =
        {
            { "console", Device::console },
            { "file", Device::file },
            { "syslog", Device::syslog }
        };
    const auto device_itr = str2device.find(_device);
    if (device_itr != str2device.end())
    {
        return device_itr->second;
    }
    throw Exception{"\"" + _device + "\" is not valid device"};
}

void LogOptions::set_devices(const std::vector<std::string>& _str_device)
{
    std::for_each(
        _str_device.begin(),
        _str_device.end(),
        [&](const std::string& str_device)
        {
            devices.insert(make_device(str_device));
        });
}

void LogOptions::set_min_severity(const std::string& _severity)
{
    if (min_severity != boost::none)
    {
        throw Exception{"severity can be defined at most once"};
    }
    min_severity = Util::make_severity(_severity);
}

void LogOptions::set_log_grpc_library_events(bool value)
{
    log_grpc_library_events = value;
}

boost::program_options::options_description make_log_options(LogOptions& _log_options)
{
    boost::program_options::options_description options_description{"Logging options", Util::terminal_width};
    const auto set_devices = [&](const std::vector<std::string>& devices) { _log_options.set_devices(devices); };
    const auto set_min_severity = [&](const std::string& severity) { _log_options.set_min_severity(severity); };
    const auto set_log_grpc_library_events = [&](bool to_log) { _log_options.set_log_grpc_library_events(to_log); };
    options_description.add_options()
        ("log.device", boost::program_options::value<std::vector<std::string>>()->multitoken()->notifier(set_devices), "where to log (console/file/syslog)")
        ("log.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.grpc_library",
         boost::program_options::value<bool>()->zero_tokens()->notifier(set_log_grpc_library_events),
         "log gRPC library events");
    return options_description;
}

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
