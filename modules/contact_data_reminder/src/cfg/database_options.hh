/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef DATABASE_OPTIONS_HH_A89A98941FB04B10A42F56AC981913EE
#define DATABASE_OPTIONS_HH_A89A98941FB04B10A42F56AC981913EE

#include <boost/asio/ip/address.hpp>
#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <chrono>
#include <string>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

struct DatabaseOptions
{
    boost::optional<std::string> host;
    boost::optional<boost::asio::ip::address> host_addr;
    boost::optional<::in_port_t> port;
    boost::optional<std::string> user;
    boost::optional<std::string> dbname;
    boost::optional<std::string> password;
    boost::optional<std::chrono::seconds> connect_timeout;
};

boost::program_options::options_description make_database_options(DatabaseOptions& _database_options);

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
