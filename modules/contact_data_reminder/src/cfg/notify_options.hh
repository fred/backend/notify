/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFY_OPTIONS_HH_BF0893CDCBC24B71ABC086F0C26F2111
#define NOTIFY_OPTIONS_HH_BF0893CDCBC24B71ABC086F0C26F2111

#include <boost/program_options.hpp>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

struct NotifyOptions
{
    int validation_age_years_limit;
};

boost::program_options::options_description make_notify_options(NotifyOptions& _notify_options);

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
