/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAKE_OUTPUT_STREAM_HH_5448B4D3804D47B1A8E95CA4D8C2B062
#define MAKE_OUTPUT_STREAM_HH_5448B4D3804D47B1A8E95CA4D8C2B062

#include "liblog/sink/console_sink_config.hh"

#include <string>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {
namespace Util {

LibLog::Sink::ConsoleSinkConfig::OutputStream make_output_stream(const std::string& _str);

} // namespace Fred::Notify::ContactDataReminder::Util
} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
