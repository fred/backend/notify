/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/util/make_output_stream.hh"

#include "src/cfg/exception.hh"

#include <string>
#include <unordered_map>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {
namespace Util {

LibLog::Sink::ConsoleSinkConfig::OutputStream make_output_stream(const std::string& _str)
{
    static const std::unordered_map<std::string, LibLog::Sink::ConsoleSinkConfig::OutputStream> str2output_stream =
        {
            { "stderr", LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr },
            { "stdout", LibLog::Sink::ConsoleSinkConfig::OutputStream::stdout }
        };
    const auto output_stream_itr = str2output_stream.find(_str);
    if (output_stream_itr != str2output_stream.end())
    {
        return output_stream_itr->second;
    }
    throw Exception("\"" + _str + "\" not a valid output stream");
}

} // namespace Fred::Notify::ContactDataReminder::Cfg::Util
} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
