/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_OPTIONS_HH_6F21DF27AB714AFC844E50208B65F64C
#define LOG_OPTIONS_HH_6F21DF27AB714AFC844E50208B65F64C

#include "src/cfg/util/make_severity.hh"

#include "src/cfg/log_console_options.hh"
#include "src/cfg/log_file_options.hh"
#include "src/cfg/log_syslog_options.hh"

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <algorithm>
#include <set>
#include <string>
#include <unordered_map>
#include <vector>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

struct Log
{
    boost::optional<LogConsole> log_console;
    boost::optional<LogFile> log_file;
    boost::optional<LogSyslog> log_syslog;
    bool log_grpc_library_events;
};

struct LogOptions
{
    enum class Device
    {
        console,
        file,
        syslog
    };
    static decltype(auto) make_device(const std::string& device);
    std::set<Device> devices;
    boost::optional<LibLog::Level> min_severity;
    boost::optional<bool> log_grpc_library_events;
    void set_devices(const std::vector<std::string>& str_devices);
    void set_min_severity(const std::string& severity);
    void set_log_grpc_library_events(bool value);
};

boost::program_options::options_description make_log_options(LogOptions& _log_common_options);

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
