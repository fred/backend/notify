/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/cfg/notify_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Cfg {

boost::program_options::options_description make_notify_options(NotifyOptions& _notify_options)
{
    boost::program_options::options_description notify_options{"Common notify access options", Util::terminal_width};
    const auto set_validation_age_years_limit = [&](const int validation_age_years_limit) { _notify_options.validation_age_years_limit = validation_age_years_limit; };
    notify_options.add_options()
        ("notify.validation_age_years_limit", boost::program_options::value<int>()->default_value(5)->notifier(set_validation_age_years_limit), "do not notify contacts validated after (NOW - validation_age_years_limit)");
    return notify_options;
}

} // namespace Fred::Notify::ContactDataReminder::Cfg
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
