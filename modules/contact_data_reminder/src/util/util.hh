/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_HH_2F791714CDB347ED88A86AC316552DD8
#define UTIL_HH_2F791714CDB347ED88A86AC316552DD8

#include "libpg/pg_connection.hh"
#include "libpg/pg_ro_transaction.hh"
#include "libpg/pg_rw_transaction.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Util {

using SessionDefault = LibPg::PgTransaction::SessionDefault;
using SerializableDeferrable = LibPg::PgTransaction::Serializable<LibPg::PgTransaction::Deferrable>;

LibPg::PgConnection make_ro_connection();
LibPg::PgConnection make_rw_connection();

template <typename Level = SessionDefault>
LibPg::PgRoTransaction make_ro_transaction(LibPg::PgTransaction::IsolationLevel<Level> level = LibPg::PgTransaction::session_default);
LibPg::PgRwTransaction make_rw_transaction();

extern template LibPg::PgRoTransaction make_ro_transaction<>(LibPg::PgTransaction::IsolationLevel<SessionDefault>);
extern template LibPg::PgRoTransaction make_ro_transaction<SerializableDeferrable>(LibPg::PgTransaction::IsolationLevel<SerializableDeferrable>);

} // namespace Fred::Notify::ContactDataReminder::Util
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
