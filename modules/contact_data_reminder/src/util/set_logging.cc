/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/set_logging.hh"

#include "liblog/liblog.hh"
#include "liblog/log_config.hh"
#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <iostream>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Util {

namespace {

decltype(auto) make_sink_config(const Cfg::LogConsole& log_options)
{
    LibLog::Sink::ConsoleSinkConfig sink_config;
    sink_config.set_level(log_options.min_severity);
    sink_config.set_output_stream(log_options.output_stream);
    sink_config.set_color_mode(log_options.color_mode);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::LogFile& log_options)
{
    LibLog::Sink::FileSinkConfig sink_config{log_options.file_name};
    sink_config.set_level(log_options.min_severity);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::LogSyslog& log_options)
{
    LibLog::Sink::SyslogSinkConfig sink_config;
    sink_config.set_syslog_facility(log_options.facility);
    sink_config.set_level(log_options.min_severity);
    sink_config.set_syslog_ident(log_options.ident);
    sink_config.set_syslog_options(log_options.options);
    return sink_config;
}

} // namespace Fred::Notify::ContactDataReminder::Util::{anonymous}

void set_logging(const Cfg::Log& log)
{
    bool has_device = false;
    LibLog::LogConfig log_config;
    if (log.log_console != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_console));
            has_device = true;
        }
        catch (...) { }
    }
    if (log.log_file != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_file));
            has_device = true;
        }
        catch (...) { }
    }
    if (log.log_syslog != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_syslog));
            has_device = true;
        }
        catch (...) { }
    }
    if (has_device)
    {
        try
        {
            LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
            return;
        }
        catch (const std::exception& e)
        {
            std::cerr << "Setting up logging failure: " << e.what() << std::endl;
            ::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Setting up logging failure: Unexpected exception caught" << std::endl;
            ::exit(EXIT_FAILURE);
        }
    }
    std::cout << "no logging is configured!?" << std::endl;
}

} // namespace Fred::Notify::ContactDataReminder::Util
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
