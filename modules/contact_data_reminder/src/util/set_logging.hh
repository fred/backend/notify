/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SET_LOGGING_HH_0261FD7B93524298AEEA302FCDF90156
#define SET_LOGGING_HH_0261FD7B93524298AEEA302FCDF90156

#include "src/cfg.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Util {

void set_logging(const Cfg::Log& log);

} // namespace Fred::Notify::ContactDataReminder::Util
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
