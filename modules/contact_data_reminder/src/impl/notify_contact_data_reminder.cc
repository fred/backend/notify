/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/notify_contact_data_reminder.hh"

#include "src/impl/unwrap_uuid_from_psql.hh"

#include "src/util/get_optional.hh"
#include "src/util/util.hh"

#include "libhermes/libhermes.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"

#include "libstrong/optional.hh"

#include <boost/algorithm/string.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/optional/optional_io.hpp>
#include <boost/tokenizer.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/variant.hpp>

#include <stdexcept>
#include <string>
#include <vector>


namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Impl {

namespace {

struct Contact
{
    using Id = LibStrong::Type<int, struct ContactIdTag_>;
    using Uuid = StrongUuid<struct ContactUuidTag_>;
    using Roid = StrongString<struct ContactRoidTag_>;
    using Handle = StrongString<struct ContactHandleTag_>;
    using Crdate = LibStrong::Type<boost::posix_time::ptime, struct ContactCrdateTag_>;
    using Registrar = StrongString<struct ContactRegistrarTag_>;
    using RegistrarName = StrongString<struct ContactRegistrarnameTag_>;
    using RegistrarUrl = StrongString<struct ContactRegistrarurlTag_>;
    using RegistrarMemoCz = StrongString<struct ContactRegistrarMemoCzTag_>;
    using RegistrarMemoEn = StrongString<struct ContactRegistrarMemoEnTag_>;
    using RegistrarReplyTo = StrongString<struct ContactRegistrarReplyToTag_>;
    using Name = StrongString<struct ContactNameTag_>;
    using Address = StrongString<struct ContactAddressTag_>;
    using Organization = StrongString<struct ContactOrganizationTag_>;
    using IdentType = StrongString<struct ContactIdentTypeTag_>;
    using IdentValue = StrongString<struct ContactIdentvalueTag_>;
    using Dic = StrongString<struct ContactDicTag_>;
    using Telephone = StrongString<struct ContactTelephoneTag_>;
    using Fax = StrongString<struct ContactFaxTag_>;
    using Email = StrongString<struct ContactEmailTag_>;
    using NotifyEmail = StrongString<struct ContactNotifyEmailTag_>;
    using ArrDomains = StrongString<struct ContactArrDomainsTag_>;
    using ArrNssets = StrongString<struct ContactArrNssetsTag_>;
    using ArrKeysets = StrongString<struct ContactArrKeysetsTag_>;
};

template <typename> constexpr const char* item_name();
template <> constexpr const char* item_name<Contact::Roid>() { return "roid"; }
template <> constexpr const char* item_name<Contact::Handle>() { return "handle"; }
template <> constexpr const char* item_name<Contact::Crdate>() { return "crdate"; }
template <> constexpr const char* item_name<Contact::Registrar>() { return "registrar"; }
template <> constexpr const char* item_name<Contact::RegistrarName>() { return "registrar_name"; }
template <> constexpr const char* item_name<Contact::RegistrarUrl>() { return "registrar_url"; }
template <> constexpr const char* item_name<Contact::RegistrarMemoCz>() { return "registrar_memo_cz"; }
template <> constexpr const char* item_name<Contact::RegistrarMemoEn>() { return "registrar_memo_en"; }
template <> constexpr const char* item_name<Contact::RegistrarReplyTo>() { return "registrar_reply_to"; }
template <> constexpr const char* item_name<Contact::Name>() { return "name"; }
template <> constexpr const char* item_name<Contact::Address>() { return "address"; }
template <> constexpr const char* item_name<Contact::Organization>() { return "organization"; }
template <> constexpr const char* item_name<Contact::IdentType>() { return "ident_type"; }
template <> constexpr const char* item_name<Contact::IdentValue>() { return "ident_value"; }
template <> constexpr const char* item_name<Contact::Dic>() { return "dic"; }
template <> constexpr const char* item_name<Contact::Telephone>() { return "telephone"; }
template <> constexpr const char* item_name<Contact::Fax>() { return "fax"; }
template <> constexpr const char* item_name<Contact::Email>() { return "email"; }
template <> constexpr const char* item_name<Contact::NotifyEmail>() { return "notify_email"; }
template <> constexpr const char* item_name<Contact::ArrDomains>() { return "domains"; }
template <> constexpr const char* item_name<Contact::ArrNssets>() { return "nssets"; }
template <> constexpr const char* item_name<Contact::ArrKeysets>() { return "keysets"; }

// very simple parser - expects comma separated database array of handles;
// handles may not include the comma character
std::vector<LibHermes::StructValue> simple_dbarr_to_vector(const std::string& _dbarr)
{
    std::vector<LibHermes::StructValue> result;

    const auto dbarr = boost::algorithm::trim_copy(_dbarr);
    if (dbarr.empty())
    {
        throw std::runtime_error("not a database array value (empty)");
    }
    const auto left_cbrace = dbarr.begin();
    const auto right_cbrace = dbarr.end() - 1;
    if (*left_cbrace != '{' || *right_cbrace != '}')
    {
        throw std::runtime_error("not a database array value");
    }

    using tokenizer = boost::tokenizer<boost::char_separator<char>>;
    boost::char_separator<char> sep(",");

    tokenizer t(left_cbrace + 1, right_cbrace, sep);
    std::transform(
            t.begin(),
            t.end(),
            std::back_inserter(result),
            [](const std::string& value) -> LibHermes::StructValue
            {
                return LibHermes::StructValue{value};
            });
    return result;
}

template <typename T>
void insert(
        LibHermes::Struct& _context,
        const T& _option)
{
    _context[LibHermes::StructKey{item_name<T>()}] = LibHermes::StructValue{*_option};
}

template <>
void insert(
        LibHermes::Struct& _context,
        const Contact::Crdate& _option)
{
    _context[LibHermes::StructKey{item_name<Contact::Crdate>()}] = LibHermes::StructValue{to_simple_string(*_option)};
}

template <typename T>
void insert_value_if_present(
        LibHermes::Struct& _context,
        const LibStrong::Optional<T>& _option)
{
    if (!is_nullopt(_option))
    {
        insert(_context, *_option);
    }
}

template <typename T>
void insert_if_not_empty(
        LibHermes::Struct& _context,
        const std::vector<LibHermes::StructValue>& _option)
{
    _context[LibHermes::StructKey{item_name<T>()}] = LibHermes::StructValue{_option};
}

struct Message
{
    LibHermes::Email::Email email;
    Contact::Id contact_id;
    Contact::Uuid contact_uuid;
};

std::vector<Message> get_messages(
        const ReminderDate& _reminder_date,
        const NotifyValidationAgeYearsLimit& _notify_validation_age_years_limit,
        const TemplateNameEmailSubject& _template_name_email_subject,
        const TemplateNameEmailBody& _template_name_email_body)
{
    auto tx = Util::make_ro_transaction();
    const auto query = LibPg::make_query() <<
                // clang-format off
                "WITH tmp_reminder AS ("
                "SELECT coreg.id AS contact_id, "
                       "coreg.crdate AS contact_crdate "
                  "FROM object_registry coreg "
                  "JOIN object o "
                    "ON o.id = coreg.id "
                  "JOIN object_state os "
                    "ON os.object_id = coreg.id "
                  "JOIN enum_object_states eos "
                    "ON eos.id = os.state_id "
                 "WHERE coreg.type = get_object_type_id('contact') "
                   "AND coreg.erdate IS NULL "
                   "AND eos.name = 'linked' "
                   "AND os.valid_from < CURRENT_TIMESTAMP "
                   "AND (os.valid_to > CURRENT_TIMESTAMP OR os.valid_to IS NULL) "
                   "AND EXTRACT('month' FROM (" << LibPg::parameter<ReminderDate>().as_date() << " - INTERVAL '300 day')) = EXTRACT('month' FROM coreg.crdate::DATE) "
                   "AND EXTRACT('day' FROM (" << LibPg::parameter<ReminderDate>().as_date() << " - INTERVAL '300 day')) = EXTRACT('day' FROM coreg.crdate::DATE) "
                   "AND EXTRACT('year' FROM (" << LibPg::parameter<ReminderDate>().as_date() << " - INTERVAL '300 day')) >= EXTRACT('year' FROM coreg.crdate::DATE) "
                   "AND (o.update IS NULL "
                        "OR o.update::DATE NOT BETWEEN "
                            "(" << LibPg::parameter<ReminderDate>().as_date() << " - INTERVAL '2 month') "
                            "AND " << LibPg::parameter<ReminderDate>().as_date() << ") "
                   "AND NOT EXISTS (SELECT 0 "
                                     "FROM notification.contact_data_reminder ncdr "
                                    "WHERE ncdr.contact_id = coreg.id "
                                      "AND ncdr.reminder_date = " << LibPg::parameter<ReminderDate>().as_date() << ") "
                   "AND NOT EXISTS (SELECT 0 "
                                     "FROM object_state osv "
                                     "JOIN enum_object_states eosv "
                                       "ON eosv.id = osv.state_id "
                                    "WHERE osv.object_id = coreg.id "
                                      "AND eosv.name = 'validatedContact' "
                                      "AND osv.valid_from > CURRENT_TIMESTAMP - (" << LibPg::parameter<NotifyValidationAgeYearsLimit>().as_integer() << " || 'YEAR')::INTERVAL "
                                      "AND (osv.valid_to > CURRENT_TIMESTAMP OR osv.valid_to IS NULL))), "
                "tmp_contact AS ("
                "SELECT tmp.contact_id AS id, "
                       "coreg.uuid AS uuid, "
                       "coreg.roid AS roid, "
                       "coreg.name AS handle, "
                       "coreg.crdate AS crdate, "
                       "r.handle AS registrar, "
                       "r.name AS registrar_name, "
                       "r.url AS registrar_url, "
                       "split_part(rrp.template_memo, '~@~', 1) AS registrar_memo_cz, "
                       "split_part(rrp.template_memo, '~@~', 2) AS registrar_memo_en, "
                       "rrp.reply_to AS registrar_reply_to, "
                       "c.name AS name, "
                       "c.street1 || ', ' || c.city || ', ' || c.postalcode AS address, "
                       "c.organization AS organization, "
                       "est.type AS ident_type, "
                       "c.ssn AS ident_value, "
                       "c.vat AS dic, "
                       "c.telephone AS telephone, "
                       "c.fax AS fax, "
                       "c.email AS email, "
                       "c.notifyemail AS notify_email "
                  "FROM tmp_reminder tmp "
                  "LEFT JOIN (object_registry coreg "
                            "JOIN object co "
                              "ON co.id = coreg.id "
                            "JOIN registrar r "
                              "ON r.id = co.clid "
                            "LEFT JOIN reminder_registrar_parameter rrp "
                              "ON rrp.registrar_id = r.id "
                            "JOIN contact c "
                              "ON c.id = coreg.id "
                            "LEFT JOIN enum_ssntype est "
                              "ON est.id = c.ssntype) "
                    "ON coreg.id = tmp.contact_id), "
                "tmp_domain AS ("
                "SELECT t.contact_id AS id, "
                       "array_filter_null(array_agg(t.name)) AS arr_domains "
                  "FROM ("
                       "SELECT tmp.contact_id, oreg.name "
                         "FROM tmp_reminder tmp "
                         "LEFT JOIN domain_contact_map dcm "
                           "ON dcm.contactid = tmp.contact_id "
                         "LEFT JOIN object_registry oreg "
                           "ON oreg.id = dcm.domainid "
                        "UNION "
                       "SELECT tmp.contact_id, oreg.name "
                         "FROM tmp_reminder tmp "
                         "LEFT JOIN domain d "
                           "ON d.registrant = tmp.contact_id "
                         "LEFT JOIN object_registry oreg "
                           "ON oreg.id = d.id "
                   ") t "
                   "GROUP BY t.contact_id), "
                  "tmp_nsset AS ("
                  "SELECT tmp.contact_id AS id, "
                         "array_filter_null(array_agg(DISTINCT noreg.name)) AS arr_nssets "
                    "FROM tmp_reminder tmp "
                    "LEFT JOIN nsset_contact_map ncm "
                      "ON ncm.contactid = tmp.contact_id "
                    "LEFT JOIN object_registry noreg "
                      "ON noreg.id = ncm.nssetid "
                   "GROUP BY tmp.contact_id), "
                  "tmp_keyset AS ("
                  "SELECT tmp.contact_id AS id, "
                         "array_filter_null(array_agg(DISTINCT koreg.name)) AS arr_keysets "
                    "FROM tmp_reminder tmp "
                    "LEFT JOIN keyset_contact_map kcm "
                      "ON kcm.contactid = tmp.contact_id "
                    "LEFT JOIN object_registry koreg "
                      "ON koreg.id = kcm.keysetid "
                   "GROUP BY tmp.contact_id) "
                  "SELECT " << LibPg::item<Contact::Id>() << "tmp_contact.id" <<
                               LibPg::item<Contact::Uuid>() << "tmp_contact.uuid" <<
                               LibPg::nullable_item<Contact::Roid>() << "tmp_contact.roid" <<
                               LibPg::nullable_item<Contact::Handle>() << "tmp_contact.handle" <<
                               LibPg::nullable_item<Contact::Crdate>() << "tmp_contact.crdate" <<
                               LibPg::nullable_item<Contact::Registrar>() << "tmp_contact.registrar" <<
                               LibPg::nullable_item<Contact::RegistrarName>() << "tmp_contact.registrar_name" <<
                               LibPg::nullable_item<Contact::RegistrarUrl>() << "tmp_contact.registrar_url" <<
                               LibPg::nullable_item<Contact::RegistrarMemoCz>() << "tmp_contact.registrar_memo_cz" <<
                               LibPg::nullable_item<Contact::RegistrarMemoEn>() << "tmp_contact.registrar_memo_en" <<
                               LibPg::nullable_item<Contact::RegistrarReplyTo>() << "tmp_contact.registrar_reply_to" <<
                               LibPg::nullable_item<Contact::Name>() << "tmp_contact.name" <<
                               LibPg::nullable_item<Contact::Address>() << "tmp_contact.address" <<
                               LibPg::nullable_item<Contact::Organization>() << "tmp_contact.organization" <<
                               LibPg::nullable_item<Contact::IdentType>() << "tmp_contact.ident_type" <<
                               LibPg::nullable_item<Contact::IdentValue>() << "tmp_contact.ident_value" <<
                               LibPg::nullable_item<Contact::Dic>() << "tmp_contact.dic" <<
                               LibPg::nullable_item<Contact::Telephone>() << "tmp_contact.telephone" <<
                               LibPg::nullable_item<Contact::Fax>() << "tmp_contact.fax" <<
                               LibPg::nullable_item<Contact::Email>() << "tmp_contact.email" <<
                               LibPg::nullable_item<Contact::NotifyEmail>() << "tmp_contact.notify_email" <<
                               LibPg::item<Contact::ArrDomains>() << "tmp_domain.arr_domains" <<
                               LibPg::item<Contact::ArrNssets>() << "tmp_nsset.arr_nssets" <<
                               LibPg::item<Contact::ArrKeysets>() << "tmp_keyset.arr_keysets "
                     "FROM tmp_contact "
                     "JOIN tmp_domain ON tmp_domain.id = tmp_contact.id "
                     "JOIN tmp_nsset ON tmp_nsset.id = tmp_contact.id "
                     "JOIN tmp_keyset ON tmp_keyset.id = tmp_contact.id "
                     "ORDER BY tmp_contact.id";
                // clang-format on

    const auto dbres = exec(tx, query, {_reminder_date, _notify_validation_age_years_limit});
    //std::cout << dbres.get_number_of_rows_affected() << std::endl;
    std::vector<Message> messages;

    for (const auto row : dbres)
    {
        const auto recipient = Util::get_optional<Contact::Email>(row);
        if (is_nullopt(recipient))
        {
            LIBLOG_WARNING("contact with id {} has no email", *row.get<Contact::Id>());
            continue;
        }

        const auto contact_id = row.get<Contact::Id>();
        const auto contact_uuid = row.get<Contact::Uuid>();

        auto email =
                LibHermes::Email::make_minimal_email(
                        {{LibHermes::Email::RecipientEmail{boost::algorithm::trim_copy(**recipient)},
                         {LibHermes::Email::RecipientUuid{*contact_uuid}}}},
                        LibHermes::Email::SubjectTemplate{*_template_name_email_subject},
                        LibHermes::Email::BodyTemplate{*_template_name_email_body});
        email.type = LibHermes::Email::Type{"annual_contact_reminder"};

        const auto reply_to = Util::get_optional<Contact::RegistrarReplyTo>(row);
        if (!is_nullopt(reply_to))
        {
            email.extra_headers.emplace(
                    LibHermes::Email::ExtraHeaderKey{"Reply-To"},
                    LibHermes::Email::ExtraHeaderValue{boost::algorithm::trim_copy(**reply_to)});
        }

        insert_value_if_present(email.context, Util::get_optional<Contact::Roid>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Handle>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Crdate>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Registrar>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::RegistrarName>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::RegistrarUrl>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::RegistrarMemoCz>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::RegistrarMemoEn>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::RegistrarReplyTo>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Name>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Address>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Organization>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::IdentType>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::IdentValue>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Dic>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Telephone>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Fax>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::Email>(row));
        insert_value_if_present(email.context, Util::get_optional<Contact::NotifyEmail>(row));
        insert_if_not_empty<Contact::ArrDomains>(email.context, simple_dbarr_to_vector(*row.get<Contact::ArrDomains>()));
        insert_if_not_empty<Contact::ArrNssets>(email.context, simple_dbarr_to_vector(*row.get<Contact::ArrNssets>()));
        insert_if_not_empty<Contact::ArrKeysets>(email.context, simple_dbarr_to_vector(*row.get<Contact::ArrKeysets>()));

        messages.push_back(Message{email, contact_id, contact_uuid});
    }

    commit(std::move(tx));
    return messages;
}

void save_confirmation(
        const LibPg::PgRwTransaction& _tx_rw,
        const ReminderDate& _reminder_date,
        const Contact::Id& _contact_id,
        const LibHermes::Email::EmailUid& _email_uid)
{
    const auto query = LibPg::make_query() <<
                       "INSERT INTO notification.contact_data_reminder "
                       "(reminder_date, contact_id, message_ident) "
                       "VALUES (" << LibPg::parameter<ReminderDate>().as_date() << ", " <<
                                     LibPg::parameter<Contact::Id>().as_integer() << ", " <<
                                     LibPg::parameter<LibHermes::Email::EmailUid>().as_text() << ")";
    exec(_tx_rw, query, {_reminder_date, _contact_id, _email_uid});
}

} // namespace Fred::Notify::ContactDataReminder::Impl::{anonymous}

void notify_contact_data_reminder(
        const NotifyConfiguration& _notify_configuration,
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive,
        const TemplateNameEmailSubject& _template_name_email_subject,
        const TemplateNameEmailBody& _template_name_email_body)
{
    try
    {
        const auto messages =
                get_messages(
                        _notify_configuration.reminder_date,
                        _notify_configuration.notify_validation_age_years_limit,
                        _template_name_email_subject,
                        _template_name_email_body);

        LibHermes::Connection<LibHermes::Service::EmailMessenger> connection{
            LibHermes::Connection<LibHermes::Service::EmailMessenger>::ConnectionString{
                *_messenger_endpoint}};

        auto conn_ptr = std::make_unique<LibPg::PgConnection>(Util::make_rw_connection());

        for (const auto& message : messages)
        {
            LibHermes::Email::EmailUid email_uid;
            try
            {
                email_uid =
                        LibHermes::Email::send(
                                connection,
                                message.email,
                                LibHermes::Email::Archive{*_messenger_archive});
            }
            catch (const LibHermes::Email::SendFailed& e)
            {
                LIBLOG_WARNING("gRPC exception caught while sending email to contact with uuid {}: gRPC error code: {}, error message: {}, message: {}",
                        boost::uuids::to_string(*message.contact_uuid), e.error_code(), e.error_message(), e.grpc_message_json());
                continue;
            }
            catch (const std::exception& e)
            {
                LIBLOG_WARNING("std::exception caught while sending email to contact with uuid {}: {}", boost::uuids::to_string(*message.contact_uuid), e.what());
                continue;
            }
            catch (...)
            {
                LIBLOG_WARNING("exception caught while sending email to contact with uuid {}", boost::uuids::to_string(*message.contact_uuid));
                continue;
            }

            auto tx = LibPg::PgRwTransaction{std::move(*(conn_ptr.release()))};
            save_confirmation(tx, _notify_configuration.reminder_date, message.contact_id, email_uid);
            conn_ptr = std::make_unique<LibPg::PgConnection>(commit(std::move(tx)));
        }
    }
    catch (const LibPg::ExecFailure& e)
    {
        LIBLOG_WARNING("SQL exec failure: {}", e.what());
        throw;
    }
    catch (const LibPg::ConnectFailure& e)
    {
        LIBLOG_WARNING("DB connect failure: {}", e.what());
        throw;
    }
    catch (const std::exception& e)
    {
        LIBLOG_WARNING("std::exception caught: {}", e.what());
        throw;
    }
}

} // namespace Fred::Notify::ContactDataReminder::Impl
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
