/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/exception.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Impl {

Exception::Exception(std::string _msg)
    : msg_{std::move(_msg)}
{
}

const char* Exception::what() const noexcept
{
    return msg_.c_str();
}

InvalidUuid::InvalidUuid(std::string _msg)
    : Exception{std::move(_msg)}
{
}

} // namespace Fred::Notify::ContactDataReminder::Impl
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred
