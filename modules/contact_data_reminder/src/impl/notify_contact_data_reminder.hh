/*
 * Copyright (C) 2021-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef NOTIFY_CONTACT_DATA_REMINDER_HH_CEF6A90770F84B5D8BE1D774E5D32046
#define NOTIFY_CONTACT_DATA_REMINDER_HH_CEF6A90770F84B5D8BE1D774E5D32046

#include "src/impl/strong_type.hh"

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Impl {

using ReminderDate = LibStrong::Type<boost::gregorian::date, struct ReminderDateTag_>;
using NotifyValidationAgeYearsLimit = LibStrong::Type<int, struct NotifyValidationAgeYearsLimitTag_>;

struct NotifyConfiguration
{
    ReminderDate reminder_date;
    NotifyValidationAgeYearsLimit notify_validation_age_years_limit;
};

using MessengerEndpoint = StrongString<struct MessengerEndpointTag_>;
using MessengerArchive = LibStrong::Type<bool, struct MessengerArchiveTag_>;
using TemplateNameEmailSubject = StrongString<struct TemplateNameEmailSubject_>;
using TemplateNameEmailBody = StrongString<struct TemplateNameEmailBody_>;

void notify_contact_data_reminder(
        const NotifyConfiguration& _notify_configuration,
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive,
        const TemplateNameEmailSubject& _template_name_email_subject,
        const TemplateNameEmailBody& _template_email_body);

} // namespace Fred::Notify::ContactDataReminder::Impl
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
