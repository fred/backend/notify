/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTION_HH_F9BFB902D5A444B9A4ADD66FB38CA75C
#define EXCEPTION_HH_F9BFB902D5A444B9A4ADD66FB38CA75C

#include <exception>
#include <string>

namespace Fred {
namespace Notify {
namespace ContactDataReminder {
namespace Impl {

class Exception : public std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    const std::string msg_;
};

struct InvalidUuid : Exception
{
    explicit InvalidUuid(std::string _msg);
};

} // namespace Fred::Notify::ContactDataReminder::Impl
} // namespace Fred::Notify::ContactDataReminder
} // namespace Fred::Notify
} // namespace Fred

#endif
