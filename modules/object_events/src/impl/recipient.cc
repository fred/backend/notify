/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/recipient.hh"

#include <tuple>
#include <utility>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

Recipient::Recipient(ContactEmailAddress _email, ContactUuid _uuid)
    : email(std::move(_email)),
      uuid(std::move(_uuid))
{
}

bool Recipient::operator<(const Recipient& _rhs) const
{
    return std::make_tuple(*email, *uuid) < std::make_tuple(*_rhs.email, *_rhs.uuid);
}

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
