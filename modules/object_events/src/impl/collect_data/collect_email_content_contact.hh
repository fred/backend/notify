/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COLLECT_EMAIL_CONTENT_CONTACT_HH_FF0A9C6D1EF5489F84768DB7EBA93CE4
#define COLLECT_EMAIL_CONTENT_CONTACT_HH_FF0A9C6D1EF5489F84768DB7EBA93CE4

#include "src/impl/notification_request.hh"
#include "src/impl/notified_event.hh"

#include "libfred/opcontext.hh"

#include "libhermes/struct.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

LibHermes::Struct collect_contact_data_change(
        const LibFred::OperationContext& _ctx,
        NotifiedEvent _notified_event,
        const HistoryId& _history_id_after_change);

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
