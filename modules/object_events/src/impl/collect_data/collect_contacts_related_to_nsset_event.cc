/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_contacts_related_to_nsset_event.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/collect_data/util/string_list_utils.hh"

#include "libfred/registrable_object/nsset/info_nsset.hh"
#include "libfred/registrable_object/nsset/info_nsset_diff.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

std::set<Contact> get_contacts_accepting_notifications(const LibFred::InfoNssetData& _data)
{
    std::set<Contact> result;
    for (const auto& contact : _data.tech_contacts)
    {
        result.emplace(ContactId{contact.id}, ContactUuid{get_raw_value_from(contact.uuid)});
    }
    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

std::set<Contact> collect_contacts_related_to_nsset_event(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    std::set<Contact> contacts =
            get_contacts_accepting_notifications(LibFred::InfoNssetHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_nsset_data);

    // if there were possibly other old values notify those as well
    if (_notified_event == NotifiedEvent::updated)
    {
        const auto history_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
        const auto nssets_accepting_notifications_before_change =
                get_contacts_accepting_notifications(LibFred::InfoNssetHistoryByHistoryid(*history_id_before_change).exec(_ctx).info_nsset_data);

        contacts.insert(nssets_accepting_notifications_before_change.begin(), nssets_accepting_notifications_before_change.end());
    }

    return contacts;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
