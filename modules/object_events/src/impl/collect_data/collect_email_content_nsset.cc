/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_email_content_nsset.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/collect_data/util/string_list_utils.hh"
#include "src/impl/exception.hh"

#include "libfred/registrable_object/nsset/info_nsset.hh"
#include "libfred/registrable_object/nsset/info_nsset_data.hh"
#include "libfred/registrable_object/nsset/info_nsset_diff.hh"
#include "libfred/registrable_object/nsset/nsset_dns_host.hh"

#include <boost/algorithm/string/join.hpp>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

std::vector<std::string> get_string_addresses(const std::vector<boost::asio::ip::address>& _in)
{
    std::vector<std::string> result;
    result.reserve(_in.size());
    std::transform(
            _in.cbegin(),
            _in.cend(),
            std::back_inserter(result),
            [](const auto& o) { return o.to_string(); });
    return result;
}

bool sort_by_hostname(const LibFred::DnsHost& lhs, const LibFred::DnsHost& rhs)
{
    return lhs.get_fqdn() < rhs.get_fqdn();
}

bool equal(const LibFred::DnsHost& lhs, const LibFred::DnsHost& rhs)
{
    return (lhs.get_fqdn() == rhs.get_fqdn()) &&
           (lhs.get_inet_addr() == rhs.get_inet_addr());
}

bool equal(const std::vector<LibFred::DnsHost>& lhs, const std::vector<LibFred::DnsHost>& rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    for (std::vector<LibFred::DnsHost>::size_type idx = 0; idx < lhs.size(); ++idx)
    {
        if (!equal(lhs[idx], rhs[idx]))
        {
            return false;
        }
    }
    return true;
}

std::string dns_host_to_string(const LibFred::DnsHost& lhs)
{
    std::vector<boost::asio::ip::address> sorted_ip_addresses = lhs.get_inet_addr();
    std::sort(sorted_ip_addresses.begin(), sorted_ip_addresses.end()); // boost::asio::address has operator< in sense of represented integer number

    return  lhs.get_fqdn() +
            (sorted_ip_addresses.empty()
                ? ""
                : " (" + boost::algorithm::join(get_string_addresses(sorted_ip_addresses), " ") + ")");
}

LibHermes::Struct collect_nsset_update_data_change(
        const LibFred::InfoNssetData& _before,
        const LibFred::InfoNssetData& _after)
{
    const LibFred::InfoNssetDiff diff = diff_nsset_data(_before, _after);

    LibHermes::Struct nsset;
    if (diff.tech_check_level.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                nsset,
                LibHermes::StructKey{"check_level"},
                diff.tech_check_level.get_value().first.isnull()
                        ? ""
                        : std::to_string(diff.tech_check_level.get_value().first.get_value()),
                diff.tech_check_level.get_value().second.isnull()
                        ? ""
                        : std::to_string(diff.tech_check_level.get_value().second.get_value()));
    }

    if (diff.tech_contacts.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                nsset,
                LibHermes::StructKey{"tech_c"},
                boost::algorithm::join(Util::sort(Util::get_handles(diff.tech_contacts.get_value().first)), " "),
                boost::algorithm::join(Util::sort(Util::get_handles(diff.tech_contacts.get_value().second)), " "));
    }

    if (diff.dns_hosts.isset())
    {
        std::vector<LibFred::DnsHost> sorted_nameservers_old = diff.dns_hosts.get_value().first;
        std::sort(sorted_nameservers_old.begin(), sorted_nameservers_old.end(), sort_by_hostname);

        std::vector<LibFred::DnsHost> sorted_nameservers_new = diff.dns_hosts.get_value().second;
        std::sort(sorted_nameservers_new.begin(), sorted_nameservers_new.end(), sort_by_hostname);

        if (!equal(sorted_nameservers_old, sorted_nameservers_new))
        {
            std::vector<LibHermes::StructValue> nsset_dns_old;
            std::vector<LibHermes::StructValue> nsset_dns_new;

            for (std::vector<LibFred::DnsHost>::size_type idx = 0; idx < sorted_nameservers_old.size(); ++idx)
            {
                nsset_dns_old.push_back(LibHermes::StructValue{dns_host_to_string(sorted_nameservers_old[idx])});
            }

            for (std::vector<LibFred::DnsHost>::size_type idx = 0; idx < sorted_nameservers_new.size(); ++idx)
            {
                nsset_dns_new.push_back(LibHermes::StructValue{dns_host_to_string(sorted_nameservers_new[idx])});
            }

            nsset.emplace(LibHermes::StructKey{"dns"},
                          LibHermes::StructValue{LibHermes::Struct{
                                    {LibHermes::StructKey{"old"}, LibHermes::StructValue{nsset_dns_old}},
                                    {LibHermes::StructKey{"new"}, LibHermes::StructValue{nsset_dns_new}}}});
        }
    }

    LibHermes::Struct changes;
    if (!nsset.empty())
    {
        changes.emplace(LibHermes::StructKey{"nsset"}, LibHermes::StructValue{nsset});
    }
    LibHermes::Struct result;
    if (!changes.empty())
    {
        result.emplace(LibHermes::StructKey{"changes"}, LibHermes::StructValue{changes});
    }

    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

LibHermes::Struct collect_nsset_data_change(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    if (_notified_event != NotifiedEvent::updated)
    {
        return LibHermes::Struct{};
    }
    const auto history_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
    return collect_nsset_update_data_change(
            LibFred::InfoNssetHistoryByHistoryid(*history_id_before_change).exec(_ctx).info_nsset_data,
            LibFred::InfoNssetHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_nsset_data);
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
