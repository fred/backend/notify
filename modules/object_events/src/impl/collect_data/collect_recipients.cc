/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_recipients.hh"

#include "src/impl/collect_data/collect_contacts_related_to_domain_event.hh"
#include "src/impl/collect_data/collect_contacts_related_to_keyset_event.hh"
#include "src/impl/collect_data/collect_contacts_related_to_nsset_event.hh"
#include "src/impl/collect_data/collect_recipients_to_notify_contact_event.hh"
#include "src/impl/collect_data/util/get_utc_time_of_event.hh"
#include "src/impl/contact.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/registrable_object/nsset/info_nsset.hh"

#include <boost/algorithm/string/join.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/uuid/uuid.hpp>

#include <set>
#include <string>
#include <tuple>


namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

// return unique non-empty notifyemail addresses of given _contact_ids
static std::set<Recipient> add_notify_email_to_contacts(
        const LibFred::OperationContext& _ctx,
        const std::set<Contact>& _contacts,
        const boost::posix_time::ptime& _time_of_validity)
{

    std::set<std::string> contact_ids_as_strings;
    for (const auto& contact : _contacts)
    {
        contact_ids_as_strings.insert(boost::lexical_cast<std::string>(*contact.id));
    }

    const Database::Result email_addresses = _ctx.get_conn().exec_params(
            // clang-format off
            "SELECT c_h.notifyemail AS notifyemail, "
                   "oreg.uuid AS uuid "
             "FROM contact_history c_h "
             "JOIN history h ON c_h.historyid = h.id "
             "JOIN object_registry oreg ON oreg.id = c_h.id "
            "WHERE c_h.id = ANY($1::INT[]) "
                /* this is closed interval inclusive test - inclusive "newer end" is mandatory for deleted contact, "older end" is included just in case */
              "AND ( h.valid_from <= $2::TIMESTAMP AND $2::TIMESTAMP < COALESCE( h.valid_to, 'infinity'::TIMESTAMP )) "
              "AND c_h.notifyemail IS NOT NULL ",
            Database::query_param_list
            (std::string("{") + boost::algorithm::join(contact_ids_as_strings, ", ") + "}")
            (_time_of_validity)
            // clang-format on
    );

    std::set<Recipient> result;

    for (unsigned int i = 0; i < email_addresses.size(); ++i)
    {
        const auto contact_email_address = ContactEmailAddress{boost::algorithm::trim_copy(static_cast<std::string>(email_addresses[i]["notifyemail"]))};
        const auto contact_uuid = ContactUuid{boost::uuids::string_generator()(static_cast<std::string>(email_addresses[i]["uuid"]))};
        if (!(*contact_email_address).empty())
        {
            result.emplace(contact_email_address, contact_uuid);
        }
    }

    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

std::set<Recipient> collect_recipients(
        const LibFred::OperationContext& _ctx,
        const Impl::ObjectEvent& _object_event,
        const Impl::HistoryId _history_id_after_change // always post change except of delete event
)
{
    if (_object_event.object_type == ObjectType::contact)
    {
        return collect_recipients_to_notify_contact_event(_ctx, _object_event.notified_event, _history_id_after_change);
    }

    const auto contacts_to_notify = [&](){
        switch(_object_event.object_type)
        {
            case ObjectType::domain:
                return collect_contacts_related_to_domain_event(_ctx, _object_event.notified_event, _history_id_after_change);
            case ObjectType::keyset:
                return collect_contacts_related_to_keyset_event(_ctx, _object_event.notified_event, _history_id_after_change);
            case ObjectType::nsset:
                return collect_contacts_related_to_nsset_event(_ctx, _object_event.notified_event, _history_id_after_change);
            case ObjectType::contact:
                throw std::logic_error("unexpected ObjectType contact");
        }
        throw std::logic_error("unexpected ObjectType");
    }();

    return add_notify_email_to_contacts(
        _ctx,
        contacts_to_notify,
        Util::get_utc_time_of_event(_ctx, _object_event.notified_event, _history_id_after_change)
    );
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
