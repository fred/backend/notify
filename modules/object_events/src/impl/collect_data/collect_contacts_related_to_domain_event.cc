/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_contacts_related_to_domain_event.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/collect_data/util/get_utc_time_of_event.hh"

#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/nsset/info_nsset.hh"
#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/registrable_object/domain/info_domain_diff.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

std::set<Contact> get_contacts_accepting_notifications(const LibFred::InfoDomainData& _data)
{
    std::set<Contact> result;
    result.emplace(ContactId{_data.registrant.id}, ContactUuid{get_raw_value_from(_data.registrant.uuid)});
    for (const auto& contact : _data.admin_contacts)
    {
        result.emplace(ContactId{contact.id}, ContactUuid{get_raw_value_from(contact.uuid)});
    }
    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

std::set<Contact> collect_contacts_related_to_domain_event(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    std::set<Contact> contacts =
            get_contacts_accepting_notifications(LibFred::InfoDomainHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_domain_data);

    // if there were possibly other old values notify those as well
    if (_notified_event == NotifiedEvent::updated)
    {
        const auto history_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
        const auto contacts_accepting_notifications_before_change =
                get_contacts_accepting_notifications(LibFred::InfoDomainHistoryByHistoryid(*history_id_before_change).exec(_ctx).info_domain_data);

        contacts.insert(contacts_accepting_notifications_before_change.begin(), contacts_accepting_notifications_before_change.end());

        const LibFred::InfoDomainDiff diff = diff_domain_data(
            LibFred::InfoDomainHistoryByHistoryid(*history_id_before_change).exec(_ctx).info_domain_data,
            LibFred::InfoDomainHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_domain_data);

        if (diff.nsset.isset())
        {
            std::set<unsigned long long> nssets;
            if (!diff.nsset.get_value().first.isnull())
            {
                nssets.insert(diff.nsset.get_value().first.get_value().id);
            }
            if (!diff.nsset.get_value().second.isnull())
            {
                nssets.insert(diff.nsset.get_value().second.get_value().id);
            }

            const boost::posix_time::ptime time_of_change =
                    Util::get_utc_time_of_event(_ctx, _notified_event, _history_id_after_change);

            for (const auto nsset_id : nssets)
            {
                bool corresponding_nsset_history_state_found = false;

                for (const auto& nsset_history_state : LibFred::InfoNssetHistoryById(nsset_id).exec(_ctx, "UTC"))
                {
                    if ((nsset_history_state.history_valid_from <= time_of_change) &&
                        (time_of_change <= nsset_history_state.history_valid_to.get_value_or(boost::posix_time::pos_infin)))
                    {
                        corresponding_nsset_history_state_found = true;
                        for (const auto& contact : nsset_history_state.info_nsset_data.tech_contacts)
                        {
                            contacts.emplace(ContactId{contact.id}, ContactUuid{get_raw_value_from(contact.uuid)});
                        }
                        /* continuing search through other history versions - chances are there might be two history states bordering the domain event */
                    }
                }

                if (!corresponding_nsset_history_state_found)
                {
                    throw std::runtime_error("inconsistent data - Nsset that was associated before or "
                                             "after event to domain should exist at that time");
                }
            }
        }

        if (diff.keyset.isset())
        {
            std::set<unsigned long long> keysets;
            if (!diff.keyset.get_value().first.isnull())
            {
                keysets.insert(diff.keyset.get_value().first.get_value().id);
            }
            if (!diff.keyset.get_value().second.isnull())
            {
                keysets.insert(diff.keyset.get_value().second.get_value().id);
            }

            const boost::posix_time::ptime time_of_change =
                    Util::get_utc_time_of_event(_ctx, _notified_event, _history_id_after_change);

            for (const auto keyset_id : keysets)
            {
                bool corresponding_keyset_history_state_found = false;

                for (const auto& keyset_history_state : LibFred::InfoKeysetHistoryById(keyset_id).exec(_ctx, "UTC"))
                {
                    if ((keyset_history_state.history_valid_from <= time_of_change) &&
                        (time_of_change <= keyset_history_state.history_valid_to.get_value_or(boost::posix_time::pos_infin)))
                    {
                        corresponding_keyset_history_state_found = true;
                        for (const auto& contact : keyset_history_state.info_keyset_data.tech_contacts)
                        {
                            contacts.emplace(ContactId{contact.id}, ContactUuid{get_raw_value_from(contact.uuid)});
                        }
                        /* continuing search through other history versions - chances are there might be two history states bordering the domain event */
                    }
                }

                if (!corresponding_keyset_history_state_found)
                {
                    throw std::runtime_error("inconsistent data - Keyset that was associated before or "
                                             "after event to domain should exist at that time");
                }
            }
        }
    }

    return contacts;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
