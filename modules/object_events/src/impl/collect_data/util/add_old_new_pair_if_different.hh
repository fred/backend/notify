/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ADD_OLD_NEW_PAIR_IF_DIFFERENT_HH_65F1BC2552454591AF3BBE3C0F2DA750
#define ADD_OLD_NEW_PAIR_IF_DIFFERENT_HH_65F1BC2552454591AF3BBE3C0F2DA750

#include "libhermes/struct.hh"

#include <map>
#include <stdexcept>
#include <string>
#include <utility>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {
namespace Util {

template <typename T>
void add_old_new_changes_pair_if_different(
        LibHermes::Struct& _target,
        const LibHermes::StructKey& _key,
        T&& _old,
        T&& _new)
{
    if (_old != _new)
    {
        _target.emplace(
                _key,
                LibHermes::StructValue{LibHermes::Struct{
                        {LibHermes::StructKey{"old"}, LibHermes::StructValue{std::forward<T>(_old)}},
                        {LibHermes::StructKey{"new"}, LibHermes::StructValue{std::forward<T>(_new)}}}});
    }
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util
} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
