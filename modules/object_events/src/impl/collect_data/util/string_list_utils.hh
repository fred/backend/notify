/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRING_LIST_UTILS_HH_F3E104028291461FBAF0BA8EF7C6D48F
#define STRING_LIST_UTILS_HH_F3E104028291461FBAF0BA8EF7C6D48F

#include "libfred/registrable_object/registrable_object_reference.hh"

#include <algorithm>
#include <string>
#include <vector>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {
namespace Util {

template <LibFred::Object_Type::Enum object_type>
std::vector<std::string> get_handles(const std::vector<LibFred::RegistrableObject::RegistrableObjectReference<object_type> >& _in)
{
    std::vector<std::string> result;
    result.reserve(_in.size());
    std::transform(
            _in.cbegin(),
            _in.cend(),
            std::back_inserter(result),
            [](const auto& o) { return o.handle; });
    return result;
}

/**
 * interface adapter for std::sort
 */
std::vector<std::string> sort(const std::vector<std::string>& _in);

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util
} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
