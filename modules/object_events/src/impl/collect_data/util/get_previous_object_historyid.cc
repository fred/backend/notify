/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/collect_data/util/get_previous_object_historyid.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {
namespace Util {

HistoryId get_previous_object_historyid(const LibFred::OperationContext& _ctx, HistoryId _history_id)
{

    const Database::Result older_history_id_res =
            _ctx.get_conn().exec_params("SELECT id FROM history WHERE next = $1::integer", Database::query_param_list(*_history_id));

    if (older_history_id_res.size() > 1)
    {
        throw std::runtime_error("multiple object histories");
    }

    if (older_history_id_res.size() < 1)
    {
        throw std::runtime_error("previous historyid not found");
    }

    return HistoryId{static_cast<unsigned long long>(older_history_id_res[0]["id"])};
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util
} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
