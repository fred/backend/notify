/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/collect_data/util/get_utc_time_of_event.hh"

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {
namespace Util {

namespace {

std::string get_column_of_interest(const NotifiedEvent& _notified_event)
{
    switch (_notified_event)
    {
        case NotifiedEvent::created:
        case NotifiedEvent::updated:
        case NotifiedEvent::transferred:
        case NotifiedEvent::renewed:
                return "valid_from";
        case NotifiedEvent::deleted:
                return "valid_to";
    }
    throw std::logic_error("unexpected value of NotifiedEvent");
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util::{anonymous}

boost::posix_time::ptime get_utc_time_of_event(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _last_history_id)
{
    const std::string column_of_interest = get_column_of_interest(_notified_event);

    Database::query_param_list p;
    const Database::Result time_res = _ctx.get_conn().exec_params(
            // clang-format off
            "SELECT " + column_of_interest + " AS the_time_ "
              "FROM history "
             "WHERE id = $" + p.add(*_last_history_id) + "::INT ", p
            // clang-format on
    );

    if (time_res.size() != 1)
    {
        throw std::runtime_error("unknown history id");
    }
    return boost::posix_time::time_from_string(static_cast<std::string>(time_res[0]["the_time_"]));
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util
} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
