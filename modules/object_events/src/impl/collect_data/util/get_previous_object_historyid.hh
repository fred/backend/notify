/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GET_PREVIOUS_OBJECT_HISTORYID_HH_25A769A94BF8486CBE6DD61B7374EAA5
#define GET_PREVIOUS_OBJECT_HISTORYID_HH_25A769A94BF8486CBE6DD61B7374EAA5

#include "src/impl/notification_request.hh"

#include "libfred/opcontext.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {
namespace Util {

HistoryId get_previous_object_historyid(const LibFred::OperationContext& _ctx, HistoryId _history_id);

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Util
} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
