/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_email_content_domain.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/collect_data/util/string_list_utils.hh"
#include "src/impl/exception.hh"

#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/domain/info_domain_data.hh"
#include "libfred/registrable_object/domain/info_domain_diff.hh"

#include <boost/algorithm/string/join.hpp>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

std::string to_string(bool _input)
{
    return _input ? "1" : "0";
}

/**
 * @returns DD. MM. YYYY. or empty string in case _input is_special()
 */
std::string to_cz_format(const boost::gregorian::date& _input)
{
    // XXX I know. Ugly as hell. But still better than official way with dynamicaly allocated facet.
    return _input.is_special()
                   ? ""
                   : std::to_string(_input.day().as_number()) + "." +
                     std::to_string(_input.month().as_number()) + "." +
                     std::to_string(_input.year());
}

LibHermes::Struct collect_domain_update_data_change(
    const LibFred::InfoDomainData& _before,
    const LibFred::InfoDomainData& _after)
{
    const LibFred::InfoDomainDiff diff = diff_domain_data(_before, _after);

    LibHermes::Struct domain;
    if (diff.registrant.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                domain,
                LibHermes::StructKey{"registrant"},
                diff.registrant.get_value().first.handle,
                diff.registrant.get_value().second.handle);
    }

    if (diff.nsset.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                domain,
                LibHermes::StructKey{"nsset"},
                diff.nsset.get_value().first.isnull() ? "" : diff.nsset.get_value().first.get_value().handle,
                diff.nsset.get_value().second.isnull() ? "" : diff.nsset.get_value().second.get_value().handle);
    }

    if (diff.keyset.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                domain,
                LibHermes::StructKey{"keyset"},
                diff.keyset.get_value().first.isnull() ? "" : diff.keyset.get_value().first.get_value().handle,
                diff.keyset.get_value().second.isnull() ? "" : diff.keyset.get_value().second.get_value().handle);
    }

    if (diff.admin_contacts.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                domain,
                LibHermes::StructKey{"admin_c"},
                boost::algorithm::join(Util::sort(Util::get_handles(diff.admin_contacts.get_value().first)), " "),
                boost::algorithm::join(Util::sort(Util::get_handles(diff.admin_contacts.get_value().second)), " "));
    }

    if (diff.enum_domain_validation.isset())
    {
        Util::add_old_new_changes_pair_if_different(
            domain,
            LibHermes::StructKey{"val_ex_date"},
            to_cz_format(
                diff.enum_domain_validation.get_value().first.get_value_or(
                    LibFred::ENUMValidationExtension(
                            boost::gregorian::date(boost::gregorian::not_a_date_time),
                            false)).validation_expiration),
            to_cz_format(
                diff.enum_domain_validation.get_value().second.get_value_or(
                    LibFred::ENUMValidationExtension(
                            boost::gregorian::date(boost::gregorian::not_a_date_time),
                            false)).validation_expiration));

        Util::add_old_new_changes_pair_if_different(
                domain,
                LibHermes::StructKey{"publish"},
                diff.enum_domain_validation.get_value().first.isnull()
                        ? ""
                        : to_string(diff.enum_domain_validation.get_value().first.get_value().publish),
                diff.enum_domain_validation.get_value().second.isnull()
                        ? ""
                        : to_string(diff.enum_domain_validation.get_value().second.get_value().publish));
    }

    LibHermes::Struct changes;
    if (!domain.empty())
    {
        changes.emplace(LibHermes::StructKey{"domain"}, LibHermes::StructValue{domain});
    }
    LibHermes::Struct result;
    if (!changes.empty())
    {
        result.emplace(LibHermes::StructKey{"changes"}, LibHermes::StructValue{changes});
    }

    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

LibHermes::Struct collect_domain_data_change(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    if (_notified_event != NotifiedEvent::updated)
    {
        return LibHermes::Struct{};
    }
    const auto history_id_bofore_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
    return collect_domain_update_data_change(
            LibFred::InfoDomainHistoryByHistoryid(*history_id_bofore_change).exec(_ctx).info_domain_data,
            LibFred::InfoDomainHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_domain_data);
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
