/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_email_content_keyset.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/collect_data/util/string_list_utils.hh"
#include "src/impl/exception.hh"

#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/registrable_object/keyset/info_keyset_data.hh"
#include "libfred/registrable_object/keyset/info_keyset_diff.hh"
#include "libfred/registrable_object/keyset/keyset_dns_key.hh"

#include <boost/algorithm/string/join.hpp>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

bool sort_by_key(const LibFred::DnsKey& lhs, const LibFred::DnsKey& rhs)
{
    return lhs.get_key() < rhs.get_key();
}

bool equal(const LibFred::DnsKey& lhs, const LibFred::DnsKey& rhs)
{
    return (lhs.get_flags() == rhs.get_flags()) &&
           (lhs.get_protocol() == rhs.get_protocol()) &&
           (lhs.get_alg() == rhs.get_alg()) &&
           (lhs.get_key() == rhs.get_key());
}

bool equal(const std::vector<LibFred::DnsKey>& lhs, const std::vector<LibFred::DnsKey>& rhs)
{
    if (lhs.size() != rhs.size())
    {
        return false;
    }
    for (std::vector<LibFred::DnsKey>::size_type idx = 0; idx < lhs.size(); ++idx)
    {
        if (!equal(lhs[idx], rhs[idx]))
        {
            return false;
        }
    }
    return true;
}

std::string dns_key_to_string(const LibFred::DnsKey& key)
{
    return
        "("
        "flags: " + std::to_string(key.get_flags()) + " "
        "protocol: " + std::to_string(key.get_protocol()) + " "
        "algorithm: " + std::to_string(key.get_alg()) + " "
        "key: " + key.get_key() +
        ")";
}

LibHermes::Struct collect_keyset_update_data_change(
        const LibFred::InfoKeysetData& _before,
        const LibFred::InfoKeysetData& _after)
{
    const LibFred::InfoKeysetDiff diff = diff_keyset_data(_before, _after);

    LibHermes::Struct keyset;
    if (diff.tech_contacts.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                keyset,
                LibHermes::StructKey{"tech_c"},
                boost::algorithm::join(Util::sort(Util::get_handles(diff.tech_contacts.get_value().first)), " "),
                boost::algorithm::join(Util::sort(Util::get_handles(diff.tech_contacts.get_value().second)), " "));
    }

    if (diff.dns_keys.isset())
    {
        std::vector<LibFred::DnsKey> sorted_keys_old = diff.dns_keys.get_value().first;
        std::sort(sorted_keys_old.begin(), sorted_keys_old.end(), sort_by_key);

        std::vector<LibFred::DnsKey> sorted_keys_new = diff.dns_keys.get_value().second;
        std::sort(sorted_keys_new.begin(), sorted_keys_new.end(), sort_by_key);

        if (!equal(sorted_keys_old, sorted_keys_new))
        {
            std::vector<LibHermes::StructValue> keyset_dnskey_old;
            std::vector<LibHermes::StructValue> keyset_dnskey_new;

            for (std::vector<LibFred::DnsKey>::size_type idx = 0; idx < sorted_keys_old.size(); ++idx)
            {
                keyset_dnskey_old.push_back(LibHermes::StructValue{dns_key_to_string(sorted_keys_old.at(idx))});
            }

            for (std::vector<LibFred::DnsKey>::size_type idx = 0; idx < sorted_keys_new.size(); ++idx)
            {
                keyset_dnskey_new.push_back(LibHermes::StructValue{dns_key_to_string(sorted_keys_new.at(idx))});
            }

            keyset.emplace(LibHermes::StructKey{"dnskey"},
                           LibHermes::StructValue{LibHermes::Struct{
                                    {LibHermes::StructKey{"old"}, LibHermes::StructValue{keyset_dnskey_old}},
                                    {LibHermes::StructKey{"new"}, LibHermes::StructValue{keyset_dnskey_new}}}});
        }
    }

    LibHermes::Struct changes;
    if (!keyset.empty())
    {
        changes.emplace(LibHermes::StructKey{"keyset"}, LibHermes::StructValue{keyset});
    }
    LibHermes::Struct result;
    if (!changes.empty())
    {
        result.emplace(LibHermes::StructKey{"changes"}, LibHermes::StructValue{changes});
    }

    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

LibHermes::Struct collect_keyset_data_change(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    if (_notified_event != NotifiedEvent::updated)
    {
        return LibHermes::Struct{};
    }
    const auto history_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
    return collect_keyset_update_data_change(
            LibFred::InfoKeysetHistoryByHistoryid(*history_id_before_change).exec(_ctx).info_keyset_data,
            LibFred::InfoKeysetHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_keyset_data);
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
