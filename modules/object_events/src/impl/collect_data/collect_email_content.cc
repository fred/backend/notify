/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/collect_data/collect_email_content.hh"

#include "src/impl/collect_data/collect_email_content_contact.hh"
#include "src/impl/collect_data/collect_email_content_domain.hh"
#include "src/impl/collect_data/collect_email_content_keyset.hh"
#include "src/impl/collect_data/collect_email_content_nsset.hh"

#include "src/impl/exception.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/domain/info_domain.hh"
#include "libfred/registrable_object/keyset/info_keyset.hh"
#include "libfred/registrable_object/nsset/info_nsset.hh"

#include "libfred/registrar/info_registrar.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

int to_email_template_id(Impl::ObjectType _object_type)
{
    switch (_object_type)
    {
        case ObjectType::contact: return 1;
        case ObjectType::domain: return 3;
        case ObjectType::keyset: return 4;
        case ObjectType::nsset: return 2;
    }
    throw std::logic_error{"unexpected value of ObjectType"};
}

LibHermes::Struct collect_common_email_content(
        const LibFred::OperationContext& _ctx,
        const Impl::NotificationRequest& _request)
{
    LibHermes::Struct data;

    data.emplace(LibHermes::StructKey{"ticket"},
                 LibHermes::StructValue{*_request.svtr_id});

    data.emplace(LibHermes::StructKey{"registrar"},
                 [](const auto& _reg) {
                     return LibHermes::StructValue{_reg.name.get_value_or("") + " (" + _reg.url.get_value_or("") + ")"};
                 }(LibFred::InfoRegistrarById(*_request.registrar_id).exec(_ctx).info_registrar_data));

    data.emplace(LibHermes::StructKey{"handle"},
                 [&]()
                 {
                     switch (_request.event.object_type)
                     {
                         case ObjectType::contact :
                             return LibHermes::StructValue{LibFred::InfoContactHistoryByHistoryid(*_request.history_id_after_change).exec(_ctx).info_contact_data.handle};
                         case ObjectType::domain :
                             return LibHermes::StructValue{LibFred::InfoDomainHistoryByHistoryid(*_request.history_id_after_change).exec(_ctx).info_domain_data.fqdn};
                         case ObjectType::keyset :
                             return LibHermes::StructValue{LibFred::InfoKeysetHistoryByHistoryid(*_request.history_id_after_change).exec(_ctx).info_keyset_data.handle};
                         case ObjectType::nsset :
                             return LibHermes::StructValue{LibFred::InfoNssetHistoryByHistoryid(*_request.history_id_after_change).exec(_ctx).info_nsset_data.handle};
                     }
                     throw std::logic_error{"unexpected ObjectType"};
                 }());

    data.emplace(LibHermes::StructKey{"type"}, LibHermes::StructValue{to_email_template_id(_request.event.object_type)});

    return data;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

LibHermes::Struct collect_email_content(
        const LibFred::OperationContext& _ctx,
        const Impl::NotificationRequest& _request)
{
    LibHermes::Struct data = collect_common_email_content(_ctx, _request);

    LibHermes::Struct object_type_specific_data;

    if (_request.event.object_type == ObjectType::contact)
    {
        object_type_specific_data = collect_contact_data_change(_ctx, _request.event.notified_event, _request.history_id_after_change);
    }
    else if (_request.event.object_type == ObjectType::domain)
    {
        object_type_specific_data = collect_domain_data_change(_ctx, _request.event.notified_event, _request.history_id_after_change);
    }
    else if (_request.event.object_type == ObjectType::keyset)
    {
        object_type_specific_data = collect_keyset_data_change(_ctx, _request.event.notified_event, _request.history_id_after_change);
    }
    else if (_request.event.object_type == ObjectType::nsset)
    {
        object_type_specific_data = collect_nsset_data_change(_ctx, _request.event.notified_event, _request.history_id_after_change);
    }
    else
    {
        throw std::logic_error{"unexpected ObjectType"};
    }

    if (!object_type_specific_data.empty())
    {

        for (LibHermes::Struct::const_iterator it = object_type_specific_data.begin();
             it != object_type_specific_data.end();
             ++it)
        {
            if (data.find(it->first) != data.end())
            {
                throw std::runtime_error{"data loss"};
            }
        }

        data.insert(object_type_specific_data.begin(), object_type_specific_data.end());
    }

    return data;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
