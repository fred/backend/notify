/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/impl/collect_data/collect_recipients_to_notify_contact_event.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"

#include "src/impl/collect_data/util/get_previous_object_historyid.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/contact/info_contact_diff.hh"
#include "libfred/registrable_object/contact/info_contact_data.hh"

#include <boost/foreach.hpp>
#include <boost/algorithm/string/join.hpp>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace {

boost::optional<Recipient> get_recipient(
        const LibFred::OperationContext& _ctx,
        const HistoryId& _history_id)
{
    const auto contact_data =
            LibFred::InfoContactHistoryByHistoryid(*_history_id).exec(_ctx).info_contact_data;

    if (!contact_data.notifyemail.get_value_or("").empty())
    {
        return Recipient(ContactEmailAddress{contact_data.notifyemail.get_value()}, ContactUuid{get_raw_value_from(contact_data.uuid)});
    }
    return boost::none;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

std::set<Recipient> collect_recipients_to_notify_contact_event(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change)
{
    std::set<Recipient> recipients;

    // always notify new value of notifyemail if present
    const auto recipient_after_change = get_recipient(_ctx, _history_id_after_change);
    if (recipient_after_change != boost::none)
    {
        recipients.insert(*recipient_after_change);
    }

    // if there were possibly other old values notify those as well
    if (_notified_event == NotifiedEvent::updated)
    {
        const auto history_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
        const auto recipient_before_change = get_recipient(_ctx, history_id_before_change);
        if (recipient_before_change != boost::none)
        {
            recipients.insert(*recipient_before_change);
        }
    }
    return recipients;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
