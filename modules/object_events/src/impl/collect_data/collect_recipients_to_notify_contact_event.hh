/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COLLECT_RECIPIENTS_TO_NOTIFY_CONTACT_EVENT_HH_51DAEFD8284E4AB98BF97F70898531EC
#define COLLECT_RECIPIENTS_TO_NOTIFY_CONTACT_EVENT_HH_51DAEFD8284E4AB98BF97F70898531EC

#include "src/impl/notification_request.hh"
#include "src/impl/notified_event.hh"
#include "src/impl/recipient.hh"

#include "libfred/opcontext.hh"

#include <set>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

std::set<Recipient> collect_recipients_to_notify_contact_event(
        const LibFred::OperationContext& _ctx,
        const NotifiedEvent& _notified_event,
        const HistoryId& _history_id_after_change);

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
