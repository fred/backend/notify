/*
 * Copyright (C) 2022-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/collect_data/collect_email_content_contact.hh"

#include "src/impl/collect_data/util/add_old_new_pair_if_different.hh"
#include "src/impl/collect_data/util/get_previous_object_historyid.hh"
#include "src/impl/exception.hh"

#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/contact/info_contact_data.hh"
#include "libfred/registrable_object/contact/info_contact_diff.hh"
#include "libfred/registrable_object/contact/place_address.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

namespace Convert {

void aggregate_nonempty(std::vector<std::string>& _target, const std::string& _raw_input)
{
    if (!_raw_input.empty())
    {
        _target.push_back(_raw_input);
    }
}

std::string to_string(const LibFred::Contact::PlaceAddress& _address)
{
    std::vector<std::string> non_empty_parts;

    aggregate_nonempty(non_empty_parts, _address.street1);
    aggregate_nonempty(non_empty_parts, _address.street2.get_value_or(""));
    aggregate_nonempty(non_empty_parts, _address.street3.get_value_or(""));
    aggregate_nonempty(non_empty_parts, _address.stateorprovince.get_value_or(""));
    aggregate_nonempty(non_empty_parts, _address.postalcode);
    aggregate_nonempty(non_empty_parts, _address.city);
    aggregate_nonempty(non_empty_parts, _address.country);

    return boost::join(non_empty_parts, ", ");
}

std::string to_string(const LibFred::ContactAddress& _address)
{
    std::vector<std::string> non_empty_parts;

    aggregate_nonempty(non_empty_parts, _address.company_name.get_value_or(""));
    aggregate_nonempty(
        non_empty_parts,
        to_string(static_cast<const LibFred::Contact::PlaceAddress&>(_address)));

    return boost::join(non_empty_parts, ", ");
}

std::string to_string(bool _input)
{
    return _input ? "1" : "0";
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::Convert

namespace {

/* Yes we are using database "enum" values as e-mail template parameters. It's flexible. And it works! A vubec! */
std::string translate_ssntypes(const Nullable<LibFred::PersonalIdUnion> &_nullable_personal_id)
{
    if (_nullable_personal_id.isnull() ||
        _nullable_personal_id.get_value().get_type().empty())
    {
        return "";
    }

    const std::string type = _nullable_personal_id.get_value().get_type();

    if (type == "PASS")
    {
        return "PASSPORT";
    }

    if (type == "RC"   ||
        type == "OP"   ||
        type == "ICO"  ||
        type == "MPSV" ||
        type == "BIRTHDAY")
    {
        return type;
    }

    return "UNKNOWN";
}

std::string to_template_handle(LibFred::ContactAddressType::Value _type)
{
    switch (_type)
    {
        case LibFred::ContactAddressType::MAILING      : return "mailing";
        case LibFred::ContactAddressType::BILLING      : return "billing";
        case LibFred::ContactAddressType::SHIPPING     : return "shipping";
        case LibFred::ContactAddressType::SHIPPING_2   : return "shipping_2";
        case LibFred::ContactAddressType::SHIPPING_3   : return "shipping_3";
    };
    throw std::logic_error{"unexpected LibFred::ContactAddressType"};
}

LibHermes::Struct collect_contact_create_data_change(const LibFred::InfoContactData& _fresh)
{
    const Nullable<LibFred::PersonalIdUnion> nullable_personal_id =
            (_fresh.ssntype.isnull() || _fresh.ssn.isnull())
                    ? Nullable<LibFred::PersonalIdUnion>()
                    : Nullable<LibFred::PersonalIdUnion>(
                              LibFred::PersonalIdUnion::get_any_type(_fresh.ssntype.get_value(), _fresh.ssn.get_value()));

    LibHermes::Struct addresses{
        {LibHermes::StructKey{"permanent"}, LibHermes::StructValue{Convert::to_string(_fresh.place.get_value_or_default())}}};

    for (const auto& type : LibFred::ContactAddressType::get_all())
    {
        const auto address_it = _fresh.addresses.find(type);
        addresses[LibHermes::StructKey{to_template_handle(type)}] =
                LibHermes::StructValue{address_it != _fresh.addresses.end() ? Convert::to_string(address_it->second) : ""};
    }

    return LibHermes::Struct{
        {LibHermes::StructKey{"fresh"},
         LibHermes::StructValue{LibHermes::Struct{
            {LibHermes::StructKey{"object"},
                    LibHermes::StructValue{LibHermes::Struct{
                            {LibHermes::StructKey{"authinfo"}, LibHermes::StructValue{_fresh.authinfopw}}}}},
            {LibHermes::StructKey{"contact"},
                    LibHermes::StructValue{LibHermes::Struct{
                            {LibHermes::StructKey{"name"}, LibHermes::StructValue{_fresh.name.get_value_or_default()}},
                            {LibHermes::StructKey{"org"}, LibHermes::StructValue{_fresh.organization.get_value_or_default()}},
                            {LibHermes::StructKey{"address"}, LibHermes::StructValue{addresses}},
                            {LibHermes::StructKey{"telephone"}, LibHermes::StructValue{_fresh.telephone.get_value_or_default()}},
                            {LibHermes::StructKey{"fax"}, LibHermes::StructValue{_fresh.fax.get_value_or_default()}},
                            {LibHermes::StructKey{"email"}, LibHermes::StructValue{_fresh.email.get_value_or_default()}},
                            {LibHermes::StructKey{"notify_email"}, LibHermes::StructValue{_fresh.notifyemail.get_value_or_default()}},
                            {LibHermes::StructKey{"ident_type"}, LibHermes::StructValue{translate_ssntypes(nullable_personal_id)}},
                            {LibHermes::StructKey{"ident"}, LibHermes::StructValue{nullable_personal_id.get_value_or_default().get()}},
                            {LibHermes::StructKey{"vat"}, LibHermes::StructValue{_fresh.vat.get_value_or_default()}},
                            {LibHermes::StructKey{"disclose"},
                                    LibHermes::StructValue{LibHermes::Struct{
                                            {LibHermes::StructKey{"name"}, LibHermes::StructValue{_fresh.disclosename}},
                                            {LibHermes::StructKey{"org"}, LibHermes::StructValue{_fresh.discloseorganization}},
                                            {LibHermes::StructKey{"email"}, LibHermes::StructValue{_fresh.discloseemail}},
                                            {LibHermes::StructKey{"address"}, LibHermes::StructValue{_fresh.discloseaddress}},
                                            {LibHermes::StructKey{"notify_email"}, LibHermes::StructValue{_fresh.disclosenotifyemail}},
                                            {LibHermes::StructKey{"ident"}, LibHermes::StructValue{_fresh.discloseident}},
                                            {LibHermes::StructKey{"vat"}, LibHermes::StructValue{_fresh.disclosevat}},
                                            {LibHermes::StructKey{"telephone"}, LibHermes::StructValue{_fresh.disclosetelephone}},
                                            {LibHermes::StructKey{"fax"}, LibHermes::StructValue{_fresh.disclosefax}}}}}}}}}}}};
}

LibHermes::Struct collect_contact_update_data_change(
        const LibFred::InfoContactData& _before,
        const LibFred::InfoContactData& _after)
{
    const LibFred::InfoContactDiff diff = diff_contact_data(_before, _after);

    LibHermes::Struct contact;
    if (diff.name.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"name"},
                diff.name.get_value().first.get_value_or(""),
                diff.name.get_value().second.get_value_or(""));
    }

    if (diff.organization.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"org"},
                diff.organization.get_value().first.get_value_or(""),
                diff.organization.get_value().second.get_value_or(""));
    }

    LibHermes::Struct contact_address;
    if (diff.place.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_address,
                LibHermes::StructKey{"permanent"},
                Convert::to_string(diff.place.get_value().first.get_value_or(LibFred::Contact::PlaceAddress())),
                Convert::to_string(diff.place.get_value().second.get_value_or(LibFred::Contact::PlaceAddress())));
    }

    if (diff.addresses.isset())
    {
        const auto old_addresses = diff.addresses.get_value().first;
        const auto new_addresses = diff.addresses.get_value().second;

        for (const auto& type : LibFred::ContactAddressType::get_all())
        {
            const auto old_it = old_addresses.find(type);
            const auto new_it = new_addresses.find(type);

            Util::add_old_new_changes_pair_if_different(
                    contact_address,
                    LibHermes::StructKey{to_template_handle(type)},
                    old_it != old_addresses.end() ? Convert::to_string(old_it->second) : "",
                    new_it != new_addresses.end() ? Convert::to_string(new_it->second) : "");
        }
    }

    if (diff.telephone.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"telephone"},
                diff.telephone.get_value().first.get_value_or(""),
                diff.telephone.get_value().second.get_value_or(""));
    }

    if (diff.fax.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"fax"},
                diff.fax.get_value().first.get_value_or(""),
                diff.fax.get_value().second.get_value_or(""));
    }

    if (diff.email.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"email"},
                diff.email.get_value().first.get_value_or(""),
                diff.email.get_value().second.get_value_or(""));
    }

    if (diff.notifyemail.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"notify_email"},
                diff.notifyemail.get_value().first.get_value_or(""),
                diff.notifyemail.get_value().second.get_value_or(""));
    }

    if (diff.personal_id.isset())
    {
        const auto nullable_personal_id_a = diff.personal_id.get_value().first;
        const auto nullable_personal_id_b = diff.personal_id.get_value().second;
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"ident_type"},
                translate_ssntypes(nullable_personal_id_a),
                translate_ssntypes(nullable_personal_id_b));
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"ident"},
                nullable_personal_id_a.get_value_or_default().get(),
                nullable_personal_id_b.get_value_or_default().get());
    }

    if (diff.vat.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact,
                LibHermes::StructKey{"vat"},
                diff.vat.get_value().first.get_value_or(""),
                diff.vat.get_value().second.get_value_or(""));
    }

    LibHermes::Struct contact_disclose;
    if (diff.disclosename.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"name"},
                diff.disclosename.get_value().first,
                diff.disclosename.get_value().second);
    }

    if (diff.discloseorganization.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"org"},
                diff.discloseorganization.get_value().first,
                diff.discloseorganization.get_value().second);
    }

    if (diff.discloseemail.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"email"},
                diff.discloseemail.get_value().first,
                diff.discloseemail.get_value().second);
    }

    if (diff.discloseaddress.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"address"},
                diff.discloseaddress.get_value().first,
                diff.discloseaddress.get_value().second);
    }

    if (diff.disclosenotifyemail.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"notify_email"},
                diff.disclosenotifyemail.get_value().first,
                diff.disclosenotifyemail.get_value().second);
    }

    if (diff.discloseident.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"ident"},
                diff.discloseident.get_value().first,
                diff.discloseident.get_value().second);
    }

    if (diff.disclosevat.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"vat"},
                diff.disclosevat.get_value().first,
                diff.disclosevat.get_value().second);
    }

    if (diff.disclosetelephone.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"telephone"},
                diff.disclosetelephone.get_value().first,
                diff.disclosetelephone.get_value().second);
    }

    if (diff.disclosefax.isset())
    {
        Util::add_old_new_changes_pair_if_different(
                contact_disclose,
                LibHermes::StructKey{"fax"},
                diff.disclosefax.get_value().first,
                diff.disclosefax.get_value().second);
    }

    if (!contact_address.empty())
    {
        contact.emplace(LibHermes::StructKey{"address"}, LibHermes::StructValue{contact_address});
    }
    if (!contact_disclose.empty())
    {
        contact.emplace(LibHermes::StructKey{"disclose"}, LibHermes::StructValue{contact_disclose});
    }
    LibHermes::Struct changes;
    if (!contact.empty())
    {
        changes.emplace(LibHermes::StructKey{"contact"}, LibHermes::StructValue{contact});
    }
    LibHermes::Struct result;
    if (!changes.empty())
    {
        result.emplace(LibHermes::StructKey{"changes"}, LibHermes::StructValue{changes});
    }

    return result;
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData::{anonymous}

LibHermes::Struct collect_contact_data_change(
        const LibFred::OperationContext& _ctx,
        NotifiedEvent _notified_event,
        const HistoryId& _history_id_after_change)
{
    if (_notified_event == NotifiedEvent::created )
    {
        return collect_contact_create_data_change(
                LibFred::InfoContactHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_contact_data);
    }
    if (_notified_event == NotifiedEvent::updated)
    {
        const auto histor_id_before_change = Util::get_previous_object_historyid(_ctx, _history_id_after_change);
        return collect_contact_update_data_change(
                LibFred::InfoContactHistoryByHistoryid(*histor_id_before_change).exec(_ctx).info_contact_data,
                LibFred::InfoContactHistoryByHistoryid(*_history_id_after_change).exec(_ctx).info_contact_data);
    }
    return LibHermes::Struct{};
}

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
