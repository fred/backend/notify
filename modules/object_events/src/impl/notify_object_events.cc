/*
 * Copyright (C) 2021-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/impl/notify_object_events.hh"

#include "src/impl/collect_data/collect_email_content.hh"
#include "src/impl/collect_data/collect_recipients.hh"
#include "src/impl/exception.hh"
#include "src/impl/notified_event.hh"
#include "src/impl/object_type.hh"
#include "src/impl/strong_type.hh"

#include "src/util/util.hh"

#include "libfred/object/registry_object_type.hh"

#include "libhermes/libhermes.hh"
#include "libhermes/struct.hh"

#include "liblog/liblog.hh"

#include "libpg/pg_rw_transaction.hh"
#include "libpg/query.hh"

#include "libstrong/type.hh"

#include <boost/algorithm/string/trim.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <set>
#include <vector>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

namespace {

std::string to_string(ObjectType _value)
{
    switch (_value)
    {
        case ObjectType::contact: return "contact";
        case ObjectType::nsset: return "nsset";
        case ObjectType::domain: return "domain";
        case ObjectType::keyset: return "keyset";
    }
    throw std::logic_error("value doesn't exist in ObjectType");
}

std::string to_string(NotifiedEvent _value)
{
    switch (_value)
    {
        case NotifiedEvent::created: return "created";
        case NotifiedEvent::updated: return "updated";
        case NotifiedEvent::transferred: return "transferred";
        case NotifiedEvent::deleted: return "deleted";
        case NotifiedEvent::renewed: return "renewed";
    }
    throw std::logic_error("value doesn't exist in NotifiedEvent");
}

/**
 * Helps to implement from_db_handle function using to_db_handle conversion function.
 * @tparam S type of source value
 * @tparam D type of destination value
 * @tparam items number of all possible destination values
 * @param src source value
 * @param set_of_results C array of all possible destination values
 * @param forward_transformation function which transforms D value into S value
 * @return matching counterpart value
 * @throw std::invalid_argument in case that conversion fails
 */
template <class S, class D, unsigned items>
D inverse_transformation(const S& src, const D (&set_of_results)[items], S (*forward_transformation)(D))
{
    const D* const end_of_results = set_of_results + items;
    for (const D* result_candidate_ptr = set_of_results; result_candidate_ptr < end_of_results; ++result_candidate_ptr)
    {
        if (forward_transformation(*result_candidate_ptr) == src)
        {
            return *result_candidate_ptr;
        }
    }
    throw std::invalid_argument("conversion does not exist");
}

template <typename T>
T from_string(const std::string& _str);

template <>
ObjectType from_string<ObjectType>(const std::string& _str)
{
    static constexpr ObjectType possible_results[] =
            {
                ObjectType::contact,
                ObjectType::nsset,
                ObjectType::domain,
                ObjectType::keyset
            };
    return inverse_transformation(_str, possible_results, to_string);
}

template <>
NotifiedEvent from_string<NotifiedEvent>(const std::string& _str)
{
    static constexpr NotifiedEvent possible_results[] =
            {
                NotifiedEvent::created,
                NotifiedEvent::updated,
                NotifiedEvent::transferred,
                NotifiedEvent::deleted,
                NotifiedEvent::renewed
            };
    return inverse_transformation(_str, possible_results, to_string);
}

using ContactId = LibStrong::Type<int, struct ContactIdTag_>;
using ObjectUuid = LibStrong::Type<boost::uuids::uuid, struct ObjectUuidTag_>;
using TimeOfValidity = LibStrong::Type<boost::posix_time::ptime, struct TimeOfValidityTag_>;
using TemplateNameEmailSubject = StrongString<struct TemplateNameEmailSubjectTag_>;
using TemplateNameEmailBody = StrongString<struct TemplateNameEmailBodyTag_>;

LibHermes::Email::Type make_libhermes_email_type(const NotifiedEvent _notified_event)
{
    switch (_notified_event)
    {
        case NotifiedEvent::created:
            return LibHermes::Email::Type{"notification_create"};
        case NotifiedEvent::updated:
            return LibHermes::Email::Type{"notification_update"};
        case NotifiedEvent::transferred:
            return LibHermes::Email::Type{"notification_transfer"};
        case NotifiedEvent::renewed:
            return LibHermes::Email::Type{"notification_renew"};
        case NotifiedEvent::deleted:
            return LibHermes::Email::Type{"notification_delete"};
    }
    throw std::logic_error("value doesn't exist in NotifiedEvent");
}

TemplateNameEmailSubject get_template_name_email_subject(
        const NotifiedEvent _notified_event,
        const TemplateNames& _template_names)
{
    auto find_template_name_email_subject = [&](const auto key) -> TemplateNameEmailSubject
    {
        auto value_iter = _template_names.find(key);
        if (value_iter != _template_names.end())
        {
            return TemplateNameEmailSubject{value_iter->second};
        }
        throw std::logic_error("key/value pair doesn't exist in TemplateNames");
    };

    switch (_notified_event)
    {
        case NotifiedEvent::created:
            return find_template_name_email_subject(TemplateName::template_name_email_create_subject);
        case NotifiedEvent::updated:
            return find_template_name_email_subject(TemplateName::template_name_email_update_subject);
        case NotifiedEvent::transferred:
            return find_template_name_email_subject(TemplateName::template_name_email_transfer_subject);
        case NotifiedEvent::renewed:
            return find_template_name_email_subject(TemplateName::template_name_email_renew_subject);
        case NotifiedEvent::deleted:
            return find_template_name_email_subject(TemplateName::template_name_email_delete_subject);
    }
    throw std::logic_error("value doesn't exist in NotifiedEvent");
}

TemplateNameEmailBody get_template_name_email_body(
        const NotifiedEvent _notified_event,
        const TemplateNames& _template_names)
{
    auto find_template_name_email_body = [&](const auto key) -> TemplateNameEmailBody
    {
        auto value_iter = _template_names.find(key);
        if (value_iter != _template_names.end())
        {
            return TemplateNameEmailBody{value_iter->second};
        }
        throw std::logic_error("key/value pair doesn't exist in TemplateNames");
    };

    switch (_notified_event)
    {
        case NotifiedEvent::created:
            return find_template_name_email_body(TemplateName::template_name_email_create_body);
        case NotifiedEvent::updated:
            return find_template_name_email_body(TemplateName::template_name_email_update_body);
        case NotifiedEvent::transferred:
            return find_template_name_email_body(TemplateName::template_name_email_transfer_body);
        case NotifiedEvent::renewed:
            return find_template_name_email_body(TemplateName::template_name_email_renew_body);
        case NotifiedEvent::deleted:
            return find_template_name_email_body(TemplateName::template_name_email_delete_body);
    }
    throw std::logic_error("value doesn't exist in NotifiedEvent");
}

LibHermes::Reference::Type to_libhermes_reference_type(ObjectType _object_type)
{
    switch (_object_type)
    {
        case ObjectType::contact:
            return LibHermes::Reference::Type{"contact"};
        case ObjectType::domain:
            return LibHermes::Reference::Type{"domain"};
        case ObjectType::nsset:
            return LibHermes::Reference::Type{"nsset"};
        case ObjectType::keyset:
            return LibHermes::Reference::Type{"keyset"};
    }
    throw std::logic_error("unexpected value of ObjectType");
}

std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> to_libhermes_recipients(const std::set<Recipient>& _recipients)
{
    std::map<LibHermes::Email::RecipientEmail, std::set<LibHermes::Email::RecipientUuid>> result;
    for (const auto& recipient : _recipients)
    {
        result[LibHermes::Email::RecipientEmail{*recipient.email}].insert(LibHermes::Email::RecipientUuid{*recipient.uuid});
    }
    return result;
}

std::string join(const std::set<Recipient>& _recipients)
{
    std::string result;
    for (const auto& recipient : _recipients)
    {
        result += result.empty() ? *recipient.email : "," + *recipient.email;
    }
    return result;
}

struct Request
{
    HistoryId history_id;
    NotifiedEvent notified_event;
};

void process_one_notification_request(
        LibFred::OperationContext& _ctx,
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive,
        const TemplateNames& _template_names,
        const EventAgeDaysMax& _event_age_days_max,
        boost::optional<Request>& _prev_processed_request)
{
    try
    {
        const auto query_has_params = _prev_processed_request != boost::none;

        std::string query =
                // clang-format off
            // lock exclusively
            "WITH locked AS ("
                "SELECT change, "
                       "done_by_registrar, "
                       "historyid_post_change, "
                       "svtrid "
                  "FROM notification.notification_queue nq ";
        if (query_has_params)
        {
            query += "WHERE ROW(nq.historyid_post_change, nq.change) > ROW($1::BIGINT, $2::notification.notified_event) ";
        }
        query += "ORDER BY nq.historyid_post_change, nq.change "
                   "FOR UPDATE SKIP LOCKED "
                 "LIMIT 1"
            ") "
            // ...and delete (it is locked until commit and deleted immediately after)
            "DELETE "
              "FROM notification.notification_queue AS nq "
             "USING locked "
             "WHERE nq.change = locked.change "
               "AND nq.done_by_registrar = locked.done_by_registrar "
               "AND nq.historyid_post_change = locked.historyid_post_change "
               "AND nq.svtrid = locked.svtrid "
            "RETURNING "
                "locked.*, "
                "(SELECT eot.name "
                   "FROM object_history oh "
                   "JOIN object_registry oreg USING(id) "
                   "JOIN enum_object_type eot ON oreg.type = eot.id "
                  "WHERE oh.historyid = locked.historyid_post_change) "
                     "AS object_type_, "
                "(SELECT oreg.uuid "
                   "FROM object_history oh "
                   "JOIN object_registry oreg USING(id) "
                  "WHERE oh.historyid = locked.historyid_post_change) "
                     "AS object_uuid_, "
                 "(SELECT NOW()::DATE - valid_from::DATE FROM history WHERE id = locked.historyid_post_change) AS event_age_days_";
        // clang-format on

        const auto dbres =
                   query_has_params
                   ? _ctx.get_conn().exec_params(query, Database::query_param_list(*(_prev_processed_request->history_id))(to_string(_prev_processed_request->notified_event)))
                   : _ctx.get_conn().exec(query);

        if (dbres.size() < 1)
        {
            const auto message = "no record found in notification.notification_queue";
            LIBLOG_INFO("{}", message);
            _prev_processed_request = boost::none;
            return;
        }

        const ObjectEvent object_event{
                from_string<ObjectType>(static_cast<std::string>(dbres[0]["object_type_"])),
                from_string<NotifiedEvent>(static_cast<std::string>(dbres[0]["change"]))};

        const NotificationRequest notification_request{
                object_event,
                RegistrarId{static_cast<unsigned long long>(dbres[0]["done_by_registrar"])},
                HistoryId{static_cast<unsigned long long>(dbres[0]["historyid_post_change"])},
                SvtrId{static_cast<std::string>(dbres[0]["svtrid"])}};

        _prev_processed_request = Request{notification_request.history_id_after_change, object_event.notified_event};

        const auto object_uuid = ObjectUuid{boost::uuids::string_generator{}(static_cast<std::string>(dbres[0]["object_uuid_"]))};
        const auto event_age_days = static_cast<unsigned int>(dbres[0]["event_age_days_"]);

        if (event_age_days > *_event_age_days_max)
        {
            LIBLOG_INFO("event older than configuration parameter 'event_age_days_max' ({})", *_event_age_days_max);
            return;
        }

        const auto template_name_email_subject =
                get_template_name_email_subject(notification_request.event.notified_event, _template_names);

        const auto template_name_email_body =
                get_template_name_email_body(notification_request.event.notified_event, _template_names);

        const auto recipients =
                CollectData::collect_recipients(
                        _ctx,
                        notification_request.event,
                        notification_request.history_id_after_change);

        if (recipients.empty())
        {
            LIBLOG_INFO(
                    "event=\"{}\" object_type=\"{}\" done_by_registrar=\"{}\" history_id_after_change=\"{}\" svtrid=\"{}\" "
                    "no recipients found",
                    to_string(object_event.notified_event),
                    to_string(object_event.object_type),
                    *notification_request.registrar_id,
                    *notification_request.history_id_after_change,
                    *notification_request.svtr_id);
            return;
        }

        const auto template_parameters =
                CollectData::collect_email_content(
                        _ctx,
                        notification_request);

        // Ticket #6547 send update notifications only if there are some changes
        if (object_event.notified_event == NotifiedEvent::updated)
        {
            const auto changes = template_parameters.find(LibHermes::StructKey{"changes"});
            if (changes == template_parameters.end())
            {
                LIBLOG_INFO(
                        "event=\"{}\" object_type=\"{}\" done_by_registrar=\"{}\" history_id_after_change=\"{}\" svtrid=\"{}\" "
                        "no changes was done according to template parameter 'changes'",
                        to_string(object_event.notified_event),
                        to_string(object_event.object_type),
                        *notification_request.registrar_id,
                        *notification_request.history_id_after_change,
                        *notification_request.svtr_id);
                return;
            }
        }

        LIBLOG_INFO(
                "event=\"{}\" object_type=\"{}\" done_by_registrar=\"{}\" history_id_after_change=\"{}\" svtrid=\"{}\" "
                "completed - transaction not yet comitted",
                to_string(object_event.notified_event),
                to_string(object_event.object_type),
                *notification_request.registrar_id,
                *notification_request.history_id_after_change,
                *notification_request.svtr_id);

        LibHermes::Connection<LibHermes::Service::EmailMessenger> connection{
            LibHermes::Connection<LibHermes::Service::EmailMessenger>::ConnectionString{
                *_messenger_endpoint}};

        auto email =
                LibHermes::Email::make_minimal_email(
                        to_libhermes_recipients(recipients),
                        LibHermes::Email::SubjectTemplate{*template_name_email_subject},
                        LibHermes::Email::BodyTemplate{*template_name_email_body});
        email.type = make_libhermes_email_type(object_event.notified_event);
        email.context = template_parameters;

        try
        {
            LibHermes::Email::batch_send(
                    connection,
                    email,
                    LibHermes::Email::Archive{*_messenger_archive},
                    {LibHermes::Reference{
                            to_libhermes_reference_type(notification_request.event.object_type),
                            LibHermes::Reference::Value{boost::uuids::to_string(*object_uuid)}}});
        }
        catch (const LibHermes::Email::SendFailed& e)
        {
            LIBLOG_WARNING(
                    "gRPC exception caught while sending email about event of object with uuid {}: gRPC error code: {}, error message: {} "
                    "event: {} object_type: {} event_done_by_registrar: {} history_id_after_change: {} "
                    "failed_recipients: {} email_template_subject: {} email_template_body: {} "
                    "grpc_message_json: {}",
                    boost::uuids::to_string(*object_uuid),
                    e.error_code(),
                    e.error_message(),
                    to_string(object_event.notified_event),
                    to_string(object_event.object_type),
                    *notification_request.registrar_id,
                    *notification_request.history_id_after_change,
                    join(recipients),
                    *template_name_email_subject,
                    *template_name_email_body,
                    e.grpc_message_json());
            throw NotificationFailed{};
        }
        catch (const std::exception& e)
        {
            LIBLOG_WARNING(
                    "std::exception caught while sending email about event of object with uuid {}: {} "
                    "event: {} object_type: {} event_done_by_registrar: {} history_id_after_change: {} "
                    "failed_recipients: {} email_template_subject: {} email_template_body: {}",
                    boost::uuids::to_string(*object_uuid),
                    e.what(),
                    to_string(object_event.notified_event),
                    to_string(object_event.object_type),
                    *notification_request.registrar_id,
                    *notification_request.history_id_after_change,
                    join(recipients),
                    *template_name_email_subject,
                    *template_name_email_body);
            throw NotificationFailed{};
        }
        catch (...)
        {
            LIBLOG_WARNING(
                    "exception caught while sending email about event of object with uuid {} "
                    "event: {} object_type: {} event_done_by_registrar: {} history_id_after_change: {} "
                    "failed_recipients: {} email_template_subject: {} email_template_body: {}",
                    boost::uuids::to_string(*object_uuid),
                    to_string(object_event.notified_event),
                    to_string(object_event.object_type),
                    *notification_request.registrar_id,
                    *notification_request.history_id_after_change,
                    join(recipients),
                    *template_name_email_subject,
                    *template_name_email_body);
            throw NotificationFailed{};
        }
        return;
    }
    catch (const std::exception& e)
    {
        const std::string what_string(e.what());
        // legacy solution, no means to identify this error better way in legacy db layer implemantation
        if (what_string.find("could not obtain lock on row in relation \"notification.notification_queue\"") != std::string::npos)
        {
            throw FailedToLockRequest{e.what()};
        }
        throw;
    }
}

} // namespace Fred::Notify::ObjectEvents::Impl::{anonymous}

void notify_object_events(
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive,
        const TemplateNames& _template_names,
        const EventAgeDaysMax& _event_age_days_max)
{
    boost::optional<Request> last_processed_request;
    while (true)
    {
        try
        {
            LibFred::OperationContextCreator ctx;
            process_one_notification_request(
                    ctx,
                    _messenger_endpoint,
                    _messenger_archive,
                    _template_names,
                    _event_age_days_max,
                    last_processed_request);
            ctx.commit_transaction();
            if (last_processed_request == boost::none)
            {
                break;
            }
        }
        catch (const NotificationFailed& e)
        {
            continue;
        }
        catch (const FailedToLockRequest& e)
        {
            const auto error = "unable to get data from notification queue - another process is running";
            LIBLOG_ERROR("{}", error);
            throw std::runtime_error(error);
        }
    }
}

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
