/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_EVENT_HH_A895C59D9FDA4E75B0F928413B0734FA
#define OBJECT_EVENT_HH_A895C59D9FDA4E75B0F928413B0734FA

#include "src/impl/notified_event.hh"
#include "src/impl/object_type.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

struct ObjectEvent
{
    ObjectType object_type;
    NotifiedEvent notified_event;
};

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
