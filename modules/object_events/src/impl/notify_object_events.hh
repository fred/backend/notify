/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFY_OBJECT_EVENTS_HH_6C8F6059F4C64BE89FC08996A26258B4
#define NOTIFY_OBJECT_EVENTS_HH_6C8F6059F4C64BE89FC08996A26258B4

#include "src/impl/strong_type.hh"

#include <unordered_map>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

struct Notification
{
    enum struct ObjectType
    {
        contact,
        nsset,
        domain,
        keyset
    };

    enum struct NotifiedEvent
    {
        created,
        updated,
        transferred,
        deleted,
        renewed
    };
};

using MessengerEndpoint = StrongString<struct MessengerEndpointTag_>;
using MessengerArchive = LibStrong::Type<bool, struct MessengerArchiveTag_>;

enum struct TemplateName
{
    template_name_email_create_subject,
    template_name_email_create_body,
    template_name_email_update_subject,
    template_name_email_update_body,
    template_name_email_transfer_subject,
    template_name_email_transfer_body,
    template_name_email_renew_subject,
    template_name_email_renew_body,
    template_name_email_delete_subject,
    template_name_email_delete_body
};

using TemplateNames = std::unordered_map<TemplateName, std::string>;

using EventAgeDaysMax = LibStrong::Type<unsigned int, struct DaysOldMax_>;

void notify_object_events(
        const MessengerEndpoint& _messenger_endpoint,
        const MessengerArchive& _messenger_archive,
        const TemplateNames& _template_names,
        const EventAgeDaysMax& _event_age_days_max);

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
