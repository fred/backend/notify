/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef EXCEPTION_HH_F99BD713F17340F5BD32C75F00A2230D
#define EXCEPTION_HH_F99BD713F17340F5BD32C75F00A2230D

#include <exception>
#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

struct Exception : std::exception
{
public:
    explicit Exception(std::string _msg);
    const char* what() const noexcept override;

private:
    const std::string msg_;
};

struct InvalidUuid : Exception
{
    explicit InvalidUuid(std::string _msg);
};

struct InvalidObjectType : Exception
{
    explicit InvalidObjectType(std::string _msg);
};

struct InvalidNotifiedEvent : Exception
{
    explicit InvalidNotifiedEvent(std::string _msg);
};

struct FailedToLockRequest : Exception
{
    explicit FailedToLockRequest(std::string _msg);
};

struct UnknownHistoryId : Exception
{
    explicit UnknownHistoryId(std::string _msg);
};

struct NotificationFailed : Exception
{
    explicit NotificationFailed();
};

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
