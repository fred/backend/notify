/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STRONG_TYPE_HH_48271533F31A4A799AC31B127D40DF66
#define STRONG_TYPE_HH_48271533F31A4A799AC31B127D40DF66

#include "libstrong/type.hh"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/uuid/uuid.hpp>

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

namespace StrongTypeSkill = LibStrong::Skill;

template <typename Type, typename Tag, template <typename> class ...Skills>
using StrongType = LibStrong::TypeWithSkills<Type, Tag, Skills...>;

template <typename StrongType>
using Optional = LibStrong::Optional<StrongType>;

template <typename Tag>
using StrongString = StrongType<std::string, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

template <typename Tag>
using StrongUuid = StrongType<boost::uuids::uuid, Tag, StrongTypeSkill::Comparable, StrongTypeSkill::Streamable>;

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
