/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFICATION_REQUEST_HH_CE354237BDDF4B1888044FFAFAA25770
#define NOTIFICATION_REQUEST_HH_CE354237BDDF4B1888044FFAFAA25770

#include "src/impl/object_event.hh"
#include "src/impl/strong_type.hh"

#include <libstrong/type.hh>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {

using RegistrarId = LibStrong::Type<unsigned long long, struct RegistrarIdTag_>;
using HistoryId = LibStrong::Type<unsigned long long, struct HistoryIdTag_>;
using SvtrId = StrongString<struct SvtrIdTag_>;

struct NotificationRequest
{
    ObjectEvent event;
    RegistrarId registrar_id;
    HistoryId history_id_after_change;
    SvtrId svtr_id;
};

} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
