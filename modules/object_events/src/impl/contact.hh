/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONTACT_HH_09601CA90071419F82610A6BE1BECBAB
#define CONTACT_HH_09601CA90071419F82610A6BE1BECBAB

#include "src/impl/recipient.hh"
#include "src/impl/strong_type.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Impl {
namespace CollectData {

using ContactId = LibStrong::Type<unsigned long long, struct ContactIdTag_>;
struct Contact
{
    ContactId id;
    ContactUuid uuid;

    explicit Contact(ContactId _id, ContactUuid _uuid)
        : id(std::move(_id)),
          uuid(std::move(_uuid))
    {
    }

    bool operator<(const Contact& _rhs) const
    {
        return std::make_tuple(*id, *uuid) < std::make_tuple(*_rhs.id, *_rhs.uuid);
    }
};

} // namespace Fred::Notify::ObjectEvents::Impl::CollectData
} // namespace Fred::Notify::ObjectEvents::Impl
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
