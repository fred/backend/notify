/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg.hh"

#include "src/impl/notify_object_events.hh"
#include "src/util/set_logging.hh"

#include "libfred/db_settings.hh"

#include "liblog/liblog.hh"

#include <nlohmann/json.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>

namespace Cfg = Fred::Notify::ObjectEvents::Cfg;
namespace Impl = Fred::Notify::ObjectEvents::Impl;

namespace {

Impl::TemplateNames make_template_names(const Cfg::MessengerOptions& _messenger_options)
{
    return {
        {Impl::TemplateName::template_name_email_create_subject, _messenger_options.template_name_email_create_subject},
        {Impl::TemplateName::template_name_email_create_body, _messenger_options.template_name_email_create_body},
        {Impl::TemplateName::template_name_email_update_subject, _messenger_options.template_name_email_update_subject},
        {Impl::TemplateName::template_name_email_update_body, _messenger_options.template_name_email_update_body},
        {Impl::TemplateName::template_name_email_transfer_subject, _messenger_options.template_name_email_transfer_subject},
        {Impl::TemplateName::template_name_email_transfer_body, _messenger_options.template_name_email_transfer_body},
        {Impl::TemplateName::template_name_email_renew_subject, _messenger_options.template_name_email_renew_subject},
        {Impl::TemplateName::template_name_email_renew_body, _messenger_options.template_name_email_renew_body},
        {Impl::TemplateName::template_name_email_delete_subject, _messenger_options.template_name_email_delete_subject},
        {Impl::TemplateName::template_name_email_delete_body, _messenger_options.template_name_email_delete_body}};
}

std::string make_connect_string(const Cfg::DatabaseOptions& _option)
{
    std::string result;
    if (_option.host != boost::none)
    {
        result = "host=" + *_option.host;
    }
    if (_option.port != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "port=" + std::to_string(*_option.port);
    }
    if (_option.user != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "user=" + *_option.user;
    }
    if (_option.dbname != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "dbname=" + *_option.dbname;
    }
    if (_option.password != boost::none)
    {
        if (!result.empty())
        {
            result += " ";
        }
        result += "password=" + *_option.password;
    }
    return result;
}

} // namespace {anonymous}

int main(int argc, char** argv)
{
    Cfg::handle_cli_args(argc, argv);

    try
    {
        Fred::Notify::ObjectEvents::Util::set_logging(Cfg::Options::get().log);
        LIBLOG_SET_CONTEXT(Ctx, log_ctx, __func__);
        LIBLOG_INFO("fred-notify-object-event-notification configured");

        Database::emplace_default_manager<Database::StandaloneManager>(
                make_connect_string(Cfg::Options::get().database));

        Impl::notify_object_events(
                Impl::MessengerEndpoint{Cfg::Options::get().messenger.endpoint},
                Impl::MessengerArchive{Cfg::Options::get().messenger.archive},
                make_template_names(Cfg::Options::get().messenger),
                Impl::EventAgeDaysMax{Cfg::Options::get().notification.event_age_days_max});
    }
    catch (const std::exception& e)
    {
        LIBLOG_WARNING("std::exception caught: {}", e.what());
    }
    catch (...)
    {
        LIBLOG_WARNING("exception caught");
    }
}
