/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/notification_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

boost::program_options::options_description make_notification_options(NotificationOptions& _notification_options)
{
    boost::program_options::options_description notification_options{"Notification options", Util::terminal_width};
    const auto set_event_age_days_max = [&](unsigned int event_age_days_max) { _notification_options.event_age_days_max = event_age_days_max; };

    notification_options.add_options()
        ("notification.event_age_days_max", boost::program_options::value<unsigned int>()->notifier(set_event_age_days_max), "do not notify events older then event_age_days_max days");

    return notification_options;
}

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
