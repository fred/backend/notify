/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_CONSOLE_OPTIONS_HH_784CB5AD6B484413AE83E4BC85F8EB84
#define LOG_CONSOLE_OPTIONS_HH_784CB5AD6B484413AE83E4BC85F8EB84

#include "liblog/level.hh"
#include "liblog/sink/console_sink_config.hh"

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

struct LogConsole
{
    LibLog::Level min_severity;
    LibLog::Sink::ConsoleSinkConfig::OutputStream output_stream;
    LibLog::ColorMode color_mode;
};

struct LogConsoleOptions
{
    boost::optional<LibLog::Level> min_severity;
    boost::optional<LibLog::Sink::ConsoleSinkConfig::OutputStream> output_stream;
    boost::optional<LibLog::ColorMode> color_mode;
    void set_min_severity(const std::string& value);
    void set_output_stream(const std::string& value);
    void set_color_mode(const std::string& value);
};

boost::program_options::options_description make_log_console_options(LogConsoleOptions& _log_console_options);

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
