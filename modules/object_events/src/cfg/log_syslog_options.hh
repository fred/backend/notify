/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_SYSLOG_OPTIONS_HH_52C3F766966D4AA78ABF3CF3E42A032D
#define LOG_SYSLOG_OPTIONS_HH_52C3F766966D4AA78ABF3CF3E42A032D

#include "src/cfg/util/make_severity.hh"

#include "liblog/level.hh"

#include <boost/optional.hpp>
#include <boost/program_options.hpp>

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

struct LogSyslog
{
    std::string ident;
    LibLog::Level min_severity;
    int options;
    int facility;
};

struct LogSyslogOptions
{
    boost::optional<std::string> ident;
    boost::optional<int> facility_local_offset;
    boost::optional<LibLog::Level> min_severity;
    void set_ident(const std::string& value);
    void set_facility(int value);
    void set_min_severity(const std::string& severity);
};

boost::program_options::options_description make_log_syslog_options(LogSyslogOptions& _log_syslog_options);

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
