/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MESSENGER_OPTIONS_HH_491641DC5F844798AB7523507FA221EE
#define MESSENGER_OPTIONS_HH_491641DC5F844798AB7523507FA221EE

#include <boost/asio/ip/address.hpp>
#include <boost/program_options.hpp>

#include <string>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

struct MessengerOptions
{
    std::string endpoint;
    bool archive;
    std::string template_name_email_create_subject;
    std::string template_name_email_create_body;
    std::string template_name_email_update_subject;
    std::string template_name_email_update_body;
    std::string template_name_email_transfer_subject;
    std::string template_name_email_transfer_body;
    std::string template_name_email_renew_subject;
    std::string template_name_email_renew_body;
    std::string template_name_email_delete_subject;
    std::string template_name_email_delete_body;
};

boost::program_options::options_description make_messenger_options(MessengerOptions& _messenger_options);

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
