/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/log_syslog_options.hh"

#include "src/cfg/exception.hh"
#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

void LogSyslogOptions::set_ident(const std::string& _value)
{
    if (ident != boost::none)
    {
        throw Exception{"ident can be defined at most once"};
    }
    ident = _value;
}

void LogSyslogOptions::set_facility(int _value)
{
    if ((_value < 0) || (7 < _value))
    {
        throw Exception("facility out of range [0, 7]");
    }
    if (facility_local_offset != boost::none)
    {
        throw Exception{"facility can be defined at most once"};
    }
    facility_local_offset = _value;
}

void LogSyslogOptions::set_min_severity(const std::string& severity)
{
    if (min_severity != boost::none)
    {
        throw Exception{"severity can be defined at most once"};
    }
    min_severity = Util::make_severity(severity);
}

boost::program_options::options_description make_log_syslog_options(LogSyslogOptions& _log_syslog_options)
{
    boost::program_options::options_description options_description{"Logging into syslog options", Util::terminal_width};
    const auto set_ident = [&](const std::string& ident) { _log_syslog_options.set_ident(ident); };
    const auto set_facility = [&](int facility) { _log_syslog_options.set_facility(facility); };
    const auto set_min_severity = [&](const std::string& severity) { _log_syslog_options.set_min_severity(severity); };
    options_description.add_options()
        ("log.syslog.ident", boost::program_options::value<std::string>()->notifier(set_ident), "what ident to log with (default is empty string)")
        ("log.syslog.facility",
         boost::program_options::value<int>()->notifier(set_facility),
         "what LOG_LOCALx facility to log with (x in range 0..7, default means facility LOG_USER)")
        ("log.syslog.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace");
    return options_description;
}

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
