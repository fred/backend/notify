/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/messenger_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

boost::program_options::options_description make_messenger_options(MessengerOptions& _messenger_options)
{
    boost::program_options::options_description messenger_options{"Common messenger access options", Util::terminal_width};
    const auto set_endpoint = [&](const std::string& endpoint) { _messenger_options.endpoint = endpoint; };
    const auto set_archive = [&](const bool archive) { _messenger_options.archive = archive; };
    const auto set_template_name_email_create_subject = [&](const std::string& template_name_email_create_subject) { _messenger_options.template_name_email_create_subject = template_name_email_create_subject; };
    const auto set_template_name_email_create_body = [&](const std::string& template_name_email_create_body) { _messenger_options.template_name_email_create_body = template_name_email_create_body; };
    const auto set_template_name_email_update_subject = [&](const std::string& template_name_email_update_subject) { _messenger_options.template_name_email_update_subject = template_name_email_update_subject; };
    const auto set_template_name_email_update_body = [&](const std::string& template_name_email_update_body) { _messenger_options.template_name_email_update_body = template_name_email_update_body; };
    const auto set_template_name_email_transfer_subject = [&](const std::string& template_name_email_transfer_subject) { _messenger_options.template_name_email_transfer_subject = template_name_email_transfer_subject; };
    const auto set_template_name_email_transfer_body = [&](const std::string& template_name_email_transfer_body) { _messenger_options.template_name_email_transfer_body = template_name_email_transfer_body; };
    const auto set_template_name_email_renew_subject = [&](const std::string& template_name_email_renew_subject) { _messenger_options.template_name_email_renew_subject = template_name_email_renew_subject; };
    const auto set_template_name_email_renew_body = [&](const std::string& template_name_email_renew_body) { _messenger_options.template_name_email_renew_body = template_name_email_renew_body; };
    const auto set_template_name_email_delete_subject = [&](const std::string& template_name_email_delete_subject) { _messenger_options.template_name_email_delete_subject = template_name_email_delete_subject; };
    const auto set_template_name_email_delete_body = [&](const std::string& template_name_email_delete_body) { _messenger_options.template_name_email_delete_body = template_name_email_delete_body; };

    messenger_options.add_options()
        ("messenger.endpoint", boost::program_options::value<std::string>()->notifier(set_endpoint), "name of endpoint to connect to")
        ("messenger.archive", boost::program_options::value<bool>()->notifier(set_archive), "archive the message")
        ("messenger.template_name_email_create_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_create_subject), "name of the template used for the email subject (create event)")
        ("messenger.template_name_email_create_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_create_body), "name of the template used for the email body (create event)")
        ("messenger.template_name_email_update_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_update_subject), "name of the template used for the email subject (update event)")
        ("messenger.template_name_email_update_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_update_body), "name of the template used for the email body (update event)")
        ("messenger.template_name_email_transfer_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_transfer_subject), "name of the template used for the email subject (transfer event)")
        ("messenger.template_name_email_transfer_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_transfer_body), "name of the template used for the email body (transfer event)")
        ("messenger.template_name_email_renew_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_renew_subject), "name of the template used for the email subject (renew event)")
        ("messenger.template_name_email_renew_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_renew_body), "name of the template used for the email body (renew event)")
        ("messenger.template_name_email_delete_subject", boost::program_options::value<std::string>()->notifier(set_template_name_email_delete_subject), "name of the template used for the email subject (delete event)")
        ("messenger.template_name_email_delete_body", boost::program_options::value<std::string>()->notifier(set_template_name_email_delete_body), "name of the template used for the email body (delete event)");

    return messenger_options;
}

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
