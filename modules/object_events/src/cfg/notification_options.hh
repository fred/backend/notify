/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef NOTIFICATION_OPTIONS_HH_C960F5263E934B6898A90D3279E846F9
#define NOTIFICATION_OPTIONS_HH_C960F5263E934B6898A90D3279E846F9

#include <boost/program_options.hpp>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

struct NotificationOptions
{
    unsigned int event_age_days_max;
};

boost::program_options::options_description make_notification_options(NotificationOptions& _notification_options);

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
