/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/database_options.hh"

#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

boost::program_options::options_description make_database_options(DatabaseOptions& _database_options)
{
    boost::program_options::options_description database_options{"Common database access options", Util::terminal_width};
    const auto set_host = [&](const std::string& host) { _database_options.host = host; };
    const auto set_host_addr = [&](const std::string& ip_address) { _database_options.host_addr = boost::asio::ip::address::from_string(ip_address); };
    const auto set_port = [&](::in_port_t port) { _database_options.port = port; };
    const auto set_user = [&](const std::string& user) { _database_options.user = user; };
    const auto set_dbname = [&](const std::string& dbname) { _database_options.dbname = dbname; };
    const auto set_password = [&](const std::string& password) { _database_options.password = password; };
    const auto set_connect_timeout = [&](std::chrono::seconds::rep seconds) { _database_options.connect_timeout = std::chrono::seconds{seconds}; };
    database_options.add_options()
        ("database.host", boost::program_options::value<std::string>()->notifier(set_host), "name of host to connect to")
        ("database.host_addr", boost::program_options::value<std::string>()->notifier(set_host_addr), "IP address of host to connect to")
        ("database.port", boost::program_options::value<::in_port_t>()->notifier(set_port), "port number to connect to at the server host")
        ("database.user", boost::program_options::value<std::string>()->notifier(set_user), "PostgreSQL user name to connect as")
        ("database.dbname", boost::program_options::value<std::string>()->notifier(set_dbname), "the database name")
        ("database.password", boost::program_options::value<std::string>()->notifier(set_password), "password used for password authentication")
        ("database.connect_timeout",
         boost::program_options::value<std::chrono::seconds::rep>()->notifier(set_connect_timeout),
         "Maximum wait for connection, in seconds. Zero or not specified means wait indefinitely. "
         "It is not recommended to use a timeout of less than 2 seconds.");
    return database_options;
}

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
