/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/log_console_options.hh"

#include "src/cfg/exception.hh"
#include "src/cfg/util/make_color_mode.hh"
#include "src/cfg/util/make_output_stream.hh"
#include "src/cfg/util/make_severity.hh"
#include "src/cfg/util/terminal_width.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {

void LogConsoleOptions::set_min_severity(const std::string& value)
{
    if (min_severity != boost::none)
    {
        throw Exception{"severity can be defined at most once"};
    }
    min_severity = Util::make_severity(value);
}

void LogConsoleOptions::set_output_stream(const std::string& value)
{
    if (output_stream != boost::none)
    {
        throw Exception{"severity can be defined at most once"};
    }
    output_stream = Util::make_output_stream(value);
}

void LogConsoleOptions::set_color_mode(const std::string& value)
{
    if (color_mode != boost::none)
    {
        throw Exception{"color mode can be defined at most once"};
    }
    color_mode = Util::make_color_mode(value);
}

boost::program_options::options_description make_log_console_options(LogConsoleOptions& _log_console_options)
{
    boost::program_options::options_description options_description{"Logging on console options", Util::terminal_width};
    const auto set_min_severity = [&](const std::string& severity) { _log_console_options.set_min_severity(severity); };
    const auto set_output_stream = [&](const std::string& output_stream) { _log_console_options.set_output_stream(output_stream); };
    const auto set_color_mode = [&](const std::string& color_mode) { _log_console_options.set_color_mode(color_mode); };
    options_description.add_options()
        ("log.console.min_severity",
         boost::program_options::value<std::string>()->notifier(set_min_severity),
         "do not log more trivial events; "
         "severity in descending order: critical/error/warning/info/debug/trace")
        ("log.console.output_stream",
         boost::program_options::value<std::string>()->notifier(set_output_stream),
         "where to log (stderr/stdout), default is stderr")
        ("log.console.color_mode",
         boost::program_options::value<std::string>()->notifier(set_color_mode),
         "how to colorize output (always/auto/never), default is never");
    return options_description;
}

} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
