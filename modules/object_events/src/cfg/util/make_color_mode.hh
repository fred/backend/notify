/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MAKE_COLOR_MODE_HH_5A87B08532C94E9B98EB3B9BAF261ECB
#define MAKE_COLOR_MODE_HH_5A87B08532C94E9B98EB3B9BAF261ECB

#include "src/cfg/exception.hh"

#include "liblog/color_mode.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {
namespace Util {

LibLog::ColorMode make_color_mode(const std::string& _str);

} // namespace Fred::Notify::ObjectEvents::Cfg::Util
} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
