/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/cfg/exception.hh"
#include "src/cfg/util/make_color_mode.hh"

#include <string>
#include <unordered_map>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Cfg {
namespace Util {

LibLog::ColorMode make_color_mode(const std::string& _str)
{
    static const std::unordered_map<std::string, LibLog::ColorMode> str2color_mode =
        {
            { "always", LibLog::ColorMode::always },
            { "auto", LibLog::ColorMode::automatic },
            { "never", LibLog::ColorMode::never }
        };
    const auto color_mode_itr = str2color_mode.find(_str);
    if (color_mode_itr != str2color_mode.end())
    {
        return color_mode_itr->second;
    }
    throw Exception("\"" + _str + "\" not a valid color mode");
}

} // namespace Fred::Notify::ObjectEvents::Cfg::Util
} // namespace Fred::Notify::ObjectEvents::Cfg
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
