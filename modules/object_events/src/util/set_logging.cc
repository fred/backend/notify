/*
 * Copyright (C) 2021-2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/set_logging.hh"

#include "liblog/liblog.hh"
#include "liblog/log_config.hh"
#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <grpc++/grpc++.h>
#include <grpc/support/log.h>

#include <iostream>

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Util {

namespace {

decltype(auto) make_sink_config(const Cfg::LogConsole& log_options)
{
    LibLog::Sink::ConsoleSinkConfig sink_config;
    sink_config.set_level(log_options.min_severity);
    sink_config.set_output_stream(log_options.output_stream);
    sink_config.set_color_mode(log_options.color_mode);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::LogFile& log_options)
{
    LibLog::Sink::FileSinkConfig sink_config{log_options.file_name};
    sink_config.set_level(log_options.min_severity);
    return sink_config;
}

decltype(auto) make_sink_config(const Cfg::LogSyslog& log_options)
{
    LibLog::Sink::SyslogSinkConfig sink_config;
    sink_config.set_syslog_facility(log_options.facility);
    sink_config.set_level(log_options.min_severity);
    sink_config.set_syslog_ident(log_options.ident);
    sink_config.set_syslog_options(log_options.options);
    return sink_config;
}

void disable_grpc_library_logging()
{
    static const auto dummy_log_func = [](::gpr_log_func_args*) { };
    ::gpr_set_log_function(dummy_log_func);
    static constexpr auto do_not_log = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);
    ::gpr_set_log_verbosity(do_not_log);
}

void grpc_log(::gpr_log_func_args* log_args)
{
    try
    {
        switch (log_args->severity)
        {
            case GPR_LOG_SEVERITY_DEBUG:
                LIBLOG_DEBUG("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_INFO:
                LIBLOG_INFO("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
            case GPR_LOG_SEVERITY_ERROR:
                LIBLOG_ERROR("{} at {}:{}", log_args->message, log_args->file, log_args->line);
                return;
        }
        LIBLOG_WARNING("{} at {}:{}", log_args->message, log_args->file, log_args->line);
    } catch (...)
    { }
}

constexpr auto no_log_grpc_severity = static_cast<::gpr_log_severity>(GPR_LOG_SEVERITY_ERROR + 1);

constexpr ::gpr_log_severity make_grpc_log_severity(LibLog::Level min_severity)
{
    switch (min_severity)
    {
        case LibLog::Level::critical:
            return no_log_grpc_severity;
        case LibLog::Level::error:
            return GPR_LOG_SEVERITY_ERROR;
        case LibLog::Level::warning:
        case LibLog::Level::info:
            return GPR_LOG_SEVERITY_INFO;
        case LibLog::Level::debug:
        case LibLog::Level::trace:
            return GPR_LOG_SEVERITY_DEBUG;
    }
    return no_log_grpc_severity;
}

decltype(auto) get_min_severity(const Cfg::Log& log)
{
    boost::optional<LibLog::Level> min_severity;
    const auto update_min_severity = [&](LibLog::Level severity)
    {
        if ((min_severity == boost::none) ||
            (severity < *min_severity))
        {
            min_severity = severity;
        }
    };
    if (log.log_console != boost::none)
    {
        update_min_severity(log.log_console->min_severity);
    }
    if (log.log_file != boost::none)
    {
        update_min_severity(log.log_file->min_severity);
    }
    if (log.log_syslog != boost::none)
    {
        update_min_severity(log.log_syslog->min_severity);
    }
    return min_severity;
}

void set_grpc_library_logging(const Cfg::Log& log)
{
    if (!log.log_grpc_library_events)
    {
        LIBLOG_INFO("do not log gRPC library");
        disable_grpc_library_logging();
        return;
    }
    const auto min_severity = get_min_severity(log);
    if (min_severity == boost::none)
    {
        disable_grpc_library_logging();
        return;
    }
    ::gpr_set_log_function(grpc_log);
    ::gpr_set_log_verbosity(make_grpc_log_severity(*min_severity));
    LIBLOG_INFO("log gRPC library");
}

} // namespace Fred::Notify::ObjectEvents::Util::{anonymous}

void set_logging(const Cfg::Log& log)
{
    bool has_device = false;
    LibLog::LogConfig log_config;
    if (log.log_console != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_console));
            has_device = true;
        }
        catch (...) { }
    }
    if (log.log_file != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_file));
            has_device = true;
        }
        catch (...) { }
    }
    if (log.log_syslog != boost::none)
    {
        try
        {
            log_config.add_sink_config(make_sink_config(*log.log_syslog));
            has_device = true;
        }
        catch (...) { }
    }
    if (has_device)
    {
        try
        {
            LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
            set_grpc_library_logging(log);
            return;
        }
        catch (const std::exception& e)
        {
            std::cerr << "Setting up logging failure: " << e.what() << std::endl;
            ::exit(EXIT_FAILURE);
        }
        catch (...)
        {
            std::cerr << "Setting up logging failure: Unexpected exception caught" << std::endl;
            ::exit(EXIT_FAILURE);
        }
    }
        try
    {
        disable_grpc_library_logging();
    }
    catch (...)
    {
        std::cerr << "unable to disable gRPC library logging" << std::endl;
    }
    std::cout << "no logging is configured!?" << std::endl;
}

} // namespace Fred::Notify::ObjectEvents::Util
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred
