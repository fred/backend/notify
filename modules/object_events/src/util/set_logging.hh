/*
 * Copyright (C) 2021  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SET_LOGGING_HH_0238DF1D7337466988DD6CC7B3856B6F
#define SET_LOGGING_HH_0238DF1D7337466988DD6CC7B3856B6F

#include "src/cfg.hh"

namespace Fred {
namespace Notify {
namespace ObjectEvents {
namespace Util {

void set_logging(const Cfg::Log& log);

} // namespace Fred::Notify::ObjectEvents::Util
} // namespace Fred::Notify::ObjectEvents
} // namespace Fred::Notify
} // namespace Fred

#endif
