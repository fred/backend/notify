# Logging options
# * device - specifies devices used for logging
#  * possible values: console, file, syslog
#  * is a multiple option
# * min_severity - events at least this severity are logged
#  * severities in ascending order: trace, debug, info, warning, error, critical
#  * can be overriden by particular device option
# * grpc_library - enable logging of events from gRPC library
[log]
device       = console
device       = file
device       = syslog
min_severity = debug
grpc_library = yes

# Logging into console options
# * min_severity - events at least this severity are logged into console
#  * overrides log.min_severity option
# * output_stream - where to log
#  * possible values: stderr, stdout
#  * default is stderr
# * color_mode - use colorized output
#  * possible values: always, auto, never
#  * default is never
[log.console]
min_severity  = info
output_stream = stderr
color_mode    = auto

# Logging into file options
# * file_name - file name for logging
# * min_severity - events at least this severity are logged into file
#  * overrides log.min_severity option
[log.file]
file_name    = fred-notify-object-events.log
min_severity = trace

# Logging by syslog options
# * ident - what ident to log with (default is an empty string)
# * min_severity - events at least this severity are logged into file
#  * overrides log.min_severity option
# * facility - what LOG_LOCALx facility to log with (x in range 0..7)
#  * if does not present use LOG_USER facility
[log.syslog]
ident        = fred-notify-object-events
min_severity = trace
facility     = 2

# Database options
# * host - name of host to connect to
# * host_addr - IP address of host to connect to
# * port - port number to connect to at the server host
# * user - PostgreSQL user name to connect as
# * dbname - the database name
# * password - password used for password authentication
# * connect_timeout - maximum wait for connection
#  * in seconds
#  * zero or not specified means wait indefinitely
#  * [It is not recommended to use a timeout of less than 2 seconds.]
[database]
host     = localhost
port     = 5432
dbname   = fred
user     = fred
password = password

# Messenger options
# * endpoint - URI of Fred.Messenger.Api.Email gRPC service
# * archive - archive email message
# * template_name_email_EVENTNAME_subject - name of template to be used for email subject for EVENTNAME event
# * template_name_email_EVENTNAME_body - name of template to be used for email body for EVENTNAME event
#   * EVENTNAME can be create, update, transfer, renew, delete
# * event_age_days_max - event older than this will not be notified
[messenger]
endpoint = localhost:50051
archive = yes
template_name_email_create_subject = notification-create-subject.txt
template_name_email_create_body = notification-create-body.txt
template_name_email_update_subject = notification-update-subject.txt
template_name_email_update_body = notification-update-body.txt
template_name_email_transfer_subject = notification-transfer-subject.txt
template_name_email_transfer_body = notification-transfer-body.txt
template_name_email_renew_subject = notification-renew-subject.txt
template_name_email_renew_body = notification-renew-body.txt
template_name_email_delete_subject = notification-delete-subject.txt
template_name_email_delete_body = notification-delete-body.txt

[notification]
# * event_age_days_max - do not notify events older then event_age_days_max days
event_age_days_max = 1
